<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<main class="page-contents full-width">
  
  <div class="pge-bg dark-bg">
  	<!-- PAGE TITLE -->
    <?php get_template_part( 'template-parts/pages/content', 'page-title' ); ?>

    <!-- BREADCRUMBS -->
    <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

    <!--
    	<?php
    		/**
    		 * woocommerce_before_main_content hook.
    		 *
    		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
    		 * @hooked woocommerce_breadcrumb - 20
    		 */
    		//do_action( 'woocommerce_before_main_content' );
    	?>
    -->

    		<?php while ( have_posts() ) : the_post(); ?>

          <div class="max-width">
    			 <?php wc_get_template_part( 'content', 'single-product' ); ?>
          </div>

    		<?php endwhile; // end of the loop. ?>

    <!--
    	<?php
    		/**
    		 * woocommerce_after_main_content hook.
    		 *
    		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
    		 */
    		//do_action( 'woocommerce_after_main_content' );
    	?>

    	<?php
    		/**
    		 * woocommerce_sidebar hook.
    		 *
    		 * @hooked woocommerce_get_sidebar - 10
    		 */
    		//do_action( 'woocommerce_sidebar' );
    	?>
    -->
  </div>

  <!-- SPECIALS SLIDER -->
  <?php get_template_part( 'template-parts/elements/content', 'specials-slider' ); ?>

  <!-- NEWSLETTER CTA -->
  <div class="newsletter cta-container full-width vertical-align-parent dark-bg">
    <div class="vertical-align-child">
				<div class="max-width">
				<h2>Join the Magic<br>Be Cinergy Elite!</h2>
				<p>Free Popcorn, $5 Elite reward after 300 points, birthday movie ticket, exclusive content, sneak peeks, special offers and so much more!</p>
				<a href="https://tickets.cinergycinemas.com/Browsing/Loyalty/clubs/1" class="btn primary-btn" tabindex="0"><span>Join Now</span></a>
			</div>
    </div>
    <div class="overlay"></div>
    <div class="video-wrap"> 
      <video muted="" autoplay="" loop="" poster="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.jpg" class="bgvid">  
				<source src="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.mp4" type="video/mp4">
			  <source src="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.webm" type="video/webm"> 
      </video> 
    </div>
  </div>

</main>
<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
