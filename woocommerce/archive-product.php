<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<main class="page-contents full-width">
	  
	  <div class="pge-bg dark-bg">
	  	<!-- PAGE TITLE -->
	    <?php get_template_part( 'template-parts/pages/content', 'page-title' ); ?>

	    <!-- BREADCRUMBS -->
	    <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>


			<!-- <?php
				/**
				 * woocommerce_before_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 * @hooked WC_Structured_Data::generate_website_data() - 30
				 */
				//do_action( 'woocommerce_before_main_content' );
			?> -->

			<!-- <header class="woocommerce-products-header">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
				<?php endif; ?>
				<?php
					/**
					 * woocommerce_archive_description hook.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );
				?>
   		 </header> -->

   		<div class="shop-container max-width">
				<?php if ( have_posts() ) : ?>
					<?php
						/**
						 * woocommerce_before_shop_loop hook.
						 *
						 * @hooked wc_print_notices - 10
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						do_action( 'woocommerce_before_shop_loop' );
					?>

					<?php woocommerce_product_loop_start(); ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php
								/**
								 * woocommerce_shop_loop hook.
								 *
								 * @hooked WC_Structured_Data::generate_product_data() - 10
								 */
								do_action( 'woocommerce_shop_loop' );
							?>

							<?php wc_get_template_part( 'content', 'product' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>

					<?php
						/**
						 * woocommerce_after_shop_loop hook.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					?>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php
						/**
						 * woocommerce_no_products_found hook.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
					?>

				<?php endif; ?>
			</div>

<!-- 		<?php
			/**
			 * woocommerce_after_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			//do_action( 'woocommerce_after_main_content' );
		?> -->

<!-- 		<?php
			/**
			 * woocommerce_sidebar hook.
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			//do_action( 'woocommerce_sidebar' );
		?> -->

  </div>

  <!-- SPECIALS SLIDER -->
  <?php get_template_part( 'template-parts/elements/content', 'specials-slider' ); ?>

  <!-- NEWSLETTER CTA -->
  <div class="newsletter cta-container full-width vertical-align-parent dark-bg">
    <div class="vertical-align-child">
			<div class="max-width">
				<h2>Join the Magic<br>Be Cinergy Elite!</h2>
				<p>Free Popcorn, $5 Elite reward after 300 points, birthday movie ticket, exclusive content, sneak peeks, special offers and so much more!</p>
				<a href="https://tickets.cinergycinemas.com/Browsing/Loyalty/clubs/1" class="btn primary-btn" tabindex="0"><span>Join Now</span></a>
			</div>
    </div>
    <div class="overlay"></div>
    <div class="video-wrap"> 
      <video muted="" autoplay="" loop="" poster="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.jpg" class="bgvid"> 
				<source src="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.mp4" type="video/mp4">
				<source src="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.webm" type="video/webm">  
      </video> 
    </div>
  </div>

</main>

<?php get_footer( 'shop' ); ?>
