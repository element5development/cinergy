<?php

/*-----------------------------------------
  ENABLE ACF OPTIONS PAGE
-----------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_sub_page('Default Images');
	acf_add_options_sub_page('Theme Options');
}

/*-----------------------------------------
  ENABLE ACF OPTIONS PAGE
-----------------------------------------*/
add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
function custom_fields_to_excerpts($content, $post, $query) {
	$custom_fields = get_post_custom_keys($post->ID);
	$remove_underscore_fields = true;
	if (is_array($custom_fields)) {
		$custom_fields = array_unique($custom_fields);  // no reason to index duplicates
		foreach ($custom_fields as $field) {
			if ($remove_underscore_fields) {
				if (substr($field, 0, 1) == '_') continue;
			}
			$values = get_post_meta($post->ID, $field, false);
			if ("" == $values) continue;
			foreach ($values as $value) {
				if ( !is_array ( $value ) ) {
					$content .= " | " . $value;
				}
			}
		}
	}
	return $content;
}

/*-----------------------------------------
		AUTO CLOSE ACF SECTIONS
-----------------------------------------*/
function my_acf_admin_head() {
 ?>
 <script type="text/javascript">
   (function($){
     $(document).ready(function(){
       $('.layout').addClass('-collapsed');
       $('.acf-postbox').addClass('closed');
     });
   })(jQuery);
 </script>
 <?php
}
add_action('acf/input/admin_head', 'my_acf_admin_head');


?>