<?php
/*-----------------------------------------
  SHORTCODE ALLOWED IN SIDEBARS
-----------------------------------------*/
add_filter('widget_text', 'do_shortcode');

/*-----------------------------------------
  LEAGAL TEXT SHORTCODE
-----------------------------------------*/
function legal_text($atts, $content = null) {
	extract( shortcode_atts( array( ), $atts ) );
	return '<p class="legal-text">' . do_shortcode($content) . '</p>';
}
add_shortcode('legal-text', 'legal_text');

/*-----------------------------------------
  BUTTON SHORTCODE
-----------------------------------------*/
function cta_button($atts, $content = null) {
	extract( shortcode_atts( array(
		'target' => '',
		'url' => '#',
		'type' => '',
		'color' => '',
	), $atts ) );

	if ( $type == 'arrow' ) {
		$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg>';
	} else if ( $type == 'email' ) {
		$svg = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" enable-background="new 0 0 50 50"><path d="M2.8 10.838l22.2 17.7 22.2-17.7h-44.4zm-1.8 1.4v25.5l14.7-13.8-14.7-11.7zm48 0l-14.7 11.7 14.7 13.8v-25.5zm-31.5 13.1l-14.9 13.8h44.8l-14.8-13.9-6.9 5.5c-.4.3-1 .3-1.4 0l-6.8-5.4z"></path></svg>';
	} else if ( $type == 'phone' ) {
		$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path d="M11.63 10.35a1.23 1.23 0 0 0-1.63.31c-.59.71-1.32 1.9-4.07-.79-2.74-2.69-1.53-3.4-.81-3.97.73-.58.52-1.3.32-1.6-.2-.3-1.47-2.22-1.7-2.54-.22-.32-.53-.85-1.23-.75A3.54 3.54 0 0 0 0 4.32c0 2.19 1.77 4.89 4.19 7.26 2.42 2.36 5.17 4.09 7.42 4.09s3.3-1.94 3.38-2.45c.1-.69-.43-.99-.76-1.2l-2.6-1.67"></path></svg>';
	} else if ( $type == 'map' ) {
		$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="18" viewBox="0 0 14 18"><path d="M2.33 6.75c0 2.49 2.09 4.5 4.67 4.5a4.58 4.58 0 0 0 4.67-4.5c0-2.49-2.09-4.5-4.67-4.5a4.58 4.58 0 0 0-4.67 4.5zM0 6.75A6.87 6.87 0 0 1 7 0c3.87 0 7 3.02 7 6.75S7 18 7 18 0 10.48 0 6.75z"?></svg>'; 
	} else if ( $type == 'pdf' ) {
		$svg = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M21.8.838c-2.7 0-4.9 2.2-4.9 4.9 0 3.5 2.1 8.3 4.3 12.7-1.8 5.5-3.7 11.4-6.3 16.4-5.2 2-9.7 3.6-12.3 5.6-1 .9-1.5 2.5-1.5 3.6 0 2.7 2.2 4.9 4.9 4.9 1.4 0 2.7-.6 3.6-1.5 1.9-2.3 6.5-10.8 6.5-10.8 5-2 10.3-4 15.3-5.2 3.5 2.9 8.7 4.8 12.9 4.8 2.7 0 4.9-2.2 4.9-4.9s-2.2-4.9-4.9-4.9c-3.4 0-8.5 1.3-12.3 2.6-3.3-3-6.3-6.8-8.4-10.9 1.5-4.6 3.2-9.2 3.2-12.5-.1-2.6-2.3-4.8-5-4.8zm0 2.2c1.5 0 2.7 1.2 2.7 2.7 0 2.1-1.1 5.7-2.4 9.5-1.7-3.7-3.1-7.3-3.1-9.5.1-1.5 1.3-2.7 2.8-2.7zm.8 17.9c1.9 3.3 4.3 6.3 6.9 8.8-4 1-7.9 2.5-11.7 3.9 1.9-4 3.4-8.4 4.8-12.7zm21.7 7.8c1.5 0 2.7 1.2 2.7 2.7 0 1.5-1.2 2.7-2.7 2.7-3 0-7.1-1.5-10.1-3.4 3.4-1 7.6-2 10.1-2zm-31.5 9.3c-1.8 3.3-3.7 6.5-5 8-.5.5-1.1.8-1.9.8-1.5 0-2.7-1.2-2.7-2.7 0-.7.3-1.4.8-1.9 1.5-1.3 4.9-2.7 8.8-4.2z"></path></svg>'; 
	} else if ( $type == 'play' ) {
		$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><clipPath id="a"><path d="M189 40V16h24v24zm3-10.35l17.54 7.13L203.83 19z"/></clipPath></defs><path stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" d="M203.83 19l5.71 17.78L192 29.65z" clip-path="url(&quot;#a&quot;)" transform="translate(-189 -16)"></path></svg>'; 
	} else {
		$svg = '';
	}

	if ( $type == 'arrow' || $type == 'play' ) { 
		$link = '<a target="'.$target.'" href="'.$url.'" class="btn '.$color.'-btn '.$type.'"><span>' . do_shortcode($content) . $svg . '</span></a>';
	} else {
		$link = '<a target="'.$target.'" href="'.$url.'" class="btn '.$color.'-btn '.$type.'"><span>' . $svg . do_shortcode($content) . '</span></a>';
	}

	return $link;
}
add_shortcode('cta-btn', 'cta_button');