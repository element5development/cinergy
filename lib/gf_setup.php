<?php

/*-----------------------------------------
  SUBMIT INPUT TO BUTTON ELEMENT
-----------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="btn btn--primary"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "<span>{$form['button']['text']}</span></button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );