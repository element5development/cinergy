<?php

/*-----------------------------------------
  STYLEGUIDE HEX TO RGB
-----------------------------------------*/
function hex2rgb( $colour ) {
  if ( $colour[0] == '#' ) { 
    $colour = substr( $colour, 1 ); 
  }
  if ( strlen( $colour ) == 6 ) {
    list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
  } elseif ( strlen( $colour ) == 3 ) {
    list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
  } else {
    return false;
  }
  $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
  return 'rgb('.$r.', '.$g.', '.$b.')';
}

/*-----------------------------------------
  STYLEGUIDE RGB TO CMYK
-----------------------------------------*/
function hex2rgb2($hex) {
  $color = str_replace('#','',$hex);
  $rgb = array(
    'r' => hexdec(substr($color,0,2)),
    'g' => hexdec(substr($color,2,2)),
    'b' => hexdec(substr($color,4,2)),
  );
  return $rgb;
}
function rgb2cmyk($var1,$g=0,$b=0) {
  if (is_array($var1)) { 
    $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
  } else { 
    $r = $var1; 
  }
  $r = $r / 255; $g = $g / 255; $b = $b / 255;
  $bl = 1 - max(array($r,$g,$b));
  if ( ( 1 - $bl ) > 0 ) {
    $c = ( 1 - $r - $bl ) / ( 1 - $bl ); 
    $m = ( 1 - $g - $bl ) / ( 1 - $bl ); 
    $y = ( 1 - $b - $bl ) / ( 1 - $bl );
  }
  $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
  return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
}
