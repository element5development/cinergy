<?php

/*-----------------------------------------
		SIDEBARS - www.wp-hasty.com
-----------------------------------------*/
if ( ! function_exists( 'custom_sidebar' ) ) {
	function custom_sidebar() {
			 //Single Right Sidebar
			 $args = array(
					 'id'          => 'blog-sidebar',
					 'name'    => __( 'Blog Sidebar'),
					 'description'   => __( 'This is the sidebar found on the blog pages'),
					 'class'   => 'blog-sidebar',
					 'before_title'  => '<h2 class="blog-sidebar-title">',
					 'after_title'   => '</h2>',
					 'before_widget' => '<div id="%1$s" class="widget %2$s">',
					 'after_widget'  => '</div>',
			 );
			 register_sidebar( $args );
	}
add_action( 'widgets_init', 'custom_sidebar' );
}