<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function register_menu () {
	register_nav_menu('primary-nav', __('Primary Navigation'));
	register_nav_menu('utility-nav', __('Utility Navigation'));
	register_nav_menu('about-nav', __('About Sub Navigation'));
}
add_action('init', 'register_menu');

/*-----------------------------------------
		ADD SMOOTHSCROLL CLASS TO MENU LINKS
-----------------------------------------*/
function add_menuclass($ulclass) {
	return preg_replace('/<a rel="smoothScroll"/', '<a rel="smoothScroll" class="smoothScroll"', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');