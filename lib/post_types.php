<?php

/*-----------------------------------------
	CUSTOM POST TYPES - www.wp-hasty.com
-----------------------------------------*/
function create_posttype() {
	//SPECIALS / DEALS
	register_post_type( 'Deals',
		array(
			'labels' => array(
				'name' => __( 'Specials' ),
				'singular_name' => __( 'Special' )
			),
			'public' => true,
			'has_archive' => false,
			'rewrite' => array('slug' => 'special'),
			'taxonomies'  => array( 'category' ),
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'  )
		)
	);
	//FOOD
	register_post_type( 'Food',
		array(
			'labels' => array(
				'name' => __( 'Food' ),
				'singular_name' => __( 'Food' )
			),
			'public' => true,
			'has_archive' => false,
			'rewrite' => array('slug' => 'food'),
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'  )
		)
	);
	//GAMES/ACTIVITIES
	register_post_type( 'Play',
		array(
			'labels' => array(
				'name' => __( 'Play' ),
				'singular_name' => __( 'Play' )
			),
			'public' => true,
			'has_archive' => false,
			'rewrite' => array('slug' => 'play'),
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt'  )
		)
	);
}
add_action( 'init', 'create_posttype' );
