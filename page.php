<?php /*
DEFAULT PAGE TEMPLATE
*/ ?>

<?php get_header(); ?>

<main class="page-contents full-width">

  <div class="pge-bg dark-bg">
    <!-- PAGE TITLE -->
    <?php get_template_part( 'template-parts/pages/content', 'page-title' ); ?>

    <a id="content-anchor" class="content-anchor"></a>

    <!-- BREADCRUMBS -->
    <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

    <!-- SECONDARY NAV -->
    <?php get_template_part( 'template-parts/navigation/content', 'secondary-nav' ); ?>

    <?php if ( is_page(1482) ) { //EAT & DRINK ?>
      <div class="menu-intro">
        <div class="max-width clearfix">
          <?php the_field('menu_intro'); ?>
        </div>
      </div>
    <?php } ?>

    <!-- TWO COLUMN BASIC -->
    <?php if ( get_field('left_half_basic') && get_field('right_half_basic') ) { ?>
      <?php $image = get_field('col_basic_background_image'); ?>
      <div class="two_col_basic
        <?php if ( is_page(1464) ) { ?>left<?php } ?>
      " style="background-image:url('<?php echo $image['url']; ?>');">
        <div class="flex-container max-width">
          <div class="one-half"><?php the_field('left_half_basic') ?></div>
          <div class="one-half">
            <?php the_field('right_half_basic') ?>
             <?php if ( is_page(1465) ) { //PARTIES AND EVENTS ?>
              <?php if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION COPPERAS COVE
                ?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_copperas_cove/424" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
              } else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MIDLAND
                ?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_midland/440" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
              } else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODESSA
                ?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_odessa/441" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
							} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION AMARILLO
                ?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_amarillo/442" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
							} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION TULSA
                ?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_tulsa/443" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
							} else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION MARBLE FALLS
                ?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_marble_falls/444" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
							} else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION GRANBURY
                ?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_granbury/445" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
							} else { ?>
								<h3 class="align-center">Select a location to book your party</h3>
								<div class="location-option-container">
									<div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
									<div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
									<div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
									<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
									<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
									<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
									<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
								</div>
             <?php } 
            } ?>
          </div>
        </div>
        <div class="overlay"></div>
        <div class="overlay two"></div>
      </div>
    <?php } ?>

    <!-- PURCHASE -->
    <?php if ( get_field('shop_instructions') && get_field('shop_products') ) { ?>
      <div class="two_col_basic shop-cards">
        <a id="fun-cards" class="styleguide-anchor"></a>
        <div class="flex-container max-width">
          <div class="one-half">
            <h2>Purchase</h2>
            <div class="flex-container">
              <?php get_template_part( 'template-parts/elements/content', 'shop-products' ); ?>
            </div>
          </div>
          <div class="one-half"><?php the_field('shop_instructions') ?></div>
        </div>
      </div>
    <?php } ?>

    <!-- SIMPLE CTA SECTION -->
    <?php if ( get_field('simple_cta_section_cta_link') && !is_page(1465) ) { ?>
      <?php get_template_part( 'template-parts/elements/content', 'simple-cta-section' ); ?>
    <?php } ?>

    <!-- SPECIALS SLIDER -->
    <?php if ( is_page(1466) ) { //FUN CARDS?>
      <?php get_template_part( 'template-parts/elements/content', 'specials-slider' ); ?>
    <?php } ?>

    <!-- TWO COLUMN SCROLL -->
    <?php if ( get_field('left_half_scroll') && get_field('right_half_scroll') ) { ?>
      <div class="two_col_basic scroll">
        <div class="flex-container max-width">
          <?php if ( is_page(1465) ) { //PARTIES & EVENTS ?><a id="possibilities" class="styleguide-anchor"></a><?php } ?> 
          <?php if ( is_page(1466) ) { //PARTIES & EVENTS ?><a id="fun-card-faq" class="styleguide-anchor"></a><?php } ?> 
          <div class="one-half">
            <span class="sticky">
              <?php the_field('left_half_scroll') ?>
							<?php if ( is_page(1465) ) { //PARTIES AND EVENTS ?>
				 				<a href="https://cinergy.com/contact/?regarding=Parties%20%26%20Events" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>Book an Event<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a>
							<?php } ?>
            </span>
          </div>
          <div class="one-half"><?php the_field('right_half_scroll') ?></div>
        </div>
      </div>
    <?php } ?>

    <!-- SIMPLE CTA SECTION -->
    <?php if ( get_field('simple_cta_section_cta_link') && is_page(1465) ) { ?>
      <?php get_template_part( 'template-parts/elements/content', 'simple-cta-section' ); ?>
    <?php } ?>

    <!-- PARTIES & EVENTS CTA -->
    <?php if ( is_page(1691) ) { //PARTY PACKAGES ?>
      <div class="two_col_basic scroll">
        <div class="flex-container max-width">

          <?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED OR UNSET ?>
            <div class="location-forced">
              <h2 class="align-center">Select a location to view available packages</h2>
              <div class="location-option-container">
                <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
                <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
                <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
								<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
								<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
								<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
								<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
              </div>
            </div>
          <?php } else { //LOCATION IS SET ?>
            <div class="one-half">
              <span class="sticky">
                <h2>Party Packages</h2>
              </span>
            </div>
            <div class="one-half">
             <?php if ( have_rows('party_packages') ) : ?>
					<?php while ( have_rows('party_packages') ) { the_row();
						$i=0; $location_option = get_sub_field('location_based');
						if ( in_array($_COOKIE["CinergyLocation"], $location_option) ) { ?>
                  			<h3><?php the_sub_field('package_title'); ?></h3>
                  			<?php the_sub_field('package_description'); $i++; ?>
						<?php } ?>
              		<?php } ?>
				<?php else : ?>
					<h3>This location currently has no active party packages.</h3>
				<?php endif; ?>
            </div> 
          <?php } ?>
        </div>
      </div>
      <?php if ( $_COOKIE["CinergyLocation"] != 'location-none' ) {  ?>
        <div class="cta-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2017/05/parties_events_bg.png');">
          <div class="vertical-align-child max-width">
            <h2>Let us do the work for you</h2>
            <p class="btn-container">
                <?php
                if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION COPPERAS COVE
									?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_copperas_cove/424" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
								} else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MIDLAND
									?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_midland/440" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
								} else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODESSA
									?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_odessa/441" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
								} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION AMARILLO
									?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_amarillo/442" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
								} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION TULSA
									?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_tulsa/443" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
								} else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION MARBLE FALLS
									?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_marble_falls/444" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
								} else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION GRANBURY
									?><a target="_blank" href="https://us.partywirks.com/storefront_express/main/vendor/cinergy_granbury/445" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
								} else { //NO LOCATION IS SET 
									?><a href="https://cinergy.com/contact/?regarding=Birthday" class="btn primary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
                } 
                ?>
            </p>
          </div>
          <div class="overlay"></div>
        </div>
      <?php } ?>
    <?php } ?>

    <!-- ********************* -->
    <!--      MAIN CONTENT     -->
    <!-- ********************* -->
    <?php if ( is_page(1456) ) { //CONTACT PAGE?>
      <div class="contact-container">
        <div class="flex-container max-width">
          <div class="one-half">
            <h2>We'd love to<br/>hear from you</h2>
            <div class="location-details">
							<?php if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC ?>
								<?php
									$remove = array(" ","(",")",".","-");
									$cleanphone = str_replace($remove, "", get_field('location_phone', 1484));
								?>
                <?php echo do_shortcode('[cta-btn url="tel:+1'. $cleanphone .'" color="primary" type="phone"]'. get_field('location_phone', 1484) .'[/cta-btn]'); ?>
                <?php echo do_shortcode('[cta-btn target="_blank" url="http://maps.google.com/?q=Cinergy+'. get_field('location_address', 1484) .'" color="primary" type="map"]'. get_field('location_address', 1484) .'[/cta-btn]'); ?>
                <h3>Operating Hours</h3>
                <p><?php the_field('location_hours', 1484); ?></p>
              <?php } else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID ?>
								<?php
									$remove = array(" ","(",")",".","-");
									$cleanphone = str_replace($remove, "", get_field('location_phone', 1485));
								?>
                <?php echo do_shortcode('[cta-btn url="tel:+1'. $cleanphone .'" color="primary" type="phone"]'. get_field('location_phone', 1485) .'[/cta-btn]'); ?>
                <?php echo do_shortcode('[cta-btn target="_blank" url="http://maps.google.com/?q=Cinergy+'. get_field('location_address', 1485) .'" color="primary" type="map"]'. get_field('location_address', 1485) .'[/cta-btn]'); ?>
                <h3>Operating Hours</h3>
                <p><?php the_field('location_hours', 1485); ?></p>
              <?php } else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES ?>
								<?php
									$remove = array(" ","(",")",".","-");
									$cleanphone = str_replace($remove, "", get_field('location_phone', 1486));
								?>
                <?php echo do_shortcode('[cta-btn url="tel:+1'. $cleanphone .'" color="primary" type="phone"]'. get_field('location_phone', 1486) .'[/cta-btn]'); ?>
                <?php echo do_shortcode('[cta-btn target="_blank" url="http://maps.google.com/?q=Cinergy+'. get_field('location_address', 1486) .'" color="primary" type="map"]'. get_field('location_address', 1486) .'[/cta-btn]'); ?>
                <h3>Operating Hours</h3>
                <p><?php the_field('location_hours', 1486); ?></p>
							<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION Amarillo ?>
								<?php
									$remove = array(" ","(",")",".","-");
									$cleanphone = str_replace($remove, "", get_field('location_phone', 1483));
								?>
                <?php echo do_shortcode('[cta-btn url="tel:+1'. $cleanphone .'" color="primary" type="phone"]'. get_field('location_phone', 1483) .'[/cta-btn]'); ?>
                <?php echo do_shortcode('[cta-btn target="_blank" url="http://maps.google.com/?q=Cinergy+'. get_field('location_address', 1483) .'" color="primary" type="map"]'. get_field('location_address', 1483) .'[/cta-btn]'); ?>
                <h3>Operating Hours</h3>
								<p><?php the_field('location_hours', 1483); ?></p>
							<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION Tulsa ?>
								<?php
									$remove = array(" ","(",")",".","-");
									$cleanphone = str_replace($remove, "", get_field('location_phone', 3658));
								?>
                <?php echo do_shortcode('[cta-btn url="tel:+1'. $cleanphone .'" color="primary" type="phone"]'. get_field('location_phone', 3658) .'[/cta-btn]'); ?>
                <?php echo do_shortcode('[cta-btn target="_blank" url="http://maps.google.com/?q=Cinergy+'. get_field('location_address', 3658) .'" color="primary" type="map"]'. get_field('location_address', 3658) .'[/cta-btn]'); ?>
                <h3>Operating Hours</h3>
                <p><?php the_field('location_hours', 3658); ?></p>
              <?php } else { //NO LOCATION IS SET ?>
              <?php } ?>
            </div>
            <?php the_content() ?>
          </div>
          <div class="one-half">
            <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
          </div>
        </div>
      </div>
    <?php } elseif ( is_page(1481) ) { // LOCATIONS  ?>
      <div class="locations-container">
        <div class="max-width">
					<?php
					$childArgs = array(
							'sort_order' => 'ASC',
							'child_of' => '1481'
					);
					$childList = get_pages($childArgs);
					foreach ($childList as $child) { ?>
						<div class="location-summary post-preview post-preview-container flex-container">
							<div class="title one-third">
								<a href="<?php echo get_permalink($child->ID); ?>"><h2><?php the_field('location_name', $child->ID); ?></h2></a>
							</div>
							<div class="info one-third">
								<p><?php the_field('location_hours', $child->ID); ?></p>
							</div>
							<div class="details one-third">
								<a href="tel:+1<?php $remove = array(" ","(",")",".","-"); echo str_replace($remove, "", get_field('location_phone', $child->ID)); ?>"><?php the_field('location_phone', $child->ID); ?></a><br/>
								<a href="http://maps.google.com/?q=Cinergy+<?php the_field('location_address', $child->ID)?>"><?php the_field('location_address', $child->ID); ?></a>
							</div>
						</div>
					<?php } ?>
        </div>
      </div>
    <?php } elseif ( $post->post_parent == 1481 ) { // CHILD PAGE OF LOCATIONS ?>
      <div class="location-contents">
        <div class="flex-container max-width">
          <div class="one-half">
            <div class="location-contacts">
							<?php
								$remove = array(" ","(",")",".","-");
								$cleanphone = str_replace($remove, "", get_field('location_phone'));
							?>
							<?php if ( get_field('location_phone') ) {
								echo do_shortcode('[cta-btn url="tel:+1'. $cleanphone .'" color="primary" type="phone"]'. get_field('location_phone') .'[/cta-btn]'); 
							} ?>
							<?php if ( get_field('location_address') ) {
								echo do_shortcode('[cta-btn target="_blank" url="http://maps.google.com/?q=Cinergy+'. get_field('location_address') .'" color="primary" type="map"]'. get_field('location_address') .'[/cta-btn]'); 
							} ?>
							<h2>Operating Hours</h2>
							<p class="operating-hours"><?php the_field('location_hours'); ?></p>
            </div>
            <?php the_content() ?>
          </div>
          <div class="one-half">
						<?php 
							global $post;
							$post_slug=$post->post_name;
							$location = ucwords(str_replace('-', ' ', $post_slug));
						?>
          	<?php echo do_shortcode('[cta-btn url="/contact/?cinergylocation='.$location.'" color="primary" type="arrow"]Message Us[/cta-btn]'); ?>
          </div>
        </div>
        <div class="overlay"></div>
      </div>
      <?php if ( is_page(1484) ) { //LOCATION CC ?>
        <div class="location-movies">
          <div class="max-width now filter">
            <h2>Now Showing</h2>
            <div class="btn primary-btn coming-btn"><span>View Coming Soon</span></div>
          </div>
          <div class="max-width coming">
            <h2>Coming Soon</h2>
            <div class="btn primary-btn now-btn"><span>View Now Showing</span></div>
          </div>
          <div class="now filter"><?php echo do_shortcode('[showtimes theater_id="101"][/showtimes]') ?></div>
          <div class="coming"><?php echo do_shortcode('[movies theater_id="101" type="coming_soon"][/movies]') ?></div>
        </div>
      <?php } else if ( is_page(1485) ) { //LOCATION MID ?>
        <div class="location-movies">
          <div class="max-width now filter">
            <h2>Now Showing</h2>
            <div class="btn primary-btn coming-btn"><span>View Coming Soon</span></div>
          </div>
          <div class="max-width coming">
            <h2>Coming Soon</h2>
            <div class="btn primary-btn now-btn"><span>View Now Showing</span></div>
          </div>
          <div class="now filter"><?php echo do_shortcode('[showtimes theater_id="103"][/showtimes]') ?></div>
          <div class="coming"><?php echo do_shortcode('[movies theater_id="103" type="coming_soon"][/movies]') ?></div>
        </div>
      <?php } else if ( is_page(1486) ) { //LOCATION ODES ?>
        <div class="location-movies">
          <div class="max-width now filter">
            <h2>Now Showing</h2>
            <div class="btn primary-btn coming-btn"><span>View Coming Soon</span></div>
          </div>
          <div class="max-width coming">
            <h2>Coming Soon</h2>
            <div class="btn primary-btn now-btn"><span>View Now Showing</span></div>
          </div>
          <div class="now filter"><?php echo do_shortcode('[showtimes theater_id="104"][/showtimes]') ?></div>
          <div class="coming"><?php echo do_shortcode('[movies theater_id="104" type="coming_soon"][/movies]') ?></div>
        </div>
			<?php } else if ( is_page(1483) ) { //LOCATION AMA ?>
        <div class="location-movies">
          <div class="max-width now filter">
            <h2>Now Showing</h2>
            <div class="btn primary-btn coming-btn"><span>View Coming Soon</span></div>
          </div>
          <div class="max-width coming">
            <h2>Coming Soon</h2>
            <div class="btn primary-btn now-btn"><span>View Now Showing</span></div>
          </div>
          <div class="now filter"><?php echo do_shortcode('[showtimes theater_id="105"][/showtimes]') ?></div>
          <div class="coming"><?php echo do_shortcode('[movies theater_id="105" type="coming_soon"][/movies]') ?></div>
				</div>
			<?php } else if ( is_page(3658) ) { //LOCATION TULSA ?>
        <div class="location-movies">
          <div class="max-width now filter">
            <h2>Now Showing</h2>
            <div class="btn primary-btn coming-btn"><span>View Coming Soon</span></div>
          </div>
          <div class="max-width coming">
            <h2>Coming Soon</h2>
            <div class="btn primary-btn now-btn"><span>View Now Showing</span></div>
          </div>
          <div class="now filter"><?php echo do_shortcode('[showtimes theater_id="106"][/showtimes]') ?></div>
          <div class="coming"><?php echo do_shortcode('[movies theater_id="106" type="coming_soon"][/movies]') ?></div>
        </div>
			<?php } else if ( is_page(62605) ) { //LOCATION GRANBURY ?>
        <div class="location-movies">
          <div class="max-width now filter">
            <h2>Now Showing</h2>
            <div class="btn primary-btn coming-btn"><span>View Coming Soon</span></div>
          </div>
          <div class="max-width coming">
            <h2>Coming Soon</h2>
            <div class="btn primary-btn now-btn"><span>View Now Showing</span></div>
          </div>
          <div class="now filter"><?php echo do_shortcode('[showtimes theater_id="202"][/showtimes]') ?></div>
          <div class="coming"><?php echo do_shortcode('[movies theater_id="202" type="coming_soon"][/movies]') ?></div>
        </div>
			<?php } else if ( is_page(62607) ) { //LOCATION  MARBLE FALLS ?>
        <div class="location-movies">
          <div class="max-width now filter">
            <h2>Now Showing</h2>
            <div class="btn primary-btn coming-btn"><span>View Coming Soon</span></div>
          </div>
          <div class="max-width coming">
            <h2>Coming Soon</h2>
            <div class="btn primary-btn now-btn"><span>View Now Showing</span></div>
          </div>
          <div class="now filter"><?php echo do_shortcode('[showtimes theater_id="201"][/showtimes]') ?></div>
          <div class="coming"><?php echo do_shortcode('[movies theater_id="201" type="coming_soon"][/movies]') ?></div>
				</div>
			<?php } else { //NO LOCATION IS SET ?><?php } ?>
			<?php if( have_rows('amenities') ) { ?>
        <div class="location-amenities">
          <h2>Amenities</h2>
          <div class="flex-container max-width">
            <?php while ( have_rows('amenities') ) : the_row(); ?>
              <div class="amenity one-third">
                <?php $icon = get_sub_field('icon'); ?>
                <div class="icon-container">
                    <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
                </div>
                <h3><?php the_sub_field('name'); ?></h3>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      <?php } ?>
    <?php } elseif ( is_page(1467) ) { //SPECIALS ?>
      <div class="deals-container">
        <div class="max-width">
          <?php 
            $active_cat = $_GET['cat'];
            $cat_name = get_the_category_by_ID($active_cat);
            if ( $active_cat ) { 
              if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC 
                echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-cc" meta_compare="LIKE" category="'.$cat_name.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
              } else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID 
                echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-mid" meta_compare="LIKE" category="'.$cat_name.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
              } else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES 
                echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-odes" meta_compare="LIKE" category="'.$cat_name.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION Amarillo
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-ama" meta_compare="LIKE" category="'.$cat_name.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION Tulsa
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-tulsa" meta_compare="LIKE" category="'.$cat_name.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION Tulsa
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-granbury" meta_compare="LIKE" category="'.$cat_name.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION Tulsa
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-mfalls" meta_compare="LIKE" category="'.$cat_name.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else { //NO LOCATION IS SET 
                ?>
                  <h2 class="align-center">Select a location to view specials</h2>
                  <div class="location-option-container">
                    <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
                    <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
                    <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
										<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
										<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
										<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
										<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
									</div>
                <?php
              }
            } else {
              if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC 
                echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-cc" meta_compare="LIKE" scroll="false" button_label="Load More" button_loading_label="loading..."]');
              } else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID 
                echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-mid" meta_compare="LIKE" scroll="false" button_label="Load More" button_loading_label="loading..."]');
              } else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES 
                echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-odes" meta_compare="LIKE" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION AMARILLO
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-ama" meta_compare="LIKE" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION TULSA
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-tulsa" meta_compare="LIKE" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION TULSA
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-granbury" meta_compare="LIKE" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION TULSA
								echo do_shortcode('[ajax_load_more container_type="div" post_type="deals" posts_per_page="6" meta_key="location_based" meta_value="location-mfalls" meta_compare="LIKE" scroll="false" button_label="Load More" button_loading_label="loading..."]');
							} else { //NO LOCATION IS SET 
                ?>
                  <h2 class="align-center">Select a location to view specials</h2>
                  <div class="location-option-container">
                    <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
                    <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
                    <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
										<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
										<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
										<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
										<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
									</div>
                <?php
              }
            }
          ?>
        </div>
      </div>
    <?php } elseif ( is_page(1487) ) { //GAMES & ATTRACTIONS ?>
      <div class="games-container">
        <?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED ?>
          <?php //QUERY ALL PLAY
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Play',
              'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key'     => 'location_based',
                    'value'   => 'location-cc',
                    'compare' => 'LIKE',
                ),
                array(
                    'key'     => 'location_based',
                    'value'   => 'location-mid',
                    'compare' => 'LIKE',
                ),
                array(
                    'key'     => 'location_based',
                    'value'   => 'location-odes',
                    'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-ama',
									'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-tulsa',
									'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-granbury',
									'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-mfalls',
									'compare' => 'LIKE',
								),
              ),
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) { $query->the_post(); ?>
              <?php //GET FEATURED IMAGE
                if ( has_post_thumbnail() ) {
                  $thumb_id = get_post_thumbnail_id();
                  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
                  $thumb_url = $thumb_url_array[0];
                } else {
                  $thumb_url_array = get_field('default_game_image', 'options'); 
                  $thumb_url = $thumb_url_array['url'];
                }
              ?>
              <div class="play-preview post-preview-container" style="background-image: url('<?php echo $thumb_url; ?>');">
                <?php $post_slug = get_post_field( 'post_name', get_post() ); ?>
                <a id="<?php echo $post_slug; ?>" class="styleguide-anchor"></a>
                <div class="flex-container max-width">
                  <div class="one-half">
                    <h2><?php the_title(); ?></h2>
                  </div>
                  <div class="game-link one-half">
                    <p class="intro"><?php the_field('intro'); ?></p>
                    <div class="details">
                      <?php the_content(); ?>
                    </div>
                    <?php  if ( get_the_title() != 'Billiards' ) { ?><p class="more label">more</p><?php } ?>
                  </div>                
                </div>
                <div class="overlay"></div>
                <div class="overlay two"></div>
              </div>
            <?php } ?>
          <?php } ?>
        <?php } else { //LOCATION SET ?>
          <?php //QUERY ALL PLAY
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Play',
              );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) { $query->the_post(); ?>
              <?php $locations = get_field('location_based'); ?>
              <?php foreach( $locations as $location ) { //CHECK ALL DEAL LOCATIONS ?>
                <?php if ( $_COOKIE["CinergyLocation"] == $location ) { //ONLY DISPLAY IF ONE OF THE LOCATIONS EQUALS SET LOCATION ?>
                  <?php //GET FEATURED IMAGE
                    if ( has_post_thumbnail() ) {
                      $thumb_id = get_post_thumbnail_id();
                      $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
                      $thumb_url = $thumb_url_array[0];
                    } else {
                      $thumb_url_array = get_field('default_game_image', 'options'); 
                      $thumb_url = $thumb_url_array['url'];
                    }
                  ?>
                  <div class="play-preview post-preview-container" style="background-image: url('<?php echo $thumb_url; ?>');">
                    <?php $post_slug = get_post_field( 'post_name', get_post() ); ?>
                    <a id="<?php echo $post_slug; ?>" class="styleguide-anchor"></a>
                    <div class="flex-container max-width">
                      <div class="one-half">
                        <h2><?php the_title(); ?></h2>
                      </div>
                      <div class="game-link one-half">
                        <p class="intro"><?php the_field('intro'); ?></p>
                        <div class="details">
                          <?php the_content(); ?>
                        </div>
                        <?php  if ( get_the_title() != 'Billiards' ) { ?><p class="more label">more</p><?php } ?>
                      </div>                
                    </div>
                    <div class="overlay"></div>
                    <div class="overlay two"></div>
                  </div>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          <?php } ?>
          <?php wp_reset_postdata(); ?> 
        <?php } ?>
    <?php } elseif ( is_page(1482) ) { //EAT & DRINK ?>
      <?php get_template_part( 'template-parts/pages/content', 'menu' ); ?>
    <?php } elseif ( is_page(391) ) { //MOVIES ?>
      <div class="movies-container-bg">
        <div class="max-width <?php if ( $_GET['filter'] == 'Sensory-Friendly' ) { ?> sensory <?php } ?>">
          <?php
          if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC 
            if ( $_GET['filter'] == 'Coming Soon' ) {
              echo do_shortcode('[movies theater_id="101" type="coming_soon"][/movies]');
            } elseif ( $_GET['filter'] == 'Sensory-Friendly' ) {
              echo do_shortcode('[movies theater_id="101" type="coming_soon"][/movies]');
            } else {
              echo do_shortcode('[showtimes theater_id="101" type="now_showing"][/showtimes]');
            }
          } else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID 
            if ( $_GET['filter'] == 'Coming Soon' ) {
              echo do_shortcode('[movies theater_id="103" type="coming_soon"][/movies]');
            } elseif ( $_GET['filter'] == 'Sensory-Friendly' ) {
              echo do_shortcode('[movies theater_id="103" type="coming_soon"][/movies]');
            } else {
              echo do_shortcode('[showtimes theater_id="103" type="now_showing"][/showtimes]');
            }
          } else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES 
            if ( $_GET['filter'] == 'Coming Soon' ) {
              echo do_shortcode('[movies theater_id="104" type="coming_soon"][/movies]');
            } elseif ( $_GET['filter'] == 'Sensory-Friendly' ) {
              echo do_shortcode('[movies theater_id="104" type="coming_soon"][/movies]');
            } else {
              echo do_shortcode('[showtimes theater_id="104" type="now_showing"][/showtimes]');
						}
					} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION AMARILLO
            if ( $_GET['filter'] == 'Coming Soon' ) {
              echo do_shortcode('[movies theater_id="105" type="coming_soon"][/movies]');
            } elseif ( $_GET['filter'] == 'Sensory-Friendly' ) {
              echo do_shortcode('[movies theater_id="105" type="coming_soon"][/movies]');
            } else {
              echo do_shortcode('[showtimes theater_id="105" type="now_showing"][/showtimes]');
						}
					} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION TULSA
            if ( $_GET['filter'] == 'Coming Soon' ) {
              echo do_shortcode('[movies theater_id="106" type="coming_soon"][/movies]');
            } elseif ( $_GET['filter'] == 'Sensory-Friendly' ) {
              echo do_shortcode('[movies theater_id="106" type="coming_soon"][/movies]');
            } else {
              echo do_shortcode('[showtimes theater_id="106" type="now_showing"][/showtimes]');
            }
					} else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION GRANBURY
            if ( $_GET['filter'] == 'Coming Soon' ) {
              echo do_shortcode('[movies theater_id="202" type="coming_soon"][/movies]');
            } elseif ( $_GET['filter'] == 'Sensory-Friendly' ) {
              echo do_shortcode('[movies theater_id="202" type="coming_soon"][/movies]');
            } else {
              echo do_shortcode('[showtimes theater_id="202" type="now_showing"][/showtimes]');
						}
					} else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION MARBLE FALLS
            if ( $_GET['filter'] == 'Coming Soon' ) {
              echo do_shortcode('[movies theater_id="201" type="coming_soon"][/movies]');
            } elseif ( $_GET['filter'] == 'Sensory-Friendly' ) {
              echo do_shortcode('[movies theater_id="201" type="coming_soon"][/movies]');
            } else {
              echo do_shortcode('[showtimes theater_id="201" type="now_showing"][/showtimes]');
            }
					} else { //NO LOCATION IS SET ?>
              <div class="location-forced menu-locations">
                <h2>Select a location to view movies</h2>
                <div class="location-option-container">
                  <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
                  <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
                  <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
									<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
									<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
									<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
									<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
								</div>
              </div>
          <?php } ?>
        </div>
      </div>
    <?php } else { ?>
      <?php if ( $post->post_content != "" ) { ?>
        <div class="post-contents max-width">
          <?php the_content(); ?>
        </div>
      <?php } ?>
    <?php } ?>
    <!-- ********************* -->
    <!--      MAIN CONTENT     -->
    <!-- ********************* -->

    <!-- BASIC CONTACT FORM -->
    <?php if ( get_field('basic_contact_form')  == 'yes' ) { ?>
      <?php get_template_part( 'template-parts/pages/content', 'page-contact' ); ?>
    <?php } ?>

    <!-- LOCATION MAPS -->
    <?php if ( is_page(1456) || is_page(1484) || is_page(1485) || is_page(1486) || is_page(1483) || is_page(3658) ) { //CONTACT | CC | Mid | Odes | Ama | Tulsa  ?>
      <div class="contact-map-container">
        <div class="cover"></div>
        <?php if ( is_page(1484) ) { //LOCATION CC ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3415.761188892892!2d-97.87358298439864!3d31.116368881505124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x865ab3f8bc842853%3A0xce79ddcd8b127bf6!2sCinergy+Copperas+Cove!5e0!3m2!1sen!2sus!4v1493920910948" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
        <?php } else if ( is_page(1485) ) { //LOCATION MID ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3384.9844270701915!2d-102.14764508438206!3d31.961319181225903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86fbd9e67bb00f71%3A0xf0523553e54d778e!2sCinergy+Midland+Featuring+EPIC!5e0!3m2!1sen!2sus!4v1493920888363" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
        <?php } else if ( is_page(1486) ) { //LOCATION ODES ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3386.336262374211!2d-102.28685028438275!3d31.92462678123774!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86fbcf919e4aadc9%3A0x446fb60b84b7d2f0!2sCinergy+Odessa+Featuring+EPIC!5e0!3m2!1sen!2sus!4v1493920927945" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				<?php } else if ( is_page(1483) ) { //LOCATION AMA ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3262.1876747190618!2d-101.94679428444732!3d35.15193996642412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8701517504a79727%3A0x97813a5ded2c433c!2sCinergy+Cinemas+%26+Entertainment!5e0!3m2!1sen!2sus!4v1536249160054" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				<?php } else if ( is_page(3658) ) { //LOCATION TULSA ?>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3225.2086622686265!2d-95.89125028473065!3d36.06401428010683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87b68d8ff17aceb1%3A0xced8ddd6c1df9ac9!2s6808+S+Memorial+Dr+%23300%2C+Tulsa%2C+OK+74133!5e0!3m2!1sen!2sus!4v1547747388076" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3415.761188892892!2d-97.87358298439864!3d31.116368881505124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x865ab3f8bc842853%3A0xce79ddcd8b127bf6!2sCinergy+Copperas+Cove!5e0!3m2!1sen!2sus!4v1493920910948" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
        <?php } else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3384.9844270701915!2d-102.14764508438206!3d31.961319181225903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86fbd9e67bb00f71%3A0xf0523553e54d778e!2sCinergy+Midland+Featuring+EPIC!5e0!3m2!1sen!2sus!4v1493920888363" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
        <?php } else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3386.336262374211!2d-102.28685028438275!3d31.92462678123774!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86fbcf919e4aadc9%3A0x446fb60b84b7d2f0!2sCinergy+Odessa+Featuring+EPIC!5e0!3m2!1sen!2sus!4v1493920927945" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION AMA ?>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3262.1876747190618!2d-101.94679428444732!3d35.15193996642412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8701517504a79727%3A0x97813a5ded2c433c!2sCinergy+Cinemas+%26+Entertainment!5e0!3m2!1sen!2sus!4v1536249160054" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION TULSA ?>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3225.2086622686265!2d-95.89125028473065!3d36.06401428010683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87b68d8ff17aceb1%3A0xced8ddd6c1df9ac9!2s6808+S+Memorial+Dr+%23300%2C+Tulsa%2C+OK+74133!5e0!3m2!1sen!2sus!4v1547747388076" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
				<?php } else { //NO LOCATION IS SET ?>

        <?php } ?>
      </div>
    <?php } ?>

    <!-- PAGE GALLERY -->
    <?php if ( get_field('gallery') ) { ?>
      <?php get_template_part( 'template-parts/elements/content', 'page-gallery' ); ?>
    <?php } ?>

    <!-- SHOP CTA -->
    <?php if ( get_field('shop_instructions') && get_field('shop_products') ) { ?>
      <div class="cta-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2017/05/gameroom-1024x614-1.jpg');">
        <div class="vertical-align-child max-width">
          <h2>Get your Fun Card Today!</h2>
          <p class="btn-container">
            <?php echo do_shortcode('[cta-btn url="#fun-cards" color="primary" type="smoothScroll"]buy fun cards[/cta-btn]'); ?>
          </p>
        </div>
        <div class="overlay"></div>
      </div>
    <?php } ?>
  </div>

  <!-- NEWSLETTER CTA -->
  <?php if ( get_field('newsletter_display')  == 'yes' ) { //NOT FUN CARDS?>
    <div class="newsletter cta-container full-width vertical-align-parent dark-bg">
      <a id="newsletter" class="styleguide-anchor"></a>
      <div class="vertical-align-child">
				<div class="max-width">
          <h2>Join the Magic<br>Be Cinergy Elite!</h2>
          <p>Free Popcorn, $5 Elite reward after 300 points, birthday movie ticket, exclusive content, sneak peeks, special offers and so much more!</p>
					<a href="https://tickets.cinergycinemas.com/Browsing/Loyalty/clubs/1" class="btn primary-btn" tabindex="0"><span>Join Now</span></a>
				</div>
      </div>
      <div class="overlay"></div>
      <div class="video-wrap" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.jpg');"> 
        <video muted="" autoplay="" loop="" poster="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.jpg" class="bgvid"> 
					<source src="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.mp4" type="video/mp4">
					<source src="<?php echo get_template_directory_uri() ?>/dist/images/newsletter-bg.webm" type="video/webm">  
        </video> 
      </div>
    </div>
  <?php } ?>

  <!-- SPECIALS SLIDER -->
  <?php if ( !is_page(1466) && get_field('special_slider_display')  == 'yes' ) { //NOT FUN CARDS?>
    <?php get_template_part( 'template-parts/elements/content', 'specials-slider' ); ?>
  <?php } ?>



</main>

<?php get_footer(); ?>
