<?php
/* Template for displaying search form*/
?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="looking for..." value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search-submit"><span><?php echo _x( 'Search', 'submit button', 'twentyseventeen' ); ?></span></button>
</form>
