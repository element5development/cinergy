<?php /*
THE TEMPLATE FOR DISPLAYING SINGLE BLOG POST
*/ ?>

<?php get_header(); ?>

<main class="page-contents full-width">

  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/pages/content', 'page-title' ); ?>

  <!-- BREADCRUMBS -->
  <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

  <!-- ADD PAGE CONTENT -->
  <?php if (have_posts()) : ?>
    <div class="post-contents max-width">
      <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>
  <!-- ADD PAGE CONTENT -->

  <!-- SIMPLE CTA SECTION -->
  <?php if ( get_field('simple_cta_section_cta_link') ) { ?>
    <?php get_template_part( 'template-parts/elements/content', 'simple-cta-section' ); ?>
  <?php } ?>

  <!-- COMMENTS -->
  <div class="comment-container max-width">
    <?php comments_template(); ?> 
  </div>

</main>

<?php get_footer(); ?>
