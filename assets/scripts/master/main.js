var $ = jQuery;

$(document).ready(function () {

	/*ENTRANCE ANIMATIONS*/
	jQuery(".two_col_basic.scroll .one-half:last-child h2, .two_col_basic.scroll .one-half:last-child h3, .two_col_basic.scroll .one-half:last-child h4, .two_col_basic.scroll .one-half:last-child h5, .two_col_basic.scroll .one-half:last-child h6, .two_col_basic.scroll .one-half:last-child p, .two_col_basic.scroll .one-half:last-child blockquote, .two_col_basic.scroll .one-half:last-child a.btn, .two_col_basic.scroll .one-half:last-child li").viewportChecker({
		classToAdd: "visible animated fadeIn",
		offset: 200,
	});
	jQuery(".single-food-item").viewportChecker({
		classToAdd: "visible animated fadeIn",
		offset: 200,
	});

	/* HIDE CAST IF NO ITEMS */
	function hide_cast() {
		if ($('ul.MovieDB__crewList ').children('li').length == 0) {
			$('.cast').hide();
		}
	}
	setTimeout(hide_cast, 2000)

	/*PRIMARY NAVIGATION*/
	$('#menu-open').click(function () {
		$('.open-nav').addClass('open');
	});
	$('#close-menu').click(function () {
		$('.open-nav').removeClass('open');
	});

	/*SEARCH MENU*/
	$('#search-open').click(function () {
		$('.open-search').addClass('open');
	});
	$('#close-search').click(function () {
		$('.open-search').removeClass('open');
	});

	/* LOCATION SELECTION */
	$('.location-options.label ').click(function () {
		$(this).toggleClass('active');
		$('.location-option-container').toggleClass('active');
		$('.pulse').toggleClass('active');
	});

	/*LOCATION MOVIE FILTER*/
	$('.coming-btn').click(function () {
		$('.now').removeClass('filter');
		$('.coming').addClass('filter');
	});
	$('.now-btn').click(function () {
		$('.now').addClass('filter');
		$('.coming').removeClass('filter');
	});

	/* DATE SELECTION */
	$('.active-date').click(function () {
		$('.active-date svg').toggleClass('open');
		$('.date-selection').toggleClass('open');
	});
	$('.date-selection button').click(function () {
		$('.active-date svg').toggleClass('open');
		$('.date-selection').toggleClass('open');
	});

	/* VALIDATION */
	$('select').each(function () {
		$(this).addClass('');
	}).on('change', function (ev) {
		$(this).attr('class', '').addClass('LV_valid_field medium gfield_select');
	});
	if ('.ginput_container_phone input.value') {
		$(this).addClass('LV_valid_field');
	}

	/* HOME ADDITIONS */
	$('.home-additions .one-fifth.contracted').click(function () {
		$('.home-additions').addClass('expand');
		$('.home-additions .one-fifth').removeClass('expand').addClass('contracted');
		$(this).addClass('expand').removeClass('contracted');;
	});
	$('#close-additions').click(function () {
		$('.home-additions').removeClass('expand');
		$('.home-additions .one-fifth').removeClass('expand').addClass('contracted');
	});

	/*SINGLE MOVIE R RATING LIGHTBOX*/
	$('#close-alert').click(function () {
		$('#alert-rating').removeClass('active');
	});


	/* SPEACIALS PREVIEW */
	$('body').on('click', '.special-preview', function () {
		$('.special-preview').removeClass('active');
		$(this).addClass('active');
	});

	/* PLAY PREVIEW */
	$('body').on('click', '.game-link', function () {
		$(this).toggleClass('active');
	});

	/* POST PREVIEW */
	$(document).on({
		mouseenter: function () {
			$('.post-preview').addClass('fade');
			$(this).addClass('hover');
		},
		mouseleave: function () {
			$('.post-preview').removeClass('fade');
			$(this).removeClass('hover');
		}
	}, ".post-preview"); //pass the element as an argument to .on

	/*PAGE TITLE SLIDER*/
	$('.page-title-slider').slick({
		autoplay: true,
		autoplaySpeed: 6000,
		arrows: false,
		dots: true,
		fade: true,
	});

	/*GALLERY SLIDER*/
	$('.gallery-container').slick({
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: false,
		dots: true,
		fade: true,
	});

	/*SPECIALS SLIDER*/
	$('.special-slider-bg').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.special-slider'
	});
	$('.special-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.special-slider-bg',
		dots: true,
		centerMode: true,
		focusOnSelect: true,
		responsive: [{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
					centerMode: true,
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
					centerMode: false,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
					centerMode: true,
				}
			},
		]
	});

	/* STICKY PORTIONS*/
	var window_width = jQuery(window).width();
	if (window_width < 750) {
		//DISABLE STICKY PARTS ON MOBILE
		jQuery(".sticky").trigger("sticky_kit:detach");
		jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
	} else {
		//ENABLE STICKY PARTS
		jQuery(".sticky").stick_in_parent({
			offset_top: 140
		});
		jQuery("#styleguide-sidebar").stick_in_parent({
			offset_top: 140
		});
	}
	jQuery(window).resize(function () {
		window_width = jQuery(window).width();
		if (window_width < 750) {
			//DISABLE STICKY PARTS ON MOBILE
			jQuery(".sticky").trigger("sticky_kit:detach");
			jQuery("#styleguide-sidebar").trigger("sticky_kit:detach");
		} else {
			//ENABLE STICKY PARTS
			jQuery(".sticky").stick_in_parent({
				offset_top: 140
			});
			jQuery("#styleguide-sidebar").stick_in_parent({
				offset_top: 140
			});
		}
	});

	/* LANDER NOTIFICATION BAR */
	$('#close-notification').on('click', function () {
		$(this).parent().addClass('is-closed');
	});

	/* SPEACIALS ARCHIVE NO POSTS FOUND */
	jQuery(function () {
		$.fn.almEmpty = function (alm) {
			var el = $(alm.content),
				msg = '<h3 style="text-align:center;">Sorry, nothing was for for this location.</h3>';
			el.append(msg); // Append to ALM 
			$(this).parent().addClass('is-closed');
		};
	});

});