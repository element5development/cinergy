<?php /* 
MAIN BLOG PAGE 
*/ ?>

<?php get_header(); ?>

<main class="page-cotents full-width">
  
  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/pages/content', 'page-title' ); ?>

  <!-- BREADCRUMBS -->
  <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

  <!-- SECONDARY NAV -->
  <?php get_template_part( 'template-parts/navigation/content', 'secondary-nav' ); ?>

  <!-- AJAX LOAD POSTS -->
  <div class="feed-container blog">
    <div class="max-width flex-container">
      <div class="posts-feed">
        <?php if ( is_archive() ) {
          $cat = get_category( get_query_var( 'cat' ) );
          $cat_slug = $cat->slug;
          echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="4" category="'.$cat_slug.'" scroll="false" button_label="Load More" button_loading_label="loading..."]');
        } else {
          echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="4" scroll="false" button_label="Load More" button_loading_label="loading..."]');
        } ?>
      </div>
    <div class="sidebar">
      <?php if ($_COOKIE['CinergyLocation'] == 'location-cc') { ?>
        <div id="TA_selfserveprop462" class="TA_selfserveprop widget">
        <ul id="sLbrsvV4UX" class="TA_links Ygu2RH713TZu">
        <li id="vp2xawB5" class="9CpaUebOP">
        <a target="_blank" href="https://www.tripadvisor.com.sg/"><img src="https://www.tripadvisor.com.sg/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
        </li>
        </ul>
        </div>
        <script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=462&amp;locationId=5970853&amp;lang=en_SG&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;display_version=2"></script>
      <?php } else if ($_COOKIE['CinergyLocation'] == 'location-mid') { ?>
        <div id="TA_selfserveprop417" class="TA_selfserveprop widget">
        <ul id="M2KSQ5rq" class="TA_links NnMaikU">
        <li id="iiMJR7Q2" class="4o3UCEJQaI">
        <a target="_blank" href="https://www.tripadvisor.com.sg/"><img src="https://www.tripadvisor.com.sg/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
        </li>
        </ul>
        </div>
        <script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=417&amp;locationId=5866280&amp;lang=en_SG&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;display_version=2"></script>
      <?php } else if ($_COOKIE['CinergyLocation'] == 'location-odes') { ?>
        <div id="TA_selfserveprop232" class="TA_selfserveprop">
        <ul id="sTJFWdg" class="TA_links nlE1sAfU3">
        <li id="hcjp4Smzil" class="O15SatO">
        <a target="_blank" href="https://www.tripadvisor.com.sg/"><img src="https://www.tripadvisor.com.sg/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a>
        </li>
        </ul>
        </div>
        <script src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=232&amp;locationId=12093973&amp;lang=en_SG&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;display_version=2"></script>
      <?php } else { ?>
      <?php } ?>
      <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('blog-sidebar')) : else : ?>
        <p><strong>Widget Ready</strong></p>  
      <?php endif; ?>  
    </div>
  </div>

  <!-- SPECIALS SLIDER -->
  <?php get_template_part( 'template-parts/elements/content', 'specials-slider' ); ?>

</main>

<?php get_footer(); ?>