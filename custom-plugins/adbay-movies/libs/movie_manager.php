<?php

defined( 'MOVIE_BASE_DIR' ) OR exit;

if ( !function_exists( 'add_action' ) ) {

	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';

	exit;

}



class Adbay_Movie_Manager

{

	protected static $instance;



	const MOVIE_ID_TBL = 'adbay_movie_ids';

	const SHOWTIMES_TBL='adbay_movie_showtimes';

	const THEATERS_TBL = 'adbay_movie_theaters';



	public function __construct()

	{



	}



	public static function instance()

	{

		if (!isset(self::$instance)) {

			$className = __CLASS__;

			self::$instance = new $className;

		}

		return self::$instance;

	}


	public static function get_theater_title($id)
	{
		global $wpdb;


		$title = $wpdb->get_var($wpdb->prepare("SELECT title FROM ".$wpdb->prefix.self::THEATERS_TBL." WHERE ts_theater_id=%d ",$id));

		if(empty($title))

		{

			return FALSE;

		}

		return $title;
	}


	public static function add_movie($movie)

	{

		//if(!self::movie_id_exists($movie) && !self::is_session_event($movie['title']))

		if(!self::movie_id_exists($movie))

		{

			//am_log("Movie id does not exist: ".$movie['title']);

			$movie_post = self::find_movie_post_id($movie['title']);

			if(!$movie_post)

			{

				//am_log("Movie post does not exist: ".$movie['title']);

				$movie_post = self::add_movie_post($movie);

			}

			self::add_movie_id($movie_post,$movie);

		}

	}



	public static function is_session_event($title)

	{

		if(stripos($title,'Laser Tag') !== FALSE)

		{

			return TRUE;

		}

		else if(stripos($title,'SW Ropes') !== FALSE)

		{

			return TRUE;

		}

		else if(stripos($title,'SW Ropes Course') !== FALSE)

		{

			return TRUE;

		}

		return FALSE;

	}



	public static function is_laser_tag($title)

	{

		if(stripos($title,'Laser Tag') !== FALSE)

		{

			return TRUE;

		}

		return FALSE;

	}



	public static function is_sw_ropes($title)

	{

		if(stripos($title,'SW Ropes') !== FALSE)

		{

			return TRUE;

		}

		else if(stripos($title,'SW Ropes Course') !== FALSE)

		{

			return TRUE;

		}

		return FALSE;

	}



	public static function movie_id_exists($movie)

	{

		global $wpdb;

		//First check if the movie is in the movie id table.

		$result = $wpdb->get_var('SELECT id FROM ' . $wpdb->prefix.self::MOVIE_ID_TBL."WHERE ts_movie_id='{$movie['ts_movie_id']}'");

		//am_log("Movie search results:".print_r($result,true));

		if(empty($result))

		{

			return FALSE;

		}

		return TRUE;

	}



	public static function find_movie_post_id($movie_title)

	{

		global $wpdb;



		$title = self::trim_movie_title($movie_title);



		$movie_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM ".$wpdb->prefix."posts WHERE post_type=%s AND post_title=%s",MoviePostType::POST_TYPE,$title));

		if(empty($movie_id))

		{

			return FALSE;

		}

		return $movie_id;

	}



	public static function is_escape_room($movie_title)
	{
		return (strpos($movie_title, 'Escape Room -') !== false) ? true : false;
	}



	public static function trim_movie_title($title)

	{

		$title = str_replace('IN EPIC 3D','',$title);

		$title = str_replace('IN EPIC','',$title);

		$title = str_replace('IN 3D','',$title);

		$title = str_replace(': ',':', $title);

		$title = str_replace(':',': ',$title);

		$title = trim($title);

		return $title;

	}



	public static function find_movie_type($title)

	{

		$type=0;

		if(strpos($title,'IN EPIC 3D') !== FALSE)

		{

			$type=3;

		}

		else if(strpos($title,'IN EPIC') !== FALSE)

		{

			$type=2;

		}

		else if(strpos($title,'IN 3D') !== FALSE)

		{

			$type=1;

		}

		if(self::is_session_event($title))

		{

			if(self::is_laser_tag($title))

			{

				$type = -1;

			}

			else if(self::is_sw_ropes($title))

			{

				$type = -2;

			}

		}

		return $type;

	}



	public static function add_movie_post($movie)

	{

		global $wpdb;

		$post = array(

		    'post_author' => 1,

		    'post_date' => date('Y-m-d H:i:s'),

		    'post_date_gmt' => date('Y-m-d H:i:s'),

		    'post_content' => $movie['description'],

		    'post_title' => self::trim_movie_title($movie['title']),

		    'post_name' => sanitize_title(self::trim_movie_title($movie['title'])),

		    'post_excerpt' => $movie['description'],

		    'post_status' => 'publish',

		    'comment_status' => 'closed',

		    'ping_status' => 'closed',

		    'post_modified' => date('Y-m-d H:i:s'),

		    'post_modified_gmt' => date('Y-m-d H:i:s'),

		    'post_parent' => 0,

		    'post_type' => MoviePostType::POST_TYPE,

		    'comment_count' => 0

		);



		$post_ID = wp_insert_post($post);



		//Add meta data.

		add_post_meta($post_ID,MoviePostType::PREFIX.'start_date',$movie['start_date'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'movie_id',$movie['movie_id'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'runtime',$movie['runtime'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'rating',$movie['rating'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'rating_description',$movie['rating_description'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'description',$movie['description'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'site_link',$movie['site_link'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'poster_link',$movie['poster_link'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'theater_count',$movie['theater_count'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'image',$movie['image'],TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'trailer','',TRUE);

		add_post_meta($post_ID,MoviePostType::PREFIX.'ytID','',TRUE);



		$cat = array();

		if(!self::is_session_event($movie['title']))

		{

			$cat[] = 68;

		}

		else

		{

			$cat[] = 69;

		}

		wp_set_post_categories($post_ID,$cat,false);



		return $post_ID;

	}



	public static function add_movie_id($post_ID,$movie)

	{

		global $wpdb;

		//am_log("Adding movie id: ".$post_ID." ".$movie['title']);

		$type = self::find_movie_type($movie['title']);



		$movie_entry = array();

		$movie_entry['flag'] = $type;

		$movie_entry['post_ID'] = $post_ID;

		$movie_entry['movie_id'] = $movie['movie_id'];

		$movie_entry['ts_movie_id']=$movie['ts_movie_id'];

		$movie_entry['title'] = $movie['title'];

		$movie_entry['start_date']=$movie['start_date'];



		$wpdb->insert($wpdb->prefix.self::MOVIE_ID_TBL,$movie_entry,array('%d','%d','%s','%s','%s','%s'));

	}



	public static function add_showtime($showtime)

	{

		global $wpdb;
		$showtimes = $showtime; //Array of Showtimes(array)
		$single_showtime = array();

		foreach($showtimes as $row => $entry){
			array_push($single_showtime, $entry);
		}

		$theater_var = (string)$single_showtime[2][0];
		$theater_final = (int)$theater_var;

		$showtime_entry = array();
		$showtime_entry['schedule'] = $single_showtime[0];
		$showtime_entry['seats'] = $single_showtime[11];
		$showtime_entry['movie'] = $single_showtime[1];
		$showtime_entry['hour'] = $single_showtime[3];
		$showtime_entry['min'] = $single_showtime[4];
		$showtime_entry['apm'] = $single_showtime[5];
		$showtime_entry['theater'] = $theater_final;
		$showtime_entry['date'] = $single_showtime[6];
		$showtime_entry['order_date'] = $single_showtime[9];
		$showtime_entry['vip'] = $single_showtime[8];
		$showtime_entry['link'] = $single_showtime[7];

		$wpdb->insert($wpdb->prefix.self::SHOWTIMES_TBL,$showtime_entry,array('%d','%d','%s','%d','%d','%s','%d','%s','%s','%d','%s'));

	}



	public static function edit_showtime($showtime)

	{

		global $wpdb;



		$wpdb->update($wpdb->prefix.self::SHOWTIMES_TBL,$showtime,$where,array(),array());



	}



	public static function check_showtime($schedule,$date,$theater)

	{

		global $wpdb;



		$result = $wpdb->get_row("SELECT id FROM ".$wpdb->prefix.self::SHOWTIMES_TBL." WHERE schedule=".$schedule." AND date=\"".$date."\" AND theater=".$theater,ARRAY_A);

		if(empty($result))

		{

			return FALSE;

		}

		return $result;

	}



	public static function delete_movie_data($post_ID)

	{

		//Get related movie ids

		$movies = self::get_related_movie_ids($post_ID);



		//Remove showtimes.

		foreach($movies as $movie)

		{

			self::delete_related_showtimes($movie);

		}



		//Remove movie ids

		self::delete_related_movie_ids($post_ID);

	}



	public static function delete_related_movie_ids($post_ID)

	{

		global $wpdb;

		$wpdb->delete($wpdb->prefix.self::MOVIE_ID_TBL,array('post_ID'=>$post_ID),array('%d'));

	}


	/*
		THE TABLE DOES NOT CONTAIN ANY RELATED ID ONLY 0
	*/

	public static function get_related_movie_ids($post_ID)

	{

		global $wpdb;

		return $wpdb->get_col($wpdb->prepare("SELECT ts_movie_id FROM ".$wpdb->prefix.self::MOVIE_ID_TBL." WHERE post_ID=%d",$post_ID));

	}



	public static function delete_related_showtimes($movie_id)

	{

		global $wpdb;

		$wpdb->delete($wpdb->prefix.self::SHOWTIMES_TBL,array('movie'=>$movie_id),array('%s'));

	}



	public static function remove_all_movies()

	{

		global $wpdb;

		$postids = $wpdb->get_col($wpdb->prepare("SELECT ID FROM ".$wpdb->prefix."posts WHERE post_type=%s",MoviePostType::POST_TYPE));



		foreach($postids as $ID)

		{

			wp_delete_post($ID);

		}



		self::delete_ancillary_data();

	}



	//Used to delete everything BUT the movie posts.

	public static function delete_ancillary_data()

	{

		global $wpdb;

		$wpdb->query('TRUNCATE '.$wpdb->prefix.self::MOVIE_ID_TBL);

		$wpdb->query('TRUNCATE '.$wpdb->prefix.self::SHOWTIMES_TBL);

	}





	/*

	* Movie select queries.

	*/

	public static function get_now_playing($escape_room='no', $type=NULL)

	{

		global $wpdb;

		$sel=array(

			//'DISTINCT(a.ts_movie_id) AS ID',

			'a.start_date',

			'a.flag AS epic',

			'b.post_title AS title',

			'b.ID',

			'b.post_name'

		);

		$select = implode(',',$sel);

		$query = "SELECT ".$select;

		$query.=" FROM ".$wpdb->prefix.self::MOVIE_ID_TBL." AS a";

		$query .= " LEFT JOIN ".$wpdb->prefix."posts AS b ON a.post_ID=b.ID";

		$query .= " RIGHT JOIN ".$wpdb->prefix.self::SHOWTIMES_TBL." AS c ON a.ts_movie_id=c.movie";

		$query .= " WHERE c.date='".date('Y-m-d')."' AND a.flag>-1";

		if (strtolower($escape_room) == 'yes') {
			$query .= " AND b.post_title LIKE 'Escape Room -%'";
		} else {
			$query .= " AND b.post_title NOT LIKE 'Escape Room -%'";
		}

		if($type != null && is_numeric($type))

		{

			$query .= " AND (a.flag=".$type." OR a.flag=3)";

		}

		else

		{

			$type=NULL;

		}

		$query .=" GROUP BY b.ID ORDER BY a.flag DESC,a.start_date DESC";

		$prepared = $wpdb->prepare($query,$type);

		$results = $wpdb->get_results($prepared,ARRAY_A);



		return self::compile_movie_meta($results);

	}



	public static function get_coming_soon($escape_room='no', $theater_id=NULL)

	{

		global $wpdb;

		$sel = array(

			'a.ts_movie_id',

			'a.start_date',

			'a.flag AS epic',

			'b.post_title AS title',

			'b.ID',

			'b.post_name'

		);

		$select = implode(',',$sel);

		$query = "SELECT ".$select;

		if($theater_id!=NULL)

		{

			$query .=", d.theater, MIN(d.date) AS show_date";

		}

		$query .= " FROM ".$wpdb->prefix.self::MOVIE_ID_TBL." AS a";



		$query .= " LEFT JOIN ".$wpdb->prefix."posts AS b ON a.post_ID=b.ID";



		if($theater_id != NULL)

		{

			$query .= " RIGHT JOIN ".$wpdb->prefix.self::SHOWTIMES_TBL." AS d ON a.ts_movie_id=d.movie";

		}

		$query .= " WHERE a.start_date >= DATE_ADD(CURDATE(), INTERVAL 1 DAY) AND a.start_date < DATE_ADD(CURDATE(), INTERVAL 12 WEEK) AND a.flag>-1";


		if (strtolower($escape_room) == 'yes') {
			$query .= " AND b.post_title LIKE 'Escape Room -%'";
		} else {
			$query .= " AND b.post_title NOT LIKE 'Escape Room -%'";
		}

		if($theater_id!=NULL)

		{

			$query .= " AND d.theater=%d";

		}

		$query .= " GROUP BY b.ID ORDER BY a.flag DESC, a.start_date ASC";



		$prepared = $wpdb->prepare($query,$theater_id);

		//echo $prepared;

		$results = $wpdb->get_results($prepared,ARRAY_A);



		return self::compile_movie_meta($results);

	}



	public static function compile_movie_meta($results)

	{

		$movies = array();

		//Get meta on each result.

		foreach($results as $res)

		{

			$meta = get_post_meta($res['ID']);

			$movie = $res;

			$movie['rating'] = $meta[MoviePostType::PREFIX."rating"][0];

			$movie['rating_description'] = $meta[MoviePostType::PREFIX."rating_description"][0];

			$movie['description'] = $meta[MoviePostType::PREFIX."description"][0];

			$movie['image'] = $meta[MoviePostType::PREFIX."image"][0];

			$movies[] = $movie;

		}

		return $movies;

	}



	public static function create_movie_date_block($movie_id)

	{

		global $wpdb;

		//Look up the future dates for the related ticketsoft movies to this movie id.

		$result = $wpdb->get_col('SELECT DISTINCT(b.date) AS date FROM '.$wpdb->prefix.self::MOVIE_ID_TBL.' AS a LEFT JOIN '.$wpdb->prefix.self::SHOWTIMES_TBL.' AS b ON a.ts_movie_id=b.movie WHERE a.post_ID='.$movie_id.' AND b.date>=DATE_ADD(CURDATE(),INTERVAL -24 HOUR) ORDER BY b.date ASC');

    	$dates = array();

		//Display resulting dates from list.

		if(!empty($result))

		{

			$today = date('Y-m-d');

			$six_days = strtotime('+6 days');

			foreach($result as $date)

			{

				if($date == $today)

				{

					$date_name = 'Today';

				}

				else if(($strtime = strtotime($date)) <= $six_days)

				{

					$date_name = date('l',$strtime);

				}

				else

				{

					$date_name = date('M j',$strtime);

				}

				$dates[]=array('date'=>$date,'button'=>$date_name);

				//echo '<button type="button" class="am_date_picker" value="'.$date.'">'.$date_name.'</button>';

			}

		}

		return $dates;

	}



	public static function create_theater_date_block($escape_room, $movie_id)

	{

		global $wpdb;



		//Look up the future dates for the related ticketsoft movies to this movie id.

		$query = 'SELECT DISTINCT(a.date) AS date FROM '.$wpdb->prefix.self::SHOWTIMES_TBL.' AS a WHERE a.theater='.$movie_id.' AND a.date>=DATE_ADD(CURDATE(),INTERVAL -24 HOUR) ';
		$query .= 'ORDER BY a.date ASC';

		$result = $wpdb->get_col($query);

		//Display resulting dates from list.

		$dates = array();

		if(!empty($result))

		{

			$today = date('Y-m-d');

			$six_days = strtotime('+6 days');

			foreach($result as $date)

			{

				if($date == $today)

				{

					$date_name = 'Today';

				}

				else if(($strtime = strtotime($date)) <= $six_days)

				{

					$date_name = date('l',$strtime);

				}

				else

				{

					$date_name = date('M j',$strtime);

				}

				$dates[]=array('date'=>$date,'button'=>$date_name);

				//echo '<button type="button" class="am_date_picker" value="'.$date.'">'.$date_name.'</button>';

			}

		}

		return $dates;

	}



	public static function get_movie_showtimes($movie_id,$date, $escape_room='no')

	{

		global $wpdb;



		$movies = self::get_related_movie_ids($movie_id);

		$sel = array();

		$sel[] = 'a.flag';

		$sel[] = 'a.title';

		$sel[] = 'a.ts_movie_id';

		$sel[] = 'b.title AS theater';

		$sel[] = 'b.address';

		$sel[] = 'b.city';

		$sel[] = 'b.state';

		$sel[] = 'b.phone';

		$sel[] = 'c.hour';

		$sel[] = 'c.min';

		$sel[] = 'c.apm';

		$sel[] = 'c.theater AS theater_id';

		$sel[] = 'c.link';

		$sel[] = 'c.schedule';

		$sel[] = 'c.date';

		$sel[] = 'c.seats';

		$select = implode(',',$sel);

		$query = "SELECT ".$select;

		$query .= " FROM ".$wpdb->prefix.self::MOVIE_ID_TBL." AS a ";

		$query .= "LEFT JOIN ".$wpdb->prefix.self::SHOWTIMES_TBL." AS c ON c.movie=a.ts_movie_id ";

		$query .= "LEFT JOIN ".$wpdb->prefix.self::THEATERS_TBL." AS b ON c.theater=b.ts_theater_id ";
		
		
		$query .= " WHERE c.date=%s AND b.id IS NOT NULL ";
		
		if (!empty($movies)) {
			$movieIds = array_map(function($movie) {
				return "'{$movie}'";
			}, $movies);
			$query .= " AND a.ts_movie_id IN (" . implode(',', $movieIds) . ") ";
		}
		

		if (strtolower($escape_room) == 'yes') {
			$query .= " AND a.title LIKE 'ESCAPE ROOM -%'";
		} else {
			$query .= " AND a.title NOT LIKE 'ESCAPE ROOM -%'";
		}

		$query .= "ORDER BY b.id ASC,a.flag DESC,c.order_date ASC";



		$prep = $wpdb->prepare($query,$date);
		//echo $prep;
		//exit;
		$results = $wpdb->get_results($prep,ARRAY_A);

		return $results;
	}



	public static function get_theater_showtimes($theater_id,$date,$type=NULL, $escape_room = 'no')

	{

		global $wpdb;



		//First get the movies for this theater ordered by flag.

		$sel = array();

		$sel[] = 'd.post_title';

		$sel[] = 'd.post_name';

		$sel[] = 'a.post_ID';



		$select = implode(',',$sel);

		$query = "SELECT ".$select;

		$query .= " FROM ".$wpdb->prefix.self::MOVIE_ID_TBL." AS a ";

		$query .= "LEFT JOIN ".$wpdb->prefix.self::SHOWTIMES_TBL." AS c ON c.movie=a.ts_movie_id ";

		$query .= "LEFT JOIN ".$wpdb->prefix."posts AS d ON d.ID=a.post_ID ";

		$query .= " WHERE c.theater=%d AND c.date=%s";

		if($type!=NULL)

		{

			$query .= " AND a.flag <0";

		}

		else

		{

			$query .= " AND a.flag >=0";

		}

		if (strtolower($escape_room) == 'yes') {
			$query .= " AND d.post_title LIKE 'Escape Room -%'";
		} else {
			$query .= " AND d.post_title NOT LIKE 'Escape Room -%'";
		}
		$query .= " GROUP BY a.post_ID ORDER BY a.flag DESC,a.start_date DESC";




		$prep = $wpdb->prepare($query,$theater_id,$date);

		$movie_results = $wpdb->get_results($prep,ARRAY_A);

// print_r($movie_results);
// die;


		//Then get showtimes for this day/theater.

		$sel = array();

		$sel[] = 'a.flag';

		$sel[] = 'a.title';

		$sel[] = 'a.post_ID';

		$sel[] = 'a.ts_movie_id';

		$sel[] = 'c.hour';

		$sel[] = 'c.min';

		$sel[] = 'c.apm';

		$sel[] = 'c.link';

		$sel[] = 'c.schedule';

		$sel[] = 'c.date';

		$sel[] = 'c.theater AS theater_id';



		$select = implode(',',$sel);

		$query = "SELECT ".$select;

		$query .= " FROM ".$wpdb->prefix.self::MOVIE_ID_TBL." AS a ";

		$query .= "LEFT JOIN ".$wpdb->prefix.self::SHOWTIMES_TBL." AS c ON c.movie=a.ts_movie_id ";

		$query .= "LEFT JOIN ".$wpdb->prefix."posts AS d ON d.ID=a.post_ID ";

		$query .= " WHERE c.theater=%d AND c.date=%s";

		if($type!=NULL)

		{

			$query .= " AND a.flag <0";

		}

		else

		{

			$query .= " AND a.flag >=0";

		}

		$query .= " ORDER BY a.flag DESC,c.movie ASC,c.order_date ASC";



		$prep = $wpdb->prepare($query,$theater_id,$date);

		$times = $wpdb->get_results($prep,ARRAY_A);



		$movies = array();

		//Loop through and match them.

		for($i=0;$i<count($movie_results);$i++)

		{

			$movies[$i]=$movie_results[$i];

			$movies[$i]['showings'] = array();

			$showtimes=array();

			foreach($times AS $time)

			{

				if($time['post_ID']==$movies[$i]['post_ID'])

				{

					if(!isset($showtimes[$time['ts_movie_id']]))

					{

						$showtimes[$time['ts_movie_id']] = array();

						$showtimes[$time['ts_movie_id']]['flag'] = $time['flag'];

						$showtimes[$time['ts_movie_id']]['times'] = array();

					}

					$showtimes[$time['ts_movie_id']]['times'][] = am_format_showtime_link($time);

				}

			}

			$movies[$i]['showings'] = $showtimes;

		}



		return $movies;

	}



	public static function get_amusement($type=0)

	{

		global $wpdb;

		$sel=array(

			//'DISTINCT(a.ts_movie_id) AS ID',

			'a.start_date',

			'a.flag AS epic',

			'b.post_title AS title',

			'b.ID',

			'b.post_name'

		);

		$select = implode(',',$sel);

		$query = "SELECT ".$select;

		$query.=" FROM ".$wpdb->prefix.self::MOVIE_ID_TBL." AS a";

		$query .= " LEFT JOIN ".$wpdb->prefix."posts AS b ON a.post_ID=b.ID";

		$query .= " RIGHT JOIN ".$wpdb->prefix.self::SHOWTIMES_TBL." AS c ON a.ts_movie_id=c.movie";

		$query .= " WHERE c.date='".date('Y-m-d')."'";

		if($type != 0 && $type < 0)

		{

			$query .= ' AND a.flag = '.$type;

		}

		else

		{

			$query .= ' AND a.flag < 0 ';

		}

		$query .="GROUP BY b.ID ORDER BY a.flag DESC,a.start_date DESC";

		//echo $query;

		//return;

		$prepared = $wpdb->prepare($query,$type);

		$results = $wpdb->get_results($prepared,ARRAY_A);



		return self::compile_movie_meta($results);

	}





	public static function install()

	{

		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		am_log("Running database install.");

		$movie_id_sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix.self::MOVIE_ID_TBL." (

  id int(10) unsigned NOT NULL AUTO_INCREMENT,

  post_ID int(10) unsigned NOT NULL,

  movie_id int(10) unsigned NOT NULL,

  ts_movie_id int(10) unsigned NOT NULL,

  start_date date NOT NULL,

  title varchar(255) COLLATE utf8_unicode_ci NOT NULL,

  flag tinyint(4) NOT NULL DEFAULT '0',

  PRIMARY KEY (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";

		dbDelta($movie_id_sql);

		am_log('Created movie table');

		$showtimes_sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix.self::SHOWTIMES_TBL." (

  id int(10) unsigned NOT NULL AUTO_INCREMENT,

  schedule mediumint(8) unsigned NOT NULL,

  movie smallint(4) unsigned NOT NULL,

  hour tinyint(2) NOT NULL,

  min tinyint(2) NOT NULL,

  apm varchar(2) COLLATE utf8_unicode_ci NOT NULL,

  theater smallint(4) unsigned NOT NULL,

  date date NOT NULL,

  order_date datetime NOT NULL,

  vip tinyint(1) NOT NULL DEFAULT '0',

  link varchar(255) COLLATE utf8_unicode_ci NOT NULL,

  updated timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (id)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";

		dbDelta($showtimes_sql);

		am_log('Created showtimes table');

		$theater_sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix.self::THEATERS_TBL." (

  id int(10) unsigned NOT NULL AUTO_INCREMENT,

  title varchar(255) COLLATE utf8_unicode_ci NOT NULL,

  address varchar(255) COLLATE utf8_unicode_ci NOT NULL,

  address2 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,

  city varchar(100) COLLATE utf8_unicode_ci NOT NULL,

  state varchar(2) COLLATE utf8_unicode_ci NOT NULL,

  zip varchar(5) COLLATE utf8_unicode_ci NOT NULL,

  ts_theater_id smallint(3) NOT NULL,

  phone varchar(12) COLLATE utf8_unicode_ci NOT NULL,

  PRIMARY KEY (id)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";

		dbDelta($theater_sql);

		am_log('Created theater table');

		$rows = array();

		$rows[] = array('title'=>'CINERGY MIDLAND',

						'address'=>'1917 Liberty Dr',

						'address2'=>'',

						'city'=>'Midland',

						'state'=>'TX',

						'zip'=>'79706',

						'ts_theater_id'=>'103',

						'phone'=>'432-689-8180');

		$rows[] = array('title'=>'Cinergy Copperas Cove',

						'address'=>'402 Constitution Drive',

						'address2'=>'',

						'city'=>'Copperas Cove',

						'state'=>'TX',

						'zip'=>'76522',

						'ts_theater_id'=>'101',

						'phone'=>'254-542-4900');

		$rows[] = array('title'=>'Cinergy Corsicana',

						'address'=>'3501 Corsicana Crossing',

						'address2'=>'',

						'city'=>'Corsicana',

						'state'=>'TX',

						'zip'=>'75109',

						'ts_theater_id'=>'102',

						'phone'=>'903-874-3456');

		foreach($rows as $row)

		{

			am_log('Added '.$row['title']);

			$wpdb->insert($wpdb->prefix.self::THEATERS_TBL,$row,array('%s','%s','%s','%s','%s','%s','%d','%s'));

		}

		am_log('Added rows');

	}

}