<?php
/**
* @package Adbay Movie Integration
*/
/*
Plugin Name: Adbay Movie Integration System
Plugin URI: http://adbay.com
Description: Integrates the Ticketsoft movie feeds into your Wordpress site.
Version: 1.0
Author: Adbay.com
Author URI: http://adbay.com
License: Private LIcense. All Rights Reserved.
*/
//Make sure we don't expose any info if called directly.
defined( 'ABSPATH' ) OR exit;
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define('MOVIE_BASE_FILE', __FILE__);
define('MOVIE_BASE_DIR',dirname(MOVIE_BASE_FILE));
define('MOVIE_PLUGIN_URL', plugin_dir_url( __FILE__ ));

date_default_timezone_set('America/North_Dakota/Center');

require_once MOVIE_BASE_DIR.'/libs/movie_manager.php';
require_once MOVIE_BASE_DIR.'/post-types/movies.php';

class Adbay_Movie_Plugin
{
	const VERSION='1.0';
	const VERSION_OPT = "amp_version";
	const PREFIX='amp_';

	private static $instance;

	public function __construct()
	{
		add_action('init',array(&$this,'init'));
		add_action('admin_menu',array(&$this,'add_settings_page'));
	}

	public static function instance()
	{
		if (!isset(self::$instance)) {
			$className = __CLASS__;
			self::$instance = new $className;
		}
		return self::$instance;
	}

	/**
	*	Activate the plugin
	*/
	public static function activate()
	{
		if ( ! current_user_can( 'activate_plugins' ) )
			return;
		$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
		check_admin_referer( "activate-plugin_{$plugin}" );
		$version = get_option(self::VERSION_OPT,false);
		if($version === FALSE || $version != self::VERSION)
		{
			am_log('Calling install.');
			self::install();
		}
		$timestamp = wp_next_scheduled(self::PREFIX.'run_nightly_showtimes_dump');
		if($timestamp==FALSE)
		{
			wp_schedule_event(time(),'five_minutes',self::PREFIX.'run_nightly_showtimes_dump');
		}
	}

	/**
	*	Deactivate the plugin
	*/
	public static function deactivate()
	{
		if ( ! current_user_can( 'activate_plugins' ) )
			return;
		$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
		check_admin_referer( "deactivate-plugin_{$plugin}" );
		wp_clear_scheduled_hook(self::PREFIX.'run_nightly_showtimes_dump');
	}

	public static function install()
	{
		am_log('Starting Install.');
		Adbay_Movie_Manager::install();
		am_log('Setting version.');
		update_option(self::VERSION_OPT,self::VERSION);
	}

	public function init()
	{
		add_filter( 'cron_schedules', array(&$this,'adding_five_minutes_schedule'));
		//Actions
		add_action(self::PREFIX.'run_nightly_showtimes_dump',array(&$this,'run_nightly_showtimes_dump'));
		add_action('before_delete_post',array(&$this,'delete_post_data'));
		add_action('template_include',array(&$this,'template_chooser'));
		add_action('wp_ajax_'.self::PREFIX.'get_showtimes',array(&$this,'get_ajax_showtimes'));
		add_action('wp_ajax_nopriv_'.self::PREFIX.'get_showtimes',array(&$this,'get_ajax_showtimes'));

		//Shortcuts
		add_shortcode('movies',array(&$this,'run_shortcode_movies'));
		add_shortcode('showtimes',array(&$this,'run_shortcode_showtimes'));
		add_shortcode('amusement_times',array(&$this,'run_shortcode_amusement_times'));
		add_shortcode('amusement',array(&$this,'run_shortcode_amusement'));
		add_shortcode('movie',array(&$this,'run_shortcode_movie'));
		add_shortcode('movie_sidebar',array(&$this,'run_shortcode_movie_sidebar'));

	}


	public function adding_five_minutes_schedule( $schedules ) {
	    $schedules['five_minutes'] = array(
					'interval'  => 300,
					'display'   => __( 'Every 5 Minutes', 'textdomain' )
	    );     
		// echo "<pre>";
		// print_r($schedules);
		// die;
	    return $schedules;
	}

	public function add_settings_page()
	{
		add_submenu_page('edit.php?post_type='.MoviePostType::POST_TYPE,'Movie Settings','Movie Settings','manage_options','adbay-movie-settings',array(&$this,'create_settings_page'));
		add_submenu_page('edit.php?post_type='.MoviePostType::POST_TYPE,'Usage Information','Usage Information','manage_options','adbay-movie-info',array(&$this,'create_info_page'));
	}

	public function create_settings_page()
	{
		if(!empty($_POST['reset']) && $_POST['reset']==1)
		{
			Adbay_Movie_Manager::remove_all_movies();
			$this->run_nightly_showtimes_dump();
		}
		if(!empty($_POST['import']) && $_POST['import']==1)
		{
			$this->run_nightly_showtimes_dump();
		}
		?>
		<div class="wrap">
			<h2>Movie Settings</h2>
			<?php
			if(!empty($_POST['reset']) && $_POST['reset']==1)
			{
			?>
			<div class="updated">
		        <p>Movie data has been reset.</p>
		    </div>
			<?php
			}
			else if(!empty($_POST['import']) && $_POST['import']==1)
			{
			?>
			<div class="updated">
		        <p>Showtimes imported.</p>
		    </div>
			<?php
			}
			$last_run = get_option(self::PREFIX.'showtimes_job');
			?>
			<form method="post" action="">
				<input type="hidden" name="import" value="1">
				<p>Run the import script to bring showtimes data in from the XML feeds.<br>
					Last run: <?php echo !empty($last_run)?$last_run:'Never'; ?><br>
					Current Time: <?php $current = new DateTime(); echo $current->format('l, F jS, Y g:i A'); ?>
				</p>
				<?php submit_button("Import Showtimes Data"); ?>
			</form>
			<form method="post" action="">
				<input type="hidden" name="reset" value="1">
				<p>Removes all movie data and re-imports from the XML feeds. THIS REMOVES TRAILERS AS WELL.</p>
				<?php submit_button("Reset ALL Movie Data"); ?>
			</form>
		</div>
		<?php
	}

	public function create_info_page()
	{
		include dirname(__FILE__).'/views/movie_system_info.php';
	}

	/*
	* Movie types:
	* 	now_showing
	*	coming_soon
	*	epic
	*	3d
	*	epic3d
	*/
	public function run_shortcode_movies($atts)
	{
		extract( shortcode_atts( array(
				'theater_id'=>'NULL',
				'type'=>'now_showing',
				'escape_room'=>'no'
			), $atts ) );
		//Return a template.
		ob_start();
		if($type=="coming_soon")
		{
			am_log(3);
			$cs_movies=Adbay_Movie_Manager::get_coming_soon($escape_room, $theater_id);
			am_log(8);
			if(!empty($theater_id))
			{
				include(sprintf("%s/views/movie_list_theater.php",dirname(__FILE__)));
			}
			else
			{
				include(sprintf("%s/views/movie_list.php",dirname(__FILE__)));
			}
		}
		else if($type=="now_showing")
		{
			am_log(4);
			$cs_movies=Adbay_Movie_Manager::get_now_playing($escape_room);
			include(sprintf("%s/views/movie_list.php",dirname(__FILE__)));
		}
		else if($type=="epic")
		{
			am_log(5);
			$cs_movies=Adbay_Movie_Manager::get_now_playing($escape_room, 2);
			include(sprintf("%s/views/movie_list.php",dirname(__FILE__)));
		}
		else if($type=="3d")
		{
			am_log(6);
			$cs_movies=Adbay_Movie_Manager::get_now_playing($escape_room, 1);
			include(sprintf("%s/views/movie_list.php",dirname(__FILE__)));
		}
		else if($type=="epic3d")
		{
			am_log(7);
			$cs_movies=Adbay_Movie_Manager::get_now_playing($escape_room, 3);
			include(sprintf("%s/views/movie_list.php",dirname(__FILE__)));
		}
		$output = ob_get_contents();
		@ob_end_clean();
		return $output;
	}

	public function run_shortcode_showtimes($atts)
	{
		extract( shortcode_atts( array(
				'theater_id'=>0,
				'escape_room'=>'no'
			), $atts ) );

		//Return a template.
		ob_start();
		include(sprintf("%s/views/showtimes.php",dirname(__FILE__)));
		$output = ob_get_contents();
		@ob_end_clean();
		return $output;
	}

	public function run_shortcode_amusement_times($atts)
	{
		extract( shortcode_atts( array(
				'theater_id'=>0
			), $atts ) );

		//Return a template.
		ob_start();
		include(sprintf("%s/views/amusement_times.php",dirname(__FILE__)));
		$output = ob_get_contents();
		@ob_end_clean();
		return $output;
	}

	public function run_shortcode_amusement($atts)
	{
		extract( shortcode_atts( array(
				'type'=>'all'
			), $atts ) );

		$m_type=0;

		if ($type=='laser')
		{
			$m_type= -1;
		}
		else if($type=='ropes')
		{
			$m_type = -2;
		}

		ob_start();
		$cs_movies = Adbay_Movie_Manager::get_amusement($m_type);
		include dirname(__FILE__).'/views/amusement.php';
		$output = ob_get_clean();
		return $output;
	}

	public function run_shortcode_movie($atts)
	{
		extract( shortcode_atts( array(
				'id'=>'0'
			), $atts ) );

		if($id==0)
		{
			return '<p>No movie selected.</p>';
		}

		ob_start();
		$theaters = array();
		$showtimes = Adbay_Movie_Manager::get_movie_showtimes($id,date('Y-m-d'));
		$current_theater = NULL;
		if(!empty($showtimes))
		{
			foreach($showtimes as $time)
			{
				if($current_theater != $time['theater_id'])
				{
					$theaters[$time['theater_id']]=array('theater'=>$time['theater'],'address'=>$time['address'],'city'=>$time['city'],'state'=>$time['state'],'phone'=>$time['phone'],'showings'=>array());
					$current_theater = $time['theater_id'];
				}
				if(empty($theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]))
				{
					$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['flag'] = $time['flag'];
					$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['times'] = array();
				}
				$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['times'][] = am_format_showtime_link($time);
			}
			include(sprintf("%s/views/showtimes_block.php",dirname(__FILE__)));
		}
		else
		{
			echo 'No times available.';
		}
		$html = ob_get_clean();
		return $html;
	}

	public function run_shortcode_movie_sidebar($atts)
	{
		extract( shortcode_atts( array(
				'id'=>'0'
			), $atts ) );

		if($id==0)
		{
			return '<p>No movie selected.</p>';
		}

		ob_start();
		include(sprintf("%s/views/single-movie-shortcode.php",dirname(__FILE__)));
		$html = ob_get_clean();
		return $html;
	}

	public function get_ajax_showtimes()
	{
		$escape_room=!empty($_POST['escape_room'])?$_POST['escape_room']:NULL;
		$movie_id=!empty($_POST['movieid'])?$_POST['movieid']:NULL;
		$date = $_POST['date'];
		if(empty($date) || $date=='false')
		{
			$date = date('Y-m-d');
		}
		$theater_id = !empty($_POST['theater'])?$_POST['theater']:NULL;

		$response = array();
		ob_start();
		if(!empty($theater_id))
		{
			$type=NULL;
			if(!empty($_POST['type']) && $_POST['type']=='amusement')
			{
				$type = 'amusement';
			}
			$theater_title = Adbay_Movie_Manager::get_theater_title($theater_id);
			// print_r($_POST['escape_room']);
			// die;
			$movies = Adbay_Movie_Manager::get_theater_showtimes($theater_id,$date,$type, $escape_room);
			include(sprintf("%s/views/showtimes_theater_block.php",dirname(__FILE__)));
		}
		else
		{
			$theaters = array();
			$showtimes = Adbay_Movie_Manager::get_movie_showtimes($movie_id,$date, $escape_room);
			$current_theater = NULL;
			if(!empty($showtimes))
			{
				foreach($showtimes as $time)
				{
					if($current_theater != $time['theater_id'])
					{
						$theaters[$time['theater_id']]=array('theater'=>$time['theater'],'address'=>$time['address'],'city'=>$time['city'],'state'=>$time['state'],'phone'=>$time['phone'], 'showings'=>array());
						$current_theater = $time['theater_id'];
					}
					if(empty($theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]))
					{
						$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['flag'] = $time['flag'];
						$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['seats'] = array();
						$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['times'] = array();
					}
					$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['seats'][] = $time['seats'];
					$theaters[$time['theater_id']]['showings'][$time['ts_movie_id']]['times'][] = am_format_showtime_link($time);
				}
				include(sprintf("%s/views/showtimes_block.php",dirname(__FILE__)));
			}
			else
			{
				echo 'No Showtimes Available.';
			}
		}
		$output = ob_get_contents();
		@ob_end_clean();
		$response['success']=TRUE;
		$response['result']=$output;
		$response['date'] = date('l, F d, Y',strtotime($date));
		header('Content-type: application/json');
		echo json_encode($response);
		exit();
	}

	public function run_nightly_showtimes_dump()
	{
		$run = new DateTime();
		update_option(self::PREFIX.'showtimes_job',$run->format('l, F jS, Y g:i A'));
		Adbay_Movie_Manager::delete_ancillary_data();
		$this->run_movie_cron();
		$this->run_showtimes_cron();
	}

	public function run_movie_cron()
	{
		$url = "http://cinergy.tstickets.com/rss/Movies.xml";
		$url = "http://tickets.cinergycinemas.com/RssFeeds/Movies.xml";
		$poster_url = "http://cinergy.tstickets.com/images/MoviePosters/";
		$destination = MOVIE_BASE_DIR."/images/movies/";

		if($url = (filter_var($url,FILTER_VALIDATE_URL)))
		{
			$xml = @simplexml_load_file(rawurlencode($url));
			foreach($xml->MoviesRow as $movie)
			{
				if(!empty($movie->MoviePosterLink))
				{
					$poster_link = explode("/", $movie->MoviePosterLink);
					$image = (string)end($poster_link);
					if($temp_url = (filter_var($poster_link,FILTER_VALIDATE_URL)))
					{
						@copy($temp_url,$destination . $image);
					}
				}
				$data = array();
				$data['movie_id'] = (string)$movie->MovieID;
				$data['ts_movie_id'] = (string)$movie->TSMovieID;
				$data['start_date'] = date('Y-m-d',strtotime((string)$movie->StartDate));
				$data['title'] = strtoupper((string)$movie->MovieTitle);
				$data['runtime'] = (string)$movie->MovieRunTime;
				$data['rating'] = (string)$movie->MovieRating;
				$data['rating_description'] = (string)$movie->MovieRatingDesc;
				$data['description'] = (string)$movie->MovieDescription;
				$data['site_link'] = (string)$movie->MovieSiteLink;
				$data['poster_link'] = (string)$movie->MoviePosterLink;
				$data['theater_count'] = (string)$movie->TheatreCount;
				$data['image'] = $image;

				Adbay_Movie_Manager::add_movie($data);
			}
		}
	}

	public function run_showtimes_cron()
	{
		$url = 'http://cinergy.tstickets.com/rss/AdvShowtimesDetail.xml';
		$url = 'http://tickets.cinergycinemas.com/RssFeeds/AdvShowtimesDetail.xml';

		if($url = (filter_var($url,FILTER_VALIDATE_URL)))
		{
			$xml = @simplexml_load_file(rawurlencode($url));
			foreach($xml->AdvShowtimesDetailRow as $row)
			{
				$showstr = $row->Showtimes;
				$showtimes = explode('|',$showstr);
				$schedstr = $row->ScheduleIDs;
				$schedules = explode('|',$schedstr);
				$seatstr = $row->SeatsRemaining;
				$seats = explode('|',$seatstr);
				$theater = $row->TheatreID;
				$movie = (string)$row->TSMovieID;
				$link = (string)$row->link;
				$vipflags = $row->VipFlags;
				$vipflags = explode('|', $vipflags);
				$busDate = (string)$row->BusinessDate;
				$pub_ex_date = explode('/',$busDate);
				$bus_date = $pub_ex_date[2] . '-' . $pub_ex_date[0] . '-' . $pub_ex_date[1];
				$allow_web = explode('|',(string)$row->AllowWebFlags);
				$date = '';
				$time = '';
				if(!empty($showtimes) && is_array($showtimes) && !empty($showtimes[0]))
				{
					$total = count($showtimes);
					for($i=0;$i<$total;$i++)
					{
						if($this->restrict_showtime($allow_web[$i],$movie)==TRUE)
						{
							continue;
						}

						if($theater=='103' || $theater=='101')
						{
							if((strpos($link,'tstickets.com'))!==FALSE)
							{
								continue;
							}
						}

						$bild = explode(' ',$showtimes[$i]);
						if(!empty($bild) && is_array($bild) && !empty($bild[0]) && !empty($bild[1]))
						{
							$prdate = explode('/',$bild[0]);
							$date = $prdate[2] . '-' . $prdate[0] . '-' . $prdate[1];
							$time = $bild[1];
							$ex_time = explode(':', $time);
							$hour = $ex_time[0];
							$min = substr($ex_time[1], 0, 2);
							$apm = substr($ex_time[1], 2, 2);
							if($apm == 'AM')
							{
								$apm=0;
							}
							else if($apm=='PM')
							{
								$apm=1;
							}

							$existing_showtime= Adbay_Movie_Manager::check_showtime($schedules[$i], $date, $theater);
							$showtime['schedule'] = $schedules[$i];
							$showtime['movie'] = $movie;
							$showtime['theater'] = $theater;
							$showtime['hour'] = $hour;
							$showtime['min'] = $min;
							$showtime['apm'] = $apm;
							$showtime['date'] = $bus_date;
							$showtime['link'] = $link;
							$showtime['vip'] = $vipflags[0];
							$showtime['order_date'] = date('Y-m-d H:i:s',strtotime($bild[0].' '.$bild[1]));
							$showtime['update_flag'] = 1;
							$showtime['seats'] = $seats[$i];

							if($schedules[$i]==0)
							{
								echo 'BLANK SCHEDULE ID FOUND IN FEED!!!! Delete in Ticketsoft or contact Ticketsoft for support.';
								echo '<pre>';
								print_r($showtime);
								echo '</pre>';
							}

							if($existing_showtime==FALSE && $schedules[$i]!=0)
							{
								Adbay_Movie_Manager::add_showtime($showtime);
							}
							else
							{
								//echo 'Showtime found.';
								//Adbay_Movie_Manager::update_showtime($existing_showtime['id'],$showtime);
							}
						}
					}
				}
			}
		}
	}

	private function restrict_showtime($allow_web_flag,$movie_id)
	{
		$restricted_movies = array(9861,9860);
		if(!in_array($movie_id,$restricted_movies))
		{
			return FALSE;
		}
		if($allow_web_flag==0)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function template_chooser($template)
	{
		if(get_query_var('post_type') !=MoviePostType::POST_TYPE)
		{
			return $template;
		}

		global $wp_query;
		$wp_query->is_home = false;
		if(is_single())
		{
			return MoviePostType::get_template_hierarchy('single-adbay_movie');
		}
		else
		{
			return MoviePostType::get_template_hierarchy('archive-adbay_movie');
		}
	}

	public function delete_post_data($post_ID)
	{
		global $post_type;
		if($post_type == MoviePostType::POST_TYPE)
		{
			Adbay_Movie_Manager::delete_movie_data($post_ID);
		}
	}
}

if(class_exists('Adbay_Movie_Plugin'))
{
	register_activation_hook(__FILE__,array('Adbay_Movie_Plugin','activate'));
	register_deactivation_hook(__FILE__,array('Adbay_Movie_Plugin','deactivate'));
	$adbay_movie_plugin = new Adbay_Movie_Plugin();
}

if(isset($adbay_movie_plugin))
{
	$movie_post_type = new MoviePostType();
}

function am_log($message) {
	if (WP_DEBUG === true) {
		if (is_array($message) || is_object($message)) {
			error_log(print_r($message, true));
		} else {
			error_log($message);
		}
	}
}

function am_check_date($date)
{
	if(strtotime($date) > time())
	{
		return TRUE;
	}
	return FALSE;
}

function am_format_showtime_link($movie)
{
	if(strpos($movie['link'],'cinergycinemas.com')===FALSE)
	{
		$formatted_link = 'https://cinergy.tstickets.com/ticketing.aspx?ShowDate='.$movie['date'].'&TheatreID='.$movie['theater_id'].'&MovieID='.$movie['ts_movie_id'].'&ScheduleID='.$movie['schedule'];
	}
	else
	{
		$formatted_link = 'https://tickets.cinergycinemas.com/Ticketing/visSelectTickets.aspx?cinemacode='.$movie['theater_id'].'&amp;txtSessionId='.$movie['schedule'].'&amp;visLang=1';
		//$formatted_link = 'https://cinergy.tstickets.com/ticketing.aspx?ShowDate='.$movie['date'].'&TheatreID='.$movie['theater_id'].'&MovieID='.$movie['ts_movie_id'].'&ScheduleID='.$movie['schedule'];
	}

	if ($movie['min'] < 10 && strlen($movie['min'])==1)
	{
		$formatted_min = str_pad($movie['min'], 2, '0', STR_PAD_LEFT);
	}
	else
	{
		$formatted_min = $movie['min'];
	}
	if($movie['apm']==0)
	{
		$apm = 'AM';
	}
	else
	{
		$apm='PM';
	}
	return '<a href="'.$formatted_link.'">'.$movie['hour'].':'.$formatted_min.$apm.'</a>';
}

/*

Mixed Vista/Tickstoft Feed URLs

http://tickets.cinergycinemas.com/RssFeeds/AdvShowtimes.xml
http://tickets.cinergycinemas.com/RssFeeds/AdvShowtimesDetail.xml
http://tickets.cinergycinemas.com/RssFeeds/Bookings.xml
http://tickets.cinergycinemas.com/RssFeeds/ComingSoon.xml
http://tickets.cinergycinemas.com/RssFeeds/Movies.xml
http://tickets.cinergycinemas.com/RssFeeds/MoviesDetail.xml
http://tickets.cinergycinemas.com/RssFeeds/Showtimes.xml
http://tickets.cinergycinemas.com/RssFeeds/Theatres.xml

 */