<?php

defined( 'ABSPATH' ) OR exit;

// Make sure we don't expose any info if called directly

if ( !function_exists( 'add_action' ) ) {

	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';

	exit;

}



if(!class_exists('MoviePostType'))

{

	class MoviePostType

	{

		private $_meta = array('start_date','runtime','rating','rating_description','description','site_link','imdb','rotten','trailer','image');

		const PREFIX = 'am-';

		const POST_TYPE = "adbay_movie";



		public function __construct()

		{

			add_action('init',array(&$this,'init'));

			add_action('admin_init',array(&$this,'admin_init'));
			add_action('template_redirect',array(&$this,'check_movie_archive'));

		}

		public function check_movie_archive()
		{
			if(is_post_type_archive(self::POST_TYPE))
			{
				wp_redirect( home_url( '/movies/' ) );
				exit();
			}
		}



		public function init()

		{

			$this->create_post_type();

		}



		public function admin_init()

		{

			add_action('save_post',array(&$this,'save_post'));

			add_action('add_meta_boxes',array(&$this,'add_meta_boxes'));

			add_filter('manage_'.self::POST_TYPE.'_posts_columns',array(&$this,'table_head'));

			add_action('manage_'.self::POST_TYPE.'_posts_custom_column',array(&$this,'table_content'),10,2);

		}



		/**

		* Create the post type

		*/

		public function create_post_type()

		{

			$labels = array(

				'name'                => _x( 'Movies', 'Post Type General Name', 'text_domain' ),

				'singular_name'       => _x( 'Movie', 'Post Type Singular Name', 'text_domain' ),

				'menu_name'           => __( 'Movies', 'text_domain' ),

				'parent_item_colon'   => __( 'Parent Movie:', 'text_domain' ),

				'all_items'           => __( 'All Movies', 'text_domain' ),

				'view_item'           => __( 'View Movie', 'text_domain' ),

				'add_new_item'        => __( 'Add New Movie', 'text_domain' ),

				'add_new'             => __( 'Add Movie', 'text_domain' ),

				'edit_item'           => __( 'Edit Movie', 'text_domain' ),

				'update_item'         => __( 'Update Movie', 'text_domain' ),

				'search_items'        => __( 'Search Movies', 'text_domain' ),

				'not_found'           => __( 'Not found', 'text_domain' ),

				'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' )

			);

			$args = array(

				'label'               => __( 'adbay_movie', 'text_domain' ),

				'description'         => __( 'Movie', 'text_domain' ),

				'labels'              => $labels,

				'supports'            => array( 'title'),

				'hierarchical'        => false,

				'public'              => true,

				'show_ui'             => true,

				'show_in_menu'        => true,

				'show_in_nav_menus'   => true,

				'show_in_admin_bar'   => true,

				'menu_position'       => 5.622,

				'menu_icon'=>'dashicons-video-alt2',

				'can_export'          => true,

				'has_archive'         => true,

				'exclude_from_search' => false,

				'publicly_queryable'  => true,

				'capability_type'     => 'page',

				'taxonomies'=>array('category'),

				'rewrite'=>array('slug'=>'movie')

			);

			register_post_type( 'adbay_movie', $args );

		}



		public function table_head($defaults)

		{

			$defaults['trailer'] = 'Trailer';

			$defaults['date'] = 'Added';

			unset($defaults['thumbnail']);

			return $defaults;

		}



		public function table_content($column_name,$post_id)

		{

			if($column_name=='trailer')

			{

				$trailer = get_post_meta($post_id,self::PREFIX.'trailer',true);

				if(!empty($trailer))

				{

					echo 'Yes';

				}

				else

				{

					echo '<strong>No</strong>';

				}

			}

		}



		public function add_meta_boxes()

		{

			add_meta_box('adbay-movies','Movie Information',array(&$this,'add_inner_meta_boxes'),self::POST_TYPE);

		}



		public function add_inner_meta_boxes($post)

		{

			//Render the job order metabox

			$meta = $this->_meta;

			include(sprintf("%s/../views/%s_metabox.php",dirname(__FILE__),self::POST_TYPE));

		}



		public function save_post($post_id)

		{

			//Verify if this is an auto save routine

			//If it is our form has not been submitted, so we don't want to do anything

			if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)

			{

				return;

			}



			if($_POST['post_type']==self::POST_TYPE && current_user_can('edit_post',$post_id))

			{

				foreach($this->_meta as $field_name)

				{

					if($field_name=='trailer')

					{

						update_post_meta($post_id,self::PREFIX.'ytID',self::parse_youtube($_POST[self::PREFIX.$field_name]));

					}

					update_post_meta($post_id,self::PREFIX.$field_name,$_POST[self::PREFIX.$field_name]);

				}

			}

			else

			{

				return;

			}

		}



		public static function get_template_hierarchy($template)

		{

			if(substr($template,-4)!='.php')

			{

				$template .= '.php';

			}



			if($theme_file = locate_template(array($template)))

			{

				$file = $theme_file;

			}

			else

			{

				$file = sprintf("%s/../views/".$template,dirname(__FILE__));

			}

			return apply_filters('adbay_movie_template_'.$template,$file);

		}



		public static function parse_youtube($video)

		{

			if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video, $match))

			{

				return $match[1];

			}

			return NULL;

		}

	}

}