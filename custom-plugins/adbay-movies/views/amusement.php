<!-- Start Movie-Listing -->
<div>
	<ul id="movies">
	<?php if(!empty($cs_movies)){foreach ($cs_movies as $movie) { ?>
		<li>

            <!-- Start Movie-Poster -->
        	<div class="movie-poster">
				<img src="<?php echo plugins_url('adbay-movies/images/movies/'.$movie['image']); ?>" />
            </div>
            <!-- End Movie-Poster -->

            <!-- Start Movie-Info -->
            <div class="movie-info">
            	<div class="movie-title">
                <a href="<?php echo site_url('movie/'.$movie['post_name']) ?>"><h3><?php echo $movie['title']; ?>test</h3></a>
                </div>
                <div class="movie-meta">
                <!--<?php if(am_check_date($movie['start_date'])){?><strong>Opens Nationwide: </strong><?php echo date('l, F d, Y', strtotime($movie['start_date'])); ?><br /><?php } ?>-->
                <!--<strong>Rating: </strong><?php echo $movie['rating']; ?><br />-->
                <!--<strong>Rating Description: </strong><?php echo $movie['rating_description']; ?><br />-->
                <strong>Description: </strong><?php echo $movie['description']; ?>
                </div>
            </div>
            <!-- End Movie-Info -->

		</li>
	<?php } } else echo '<li>No movies available.</li>';?>
	</ul>
</div>
<!-- End Movie-Listing -->