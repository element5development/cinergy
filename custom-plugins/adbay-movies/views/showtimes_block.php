<?php

if (!function_exists('generate_theater_ownership')){
	function generate_theater_ownership($theater) {
		return str_replace('Cinergy ','',$theater).'\'s';
	}
}

foreach ($theaters as $theater) {
	$theater_name_first = str_replace('CINERGY ', '', $theater['theater']);
	$theater_name_second = str_replace('Cinergy ', '', $theater_name_first);
	$theater_name = ucwords(strtolower($theater_name_second));

	if ( $theater_name == "Copperas Cove" ) { $location_selected = 'location-cc'; }
	elseif ( $theater_name == "Midland" ) {$location_selected = 'location-mid'; }
	elseif ( $theater_name == "Odessa" ) {$location_selected = 'location-odes'; }
	elseif ( $theater_name == "Amarillo" ) {$location_selected = 'location-ama'; }
	elseif ( $theater_name == "Tulsa" ) {$location_selected = 'location-tulsa'; }
	elseif ( $theater_name == "Granbury" ) {$location_selected = 'location-granbury'; }
	elseif ( $theater_name == "Marble Falls" ) {$location_selected = 'location-mfalls'; }
	else { $location_selected = 'location-none'; }

	echo '<li class="'.$location_selected.'">';
	foreach($theater['showings'] as $showing) {
		switch($showing['flag']) {
			case '0':
				echo '<div class="cinema"><h3>'.$theater_name."'s Digital Cinema</h3>";
				break;
			case '1':
				echo '<div class="cinema-3d"><img src="/wp-content/uploads/2017/05/3D.png" alt="3D Movies"><h3>'.$theater_name."'s Digital Cinema 3D</h3>";
				break;
			case '2':
				echo '<div class="cinema-epic"><img class="epic" src="/wp-content/uploads/2017/05/EPIC.png")" alt="Epic Movies"><h3>'.$theater_name."'s Digital Cinema EPIC</h3>";
				break;
			case '3':
				echo '<div class="cinema-epic3d"><img src="/wp-content/uploads/2017/05/3D.png" alt="3D Movies"><img class="epic" src="/wp-content/uploads/2017/05/EPIC.png" alt="Epic Movies"><h3>'.$theater_name."'s Digital Cinema EPIC 3D</h3>";
				break;
			default:
				echo '<div class="cinema">';
				break;
		}


		//LOCATE MOVIE ID
		// foreach ($theater as $item) {
	 //    foreach ($item as $key => $value) {
  //       echo $key.' | ';
  //       //CONVERT THEATER CITY TO THEATER ID
  //       if ( $theater['city'] == 'Midland' ) {
  //       	$theater_id = 103;
  //       } elseif ( $theater['city'] == 'Coppers Cove' ) {
  //       	$theater_id = 101;
  //       } elseif ( $theater['city'] == 'Odessa' ) {
  //       	$theater_id = 104;
  //       }
  //       //PULL ALL DATA
		// 		$data = simplexml_load_file("http://tickets.cinergycinemas.com/RssFeeds/AdvShowtimesDetail.xml");
		// 		foreach ($data->AdvShowtimesDetailRow as $item)
		// 		{
		// 		  if ($item->TSMovieID == $key && $item->TheatreID == $theater_id) //SAME THEATER AND MOVIE ID
		// 		  {
		// 		     print_r($item);
		// 		  }
		// 		}
		//   }
		// }

		/* LOOP THROUGH TIMES TO DETERMINE IF TIME PAST */
		$showtimes = array( $showing['times'], $showing['seats'] );
		
 		//CHECK SEATS ARRAY IF LESS THEN 50 AND IF SO SAVE SPOT IN ARRAY
 		$lowSeats = array();
 		$i = 0;
 		$seatsRemaing = $showtimes[1];
 		foreach ( $seatsRemaing as $seats) {
 			if ( $seats < 25 ) { $lowSeats[] = $i; }
 			$i++;
 		}
		//print_r($lowSeats); //TIMES WITH LOW SEATS

		//LOOP THROUGH SHOWTIMES AND DETERMIN IF MOVIE IS LIVE, PAST, OR SOLD OUT
		$j = 0; 
		foreach ($showtimes[0] as $showtime) {
			$html_string = htmlentities($showtime);
			$showtime_html_end = substr($html_string, -12);
			$showtime_end = substr($showtime_html_end, 0, -10); //AM or PM
			$showtime_html_time = substr($html_string, -17);
			$showtime_time = preg_replace("/[^0-9]/","",$showtime_html_time);
			if ( $showtime_end == 'PM' && $showtime_time < 1199 ) { //CONVERTO TO MILITARY TIME
				$showtime_time = $showtime_time + 1200;
			}
			$current_time = date('Hi');
			if ( in_array($j, $lowSeats) ) { //movie sold out
				if ( $showtime_time < $current_time ) { //movie already started
					$html_string_start = substr($html_string, 0, 5);
					$html_string_class = ' class="past-movie low-seats btn secondary-btn" ';
					$html_string_end = substr($html_string, 5, strlen($html_string));
					$past_movie_link_string = $html_string_start.''.$html_string_class.''.$html_string_end;
					$past_movie_link = htmlspecialchars_decode($past_movie_link_string);
					echo $past_movie_link;
				} else { //movie good
					$html_string_start = substr($html_string, 0, 5);
					$html_string_class = ' class="low-seats btn secondary-btn" ';
					$html_string_end = substr($html_string, 5, strlen($html_string));
					$movie_link_string = $html_string_start.''.$html_string_class.''.$html_string_end;
					$movie_link = htmlspecialchars_decode($movie_link_string);
					echo $movie_link;
				}
			} else {
				if ( $showtime_time < $current_time ) { //movie already started
					$html_string_start = substr($html_string, 0, 5);
					$html_string_class = ' class="past-movie btn secondary-btn" ';
					$html_string_end = substr($html_string, 5, strlen($html_string));
					$past_movie_link_string = $html_string_start.''.$html_string_class.''.$html_string_end;
					$past_movie_link = htmlspecialchars_decode($past_movie_link_string);
					echo $past_movie_link;
				} else { //movie good
					$html_string_start = substr($html_string, 0, 5);
					$html_string_class = ' class="btn secondary-btn" ';
					$html_string_end = substr($html_string, 5, strlen($html_string));
					$movie_link_string = $html_string_start.''.$html_string_class.''.$html_string_end;
					$movie_link = htmlspecialchars_decode($movie_link_string);
					echo $movie_link;
				}
			}
			$j++;
		}
		echo '</div>';
	}
	echo '</li>';
}
?>