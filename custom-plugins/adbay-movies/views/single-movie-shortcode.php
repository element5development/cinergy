        	<!-- Single-Movie-Sidebar Start -->
            <div class="single-movie-sidebar">

                <!-- Start Movie-Listing -->
                <div class="movie-listing">

                    <!-- Regular_Listing Start -->
                    <div id="regular_listing">

                        <!-- Movie-Times Start -->
                        <div class="movie-times">
                            <?php $dates = Adbay_Movie_Manager::create_movie_date_block($id); ?>
                            <strong>Showtimes for: </strong>

                                <!-- Start Display_Date -->
                                <div id="display_date">
                                    <?php echo date('l, F d, Y',strtotime($dates[0]['date'])); ?>
                                </div>
                                <!-- End Display_Date -->

                            <br>

                            <?php
                            $x=0;
                            foreach($dates as $date){
                                if($x==0)
                                {
                                    echo '<button type="button" class="am_date_picker" value="'.$date['date'].'" disabled="disabled">'.$date['button'].'</button>';
                                    $x++;
                                }
                                else
                                {
                                    echo '<button type="button" class="am_date_picker" value="'.$date['date'].'">'.$date['button'].'</button>';
                                }
                                }?>
                        </div>
                        <!-- Movie-Times End -->

                        <!-- Start Theaters -->
                        <ul id="theaters"></ul>
                        <!-- End Theaters -->

                    </div>
                    <!-- Regular_Listing End -->

                </div>
                <!-- End Movie-Listing -->

            </div>
        	<!-- Single-Movie-Sidebar End -->

            <script type="text/javascript">
/* <![CDATA[ */
    var ajaxurl="<?php echo admin_url('admin-ajax.php'); ?>";
    jQuery(function($){
        var buttons = $(".am_date_picker");
        buttons.click(function(){
            buttons.removeAttr("disabled");
            $(this).attr("disabled","disabled");
            var date_mult = $(this).val();
            get_times(date_mult);
        });
        get_times(buttons.eq(0).val());
    });

    function get_times(date_mult) {
        date_mult = date_mult || false;
        var mdata = {
            movieid:<?php echo $id; ?>,
            date: date_mult,
            action:'amp_get_showtimes'
        };
        jQuery.post(ajaxurl,mdata,
            function(response_data){
                if (response_data.success){
                    jQuery("#theaters").html(response_data.result);
                    if(response_data.coming_soon)
                    {
                        $("#regular_listing").hide();
                    }
                }
                else {
                    jQuery("#theaters").html("<p>No showtime currently available.</p>");
                }
                jQuery("#display_date").html(response_data.date);
            }, "json");
    }
/* ]]> */
</script>