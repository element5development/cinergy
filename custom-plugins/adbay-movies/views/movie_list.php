<!-- Start Movie-Listing -->

<div id="movies-container" class="movies-container flex-container max-width">

	<?php if(!empty($cs_movies)){foreach ($cs_movies as $movie) { ?>

        <?php $meta = get_post_meta($movie['ID']); ?>
        <?php //UPPERCASE TO TITLE CASE
            // $movie_title = ucwords(strtolower($movie['title'])); 
            $movie_title = $movie['title']; 
        ?>

		<div class="movie-preview">
					<!-- Start Movie-Poster -->
        	<div class="movie-poster"><a href="<?php echo site_url('movie/'.$movie['post_name']) ?>">
        		<?php if(file_exists(MOVIE_BASE_DIR."/images/movies/".$movie['image'])) { ?>
        		  <img src="<?php echo plugins_url('adbay-movies/images/movies/'.$movie['image']); ?>" />
        		<?php } else { ?>
    		      <img src="https://tickets.cinergycinemas.com/CDN/Image/Entity/FilmPosterGraphic/<?php echo $movie['image']; ?>" />
                <?php } ?>
            </a></div>
            <!-- End Movie-Poster -->

            <!-- Start Movie-Info -->
            <div class="movie-info">
                <a href="<?php echo site_url('movie/'.$movie['post_name']) ?>"><h3><?php echo $movie_title; ?></h3></a>
                <?php if(am_check_date($movie['start_date'])){?>
                    <div class="movie-date"><?php echo date('l, F d, Y', strtotime($movie['start_date'])); ?></div>
                <?php } ?>
                <div class="movie-rating"><?php echo $movie['rating']; ?></div>
                <div class="movie-length"><?php echo $meta['am-runtime'][0] ?> Min</div>
                <a href="<?php echo site_url('movie/'.$movie['post_name']); ?>" class="btn primary-btn arrow movie-tickets movie-details"><span>Learn More
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                        <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                    </svg>
                    </span>
                </a>
            </div>
		</div>

	<?php } } else echo '<li>No movies available.</li>';?>

    <!-- sesory friendly not found content -->
    <div class="sensory-screenings">
        <h3>Movies are meant to be enjoyed by everyone!</h3>
        <p>If you want to know more about our future sensory-friendly showings please <a href="/contact/">contact us today</a>.</p>
        <?php if ( get_field('movie_title', 2200) ) { ?>
            <div class="sensory-display flex-container max-width">
                <div class="one-half">
                     <a href="/movies/sensory-friendly-screening/"><?php movie_db_render_poster( get_field('movie_title', 2200) ); ?></a>
                </div>
                <div class="one-half">
                    <div class="vcenter">
                        <a href="/movies/sensory-friendly-screening/">
                            <h4><?php the_field('movie_title', 2200); ?></h4>
                            <p><?php the_field('screening_date', 2200); ?></p>
                        </a>
                        <a href="/movies/sensory-friendly-screening/" class="btn primary-btn"><span>Learn More</span></a>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <p>That is exactly why Cinergy has introduced sensory friendly screenings. One Saturday a month at  9:30am (Amarillo, Midland & Odessa) and 11:30 (Marble Falls, Granbury, Copperas Cove and Tulsa), Cinergy will present a family friendly film in an auditorium dedicated to guests affected by autism and sensory processing disorders. These showings will feature brightened light levels, reduced sound volume, and room for guest interaction and movement.</p>
        <?php } ?>
    </div>
</div>

<!-- End Movie-Listing -->
