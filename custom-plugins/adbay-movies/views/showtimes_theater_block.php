<?php
if(empty($movies))
{
	if ( $escape_room == 'yes' ) { ?>

		<?php if( have_rows('items', 3390) ): ?>
			<?php while( have_rows('items', 3390) ): the_row(); ?>
				<article class="item <?php echo $class; ?>">
					<div class="block">
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<div class="block">
						<h3>
							<?php the_sub_field('headline') ?>
						</h3>
						<p class="theater" style="color: <?php the_field('cta_color', 3390); ?>;">No Times currently available.</p>
						<div class="contents">
							<?php the_sub_field('contents'); ?>
						</div>
						<h4>The Details</h4>
						<hr style="background-color: <?php the_field('cta_color', 3390); ?>;">
						<ul style="color: <?php the_field('text_color', 3390); ?>;">
							<?php while( have_rows('details') ) : the_row(); ?>
							<li>
								<?php $icon = get_sub_field('icon');
									if( !empty($icon) ): ?>
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
								<?php endif; ?>
								<?php the_sub_field('label'); ?>
							</li>
							<?php endwhile; ?>
						</ul>
					</div>
				</article>
			<?php endwhile; ?>
		<?php endif; ?>
		
	<?php } else {
		echo '<p style="text-align: center; padding: 0 0 60px 0; margin: 0;">No times are currently available for this date.</p>';
	}
	return;
}
if (!function_exists('generate_theater_ownership')){
	function generate_theater_ownership($theater)
	{
		return str_replace('Cinergy ','',$theater).'\'s';
	}
}

foreach ($movies as $movie) {
		$meta = get_post_meta($movie['post_ID']);
		?>

		<!-- ESCAPE ROOM OR MOVIE -->
		<?php $title = ucwords(strtolower($movie['post_title'])); ?>
		<?php if ( strpos($title, 'Escape Room -') !== false ) : ?>
			<?php $title = str_replace("Escape Room -", "", $title); ?>
			<article class="item <?php echo $class; ?>">
				<div class="block">
					<?php if(file_exists(MOVIE_BASE_DIR."/images/movies/".$meta[MoviePostType::PREFIX.'image'][0])) { ?>
						<img src="<?php echo plugins_url('adbay-movies/images/movies/'.$meta[MoviePostType::PREFIX.'image'][0]); ?>" />
					<?php } else { ?>
						<img src="https://tickets.cinergycinemas.com/CDN/Image/Entity/FilmPosterGraphic/<?php echo $meta[MoviePostType::PREFIX.'image'][0]; ?>" />
					<?php } ?>
				</div>
				<div class="block">
					<h3>
						<?php echo $title; ?>
					</h3>
					<?php foreach($movie['showings'] as $key=>$showing) {
						echo '<p class="theater">available times at '.strtolower($theater_title).'</p>';
						echo '<div class="times" style="color:'.get_field('cta_color', 3390).';">'.implode('<span>|</span>',$showing['times']).'</div>';
					} ?>
					<?php if( have_rows('items', 3390) ): ?>
						<?php while( have_rows('items', 3390) ): the_row(); ?>
							<?php $acftitle = get_sub_field('headline') ?>
							<?php if ( strpos($title, $acftitle) !== false ) { ?>
								<div class="contents">
									<?php the_sub_field('contents'); ?>
								</div>
								<h4>The Details</h4>
								<hr style="background-color: <?php the_field('cta_color', 3390); ?>;">
								<ul style="color: <?php the_field('text_color', 3390); ?>;">
									<?php while( have_rows('details') ) : the_row(); ?>
									<li>
										<?php $icon = get_sub_field('icon');
											if( !empty($icon) ): ?>
										<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
										<?php endif; ?>
										<?php the_sub_field('label'); ?>
									</li>
									<?php endwhile; ?>
								</ul>
							<?php } ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</article>
		<?php else : ?>

		<div class="single-movie-slide movie-preview
      <?php if ( strpos($movie_title, 'Sensory-Friendly') !== false || strpos($movie_title, 'sensory-friendly') !== false || strpos($movie_title, 'Sensory Friendly') !== false || strpos($movie_title, 'sensory friendly') !== false ) { ?>
        sensory-movie
      <?php } ?>
		">
			<!-- Start Movie-Poster -->
			<div class="movie-poster"><a href="<?php echo site_url('movie/'.$movie['post_name']); ?>">
				<?php if(file_exists(MOVIE_BASE_DIR."/images/movies/".$meta[MoviePostType::PREFIX.'image'][0])) { ?>
				<img src="<?php echo plugins_url('adbay-movies/images/movies/'.$meta[MoviePostType::PREFIX.'image'][0]); ?>" />
				<?php } else { ?>
					<img src="https://tickets.cinergycinemas.com/CDN/Image/Entity/FilmPosterGraphic/<?php echo $meta[MoviePostType::PREFIX.'image'][0]; ?>" />
				<?php } ?>
				<!--<img src="<?php echo plugins_url('adbay-movies/images/movies/'.$meta[MoviePostType::PREFIX.'image'][0]); ?>" />-->
				</a>
			</div>
			<!-- End Movie-Poster -->
			<!-- Start Movie-Info -->
			<div class="movie-info">
				<?php //UPPERCASE TO TITLE CASE
          // $movie_title = ucwords(strtolower($movie['title'])); 
						$movie_title = $movie['title']; 
        ?>
				<a href="<?php echo site_url('movie/'.$movie['post_name']); ?>"><h3><?php echo $movie_title; ?></h3></a>
				<div class="movie-rating"><strong>Rating: </strong><?php echo $meta[MoviePostType::PREFIX.'rating'][0]; ?></div>
				<div class="movie-length"><strong>Runtime: </strong><?php echo $meta[MoviePostType::PREFIX.'runtime'][0]; ?> Min</div>
				<p>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/closed-caption-grey.svg" alt="closed-caption" />
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/assisted-listening-grey.svg" alt="closed-caption" />
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/descriptive-narrative-grey.svg" alt="closed-caption" />
				</p>
        <!-- TIMES -->
				<?php foreach($movie['showings'] as $key=>$showing) {
					switch($showing['flag']) {
						case '0':
							echo '<div class="cinema"><h3><span>'.generate_theater_ownership($theater_title).'</span> Digital Cinema</h3>';
							break;
						case '1':
							echo '<div class="cinema-3d"><h3><span>'.generate_theater_ownership($theater_title).'</span> Digital Cinema 3D</h3>';
							break;
						case '2':
							echo '<div class="cinema-epic"><h3><span>'.generate_theater_ownership($theater_title).'</span> Digital Cinema EPIC</h3>';
							break;
						case '3':
							echo '<div class="cinema-epic3d"><h3><span>'.generate_theater_ownership($theater_title).'</span> Digital Cinema EPIC 3D</h3>';
							break;
					}
					echo '<br>'.implode(' | ',$showing['times']);
					echo '</div>';
				} ?>
				<a href="<?php echo site_url('movie/'.$movie['post_name']); ?>" class="btn primary-btn arrow movie-tickets"><span>Learn More
					<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
						<path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
          </svg>
          </span>
        </a>
			</div>
			<!-- END TIMES -->
		</div>

		<?php endif; ?>


</div>

<?php } ?>
<!-- End Movie-Info -->