<?php
/**
 * Page default template / Single Movie Template
 *
 * @package    WebMan WordPress Theme Framework
 * @copyright  2014 WebMan - Oliver Juhas
 *
 * @since    1.0
 * @version  1.1.1
 */

get_header();
?>

<!--SET MOVIE DATA-->
<?php $meta = get_post_meta(get_the_ID()); ?>

<!-- GET DEFAULT FEATURE IMAGE -->
<?php 
	$image = get_field('default_movie_image', 'options');
	$background_url = esc_url($image['url']);
?>

<main class="page-contents full-width">

  <!-- PAGE TITLE -->
  <section class="page-title-container full-slider full-width dark-bg">
    <div class="movie" style="background-image: url('<?php echo $background_url; ?>'); background-size: cover;">
    <?php movie_db_render_backdrop( get_the_title(), 'large', get_the_date('Y') ); ?>
      <div class="max-width">
        <div class="contents">
          <h1><?php the_title(); ?></h1>
          <p class="details"><?php echo $meta['am-rating'][0]; ?> | <?php echo $meta['am-runtime'][0]; ?> Min</p>
          <p class="btn-container">
              <a href="#tickets-anchor" class="btn primary-btn smoothScroll"><span>get tickets</span></a>
            <?php if ( get_field('trailer_url') ) { ?>
              <a target="blank" href="<?php the_field('trailer_url') ?>" class="btn secondary-btn play" data-lity><span>Watch Trailer
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <defs>
                    <clipPath id="a"><path d="M189 40V16h24v24zm3-10.35l17.54 7.13L203.83 19z"/></clipPath>
                  </defs>
                  <path stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" d="M203.83 19l5.71 17.78L192 29.65z" clip-path="url(&quot;#a&quot;)" transform="translate(-189 -16)"></path>
                </svg>
                </span>
              </a>
            <?php } ?>
          </p>
        </div>
      </div>
      <div class="overlay"></div>
    </div>
  </section>

  <!-- BREADCRUMBS -->
  <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

  <!-- MOVIE POSTER / RATING / DESCRIPTION -->
  <div class="two_col_basic">
    <div class="flex-container max-width">
      <div class="one-half">
        <?php
          if(file_exists(MOVIE_BASE_DIR."/images/movies/".$meta['am-image'][0])) {
            $poster ='<img src="'.plugins_url('adbay-movies/images/movies/'.$meta['am-image'][0]).'" />';
          } else {
            $poster ='<img src="https://tickets.cinergycinemas.com/CDN/Image/Entity/FilmPosterGraphic/'.$meta['am-image'][0].'" />';
          } echo $poster;
        ?>
      </div>
      <div class="one-half">
        <?php if ( get_field('imdb_rating') && get_field('tomatoes_rating') ) { ?>
          <p class="reviews">Imdb <b><?php the_field('imdb_rating'); ?>/10</b><span></span>Rotten Tomatoes <b><?php the_field('tomatoes_rating'); ?>/10</b></p>
        <?php } ?>
        <p><?php echo $meta['am-description'][0]; ?></p>
      </div>
    </div>
    <div class="overlay"></div>
    <div class="overlay two"></div>
  </div>


  <!-- SHOWTIMES -->
  <div class="movie_showitimes_section two_col_basic scroll">
    <a id="tickets-anchor" class="styleguide-anchor"></a>
    <div class="flex-container max-width">
      
      <?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED ?>
        <div class="location-forced menu-locations">
          <h2 class="align-center">Select a location to view showtimes</h2>
          <div class="location-option-container">
            <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
            <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
            <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
						<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
						<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
						<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
						<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
          </div>
        </div>
      <?php } else { //LOCATION SET ?>
        <div class="movie-times full-width">
          <?php $dates = Adbay_Movie_Manager::create_movie_date_block(get_the_ID()); ?>
          <p class="active-date">
            Showtimes for:
            <span id="display_date"><?php echo date('l, F d, Y',strtotime($dates[0]['date'])); ?></span>
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
              <path fill="#9ecb51" d="M12.5 14.57L7.53 9.6a.8.8 0 0 0-1.11 0 .8.8 0 0 0 0 1.11l5.53 5.52c.3.31.8.31 1.1 0l5.53-5.52a.8.8 0 0 0 0-1.11.8.8 0 0 0-1.11 0zM1.56 12.5a10.94 10.94 0 1 1 21.89 0 10.94 10.94 0 0 1-21.89 0zM0 12.5a12.5 12.5 0 1 0 25 0 12.5 12.5 0 0 0-25 0z"></path>
            </svg>
          </p>
          <div class="date-selection">
            <?php $x=0;
              foreach($dates as $date) {
                if($x==0) {
                  echo '<button type="button" class="am_date_picker" value="'.$date['date'].'" disabled="disabled"><span>'.$date['button'].'</span></button>';
                  $x++;
                } else {
                  echo '<button type="button" class="am_date_picker" value="'.$date['date'].'"><span>'.$date['button'].'</span></button>';
                }
              } ?>
          </div>
        </div>
        <div class="one-half">
          <span class="sticky">
            <h2>Showtimes</h2>
          </span>
        </div>
        <div class="one-half">
            <!-- Single-Movie-Sidebar Start -->
              <div class="single-movie-sidebar">
                <div class="movie-listing">
                  <div id="regular_listing">
                    <?php $selected_theater = $_COOKIE["CinergyLocation"]; ?>
                    <ul id="theaters" class="<?php echo $selected_theater; ?>"></ul>
                </div>
              </div>
            </div>
            <!-- Single-Movie-Sidebar End -->
        </div>
      <?php } ?> 

    </div>
  </div>

  <!-- CAST -->
    <div class="cast two_col_basic left">
      <div class="flex-container max-width">
        <div class="one-half">
          <h2>Top Billed Cast</h2>
        </div>
        <div class="one-half">
          <?php movie_db_render_crew( get_the_title(), 'small', get_the_date('Y') ); ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="overlay two"></div>
      <?php movie_db_render_backdrop( get_the_title(), 'large', get_the_date('Y') ); ?>
    </div>

  <!-- PAGE GALLERY -->
	<?php movie_db_render_gallery( get_the_title(), 'large', get_the_date('Y')); ?>
	
	<!-- R RATING LIGHTBOX -->
	<?php if ( $meta['am-rating'][0] == 'R' ) { ?>
		<div id="alert-rating" class="lightbox active">
			<div class="contents">
				<svg id="close-alert" xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37">
					<path d="M21.77 18.5l4.08-4.08a2.32 2.32 0 0 0-3.27-3.27l-4.08 4.08-4.2-4.08c-.9-.9-2.36-.9-3.27 0-.9.9-.67 2.37.24 3.27l3.96 4.08-4.2 4.19a2.32 2.32 0 0 0 3.27 3.28l4.2-4.2 4.19 4.2a2.32 2.32 0 0 0 3.27-3.28zm-19.46 0a16.2 16.2 0 1 1 32.38 0 16.2 16.2 0 0 1-32.38 0zM0 18.5a18.5 18.5 0 1 0 37 0 18.5 18.5 0 0 0-37 0z"></path>
				</svg>
				<h2>This movie is rated R</h2>
				<p>No one under the age of 17 will be admitted to a Rated-R feature unless accompanied by a parent or legal guardian. Valid ID required as proof of age.</p>
				<p>After 6pm, children ages 6 and under are not permitted to attend R-rated features, even if accompanied by an adult.</p>
			</div>
		</div>
	<?php } ?>
    <!-- NC17 RATING LIGHTBOX -->
	<?php if ( $meta['am-rating'][0] == 'NC17' ) { ?>
		<div id="alert-rating" class="lightbox active">
			<div class="contents">
				<svg id="close-alert" xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37">
					<path d="M21.77 18.5l4.08-4.08a2.32 2.32 0 0 0-3.27-3.27l-4.08 4.08-4.2-4.08c-.9-.9-2.36-.9-3.27 0-.9.9-.67 2.37.24 3.27l3.96 4.08-4.2 4.19a2.32 2.32 0 0 0 3.27 3.28l4.2-4.2 4.19 4.2a2.32 2.32 0 0 0 3.27-3.28zm-19.46 0a16.2 16.2 0 1 1 32.38 0 16.2 16.2 0 0 1-32.38 0zM0 18.5a18.5 18.5 0 1 0 37 0 18.5 18.5 0 0 0-37 0z"></path>
				</svg>
				<h2>This movie is rated NC17</h2>
				<p>No one under the age of 17 will be admitted to an NC17 feature, even if accompanied by an adult. Valid ID required as proof of age.</p>
			</div>
		</div>
	<?php } ?>

</main>

<script type="text/javascript">
/* <![CDATA[ */
  var ajaxurl="<?php echo admin_url('admin-ajax.php'); ?>";
  jQuery(function($){
    var buttons = $(".am_date_picker");
    buttons.click(function(){
      buttons.removeAttr("disabled");
      $(this).attr("disabled","disabled");
      var date_mult = $(this).val();
      get_times(date_mult);
    });
    get_times(buttons.eq(0).val());
  });

  function get_times(date_mult) {
    date_mult = date_mult || false;
    var mdata = {
      movieid:<?php echo get_the_ID() ?>,
      date: date_mult,
      action:'amp_get_showtimes'
    };
    jQuery.post(ajaxurl,mdata,
      function(response_data){
        if (response_data.success){
          jQuery("#theaters").html(response_data.result);
          if(response_data.coming_soon)
          {
            $("#regular_listing").hide();
          }
        }
        else {
          jQuery("#theaters").html("<p>No showtime currently available.</p>");
        }
        jQuery("#display_date").html(response_data.date);
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var dayNames = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        var today = new Date();
        var day = dayNames[today.getDay()-1];
        var dd = (today.getDate() < 10 ? '0' : '') + today.getDate();
        var month = monthNames[today.getMonth()];
        var yyyy = today.getFullYear();
        var today_date = day + ', '+ month + ' ' + dd + ', ' + yyyy;
        if ( response_data.date == today_date ) {
          jQuery('#theaters').addClass('today-date');
        } else {
          jQuery('#theaters').removeClass('today-date');
        }
      }, "json");
  }
/* ]]> */
</script>
<!-- Wrapper-Inner End -->
<?php get_footer(); ?>