<div class="wrap">
	<h2>Ticketsoft Plugin Use</h2>
	<h3>Feeds</h3>
	<p>Ticketsoft data is read from the following feeds:</p>
	<ul>
		<li>http://tickets.cinergycinemas.com/RssFeeds/Movies.xml</li>
		<li>http://cinergy.tstickets.com/images/MoviePosters/</li>
		<li>http://tickets.cinergycinemas.com/RssFeeds/AdvShowtimesDetail.xml</li>
	</ul>
	<h3>Shortcodes</h3>
	<p><strong>[movies][/movies]</strong><br>
		Get the movie_list.php or movie_list_theater.php view.<br>
		<em>Attributes</em>
		<ul>
			<li>theater_id: The id of the specific theater you wish to display. <em>Defaults to: None</em></li>
			<li>type: coming_soon, now_showing, epic, 3d, epic3d. <em>Defaults to: now_showing</em></li>
			<li>escape_room: yes, no. <em>Defaults to: no</em></li>
		</ul>
	</p>
	<p><strong>[showtimes][/showtimes]</strong><br>
		Get the showtimes.php view for a theater.<br>
		<em>Attributes</em>
		<ul>
			<li>theater_id: The id of the specific theater you wish to display. <em>Defaults to: No default</em></li>
		</ul>
	</p>
	<p><strong>[movie][/movie]</strong><br>
		Get invidual movie information.<br>
		<br>
		<em>Attributes</em>
		<ul>
			<li>id: The POST ID of the specific theater you wish to display. <em>Defaults to: No default</em></li>
		</ul>
	</p>
	<p><strong>[amusement][/amusement]</strong><br>
		Get amusement listings.<br>
		<em>Attributes</em>
		<ul>
			<li>type: all, laser, ropes. <em>Defaults to: all</em></li>
		</ul>
	</p>
	<p><strong>[amusement_times][/amusement_times]</strong><br>
		Get amusement times.<br>
		<em>Attributes</em>
		<ul>
			<li>theater_id:  The id of the specific theater you wish to display. <em>Defaults to: No default</em></li>
		</ul>
	</p>
</div>