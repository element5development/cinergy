<table>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="am_description">Description</label>

		</th>

		<td>

			<textarea id="am_description" name="am-description"><?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'description', true); ?></textarea>

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="am_runtime">Runtime</label>

		</th>

		<td>

			<input type="text" id="am_runtime" name="am-runtime" value="<?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'runtime', true); ?>" />

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="am_rating">Rating</label>

		</th>

		<td>

			<input type="text" id="am_rating" name="am-rating" value="<?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'rating', true); ?>" />

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="am_rating_description">Rating Description</label>

		</th>

		<td>

			<textarea id="am_rating_description" name="am-rating_description"><?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'rating_description', true); ?></textarea>

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="am_site_link">Site Link</label>

		</th>

		<td>

			<input type="text" id="am_site_link" name="am-site_link" value="<?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'site_link', true); ?>" />

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="imdb_score">IMDB Score</label>

		</th>

		<td>

			<input type="number" step=".01" id="am_imdb" name="am-imdb" value="<?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'imdb', true); ?>" />

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="rotten_score">Rotten Tomatoes Score</label>

		</th>

		<td>

			<input type="number" step=".01" id="am_rotten" name="am-rotten" value="<?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'rotten', true); ?>" />

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="am_trailer">Youtube Trailer</label>

		</th>

		<td>

			<input type="text" id="am_trailer" name="am-trailer" value="<?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'trailer', true); ?>" />

		</td>

	</tr>

	<tr valign="top">

		<th class="metabox_label_column">

			<label for="am_image">Poster Image</label>

		</th>

		<td>

			<input type="text" id="am_image" name="am-image" value="<?php echo @get_post_meta($post->ID, MoviePostType::PREFIX.'image', true); ?>" />

		</td>

	</tr>

</table>