<?php $dates = Adbay_Movie_Manager::create_theater_date_block($escape_room, $theater_id); ?>
<!-- Start Movie-Listing -->
<div class="movie-listing">
  
		<!-- Start Movie-Times -->

			<div class="movie-times max-width">
        <p class="active-date">
          Showtimes for:
          <span id="display_date" <?php if($escape_room == 'yes') { echo 'style="color: '.get_field('cta_color', 3390).';"'; } ?>><?php echo date('l, F d, Y',strtotime($dates[0]['date'])); ?></span>
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
            <path <?php if( $escape_room == 'yes' ) { echo 'fill="'.get_field('cta_color', 3390).'"'; } else { echo 'fill="#9ecb51"'; } ?> d="M12.5 14.57L7.53 9.6a.8.8 0 0 0-1.11 0 .8.8 0 0 0 0 1.11l5.53 5.52c.3.31.8.31 1.1 0l5.53-5.52a.8.8 0 0 0 0-1.11.8.8 0 0 0-1.11 0zM1.56 12.5a10.94 10.94 0 1 1 21.89 0 10.94 10.94 0 0 1-21.89 0zM0 12.5a12.5 12.5 0 1 0 25 0 12.5 12.5 0 0 0-25 0z"></path>
          </svg>
        </p>
        <div class="date-selection">
          <?php $x=0;
            foreach($dates as $date) {
              if($x==0) {
                echo '<button type="button" class="am_date_picker" value="'.$date['date'].'" disabled="disabled"><span>'.$date['button'].'</span></button>';
                $x++;
              } else {
                echo '<button type="button" class="am_date_picker" value="'.$date['date'].'"><span>'.$date['button'].'</span></button>';
              }
            } ?>
        </div>
      </div>
    <!-- End Movie-Times -->

    <!-- Start Movies -->
    <div class="movies <?php if ( is_page(1484) || is_page(1485) || is_page(1486) || is_page(1483) || is_page(62605) || is_page(62607) ) { ?>location-movie-slider max-width<?php } ?>" id="movie_list">
    </div>
    <!-- End Movies -->
</div>
<!-- End Movie-Listing -->

<script type="text/javascript">
var ajaxurl="<?php echo admin_url('admin-ajax.php'); ?>";
jQuery(function($){
	var buttons = $(".am_date_picker");
	buttons.click(function(){
		buttons.removeAttr("disabled");;
		$(this).attr("disabled","disabled");
		var date_mult = $(this).val();
		get_times(date_mult);
	});
	get_times(buttons.eq(0).val());
});

function get_times(date_mult) {
	var theater_id = <?php echo (!empty($theater_id)) ? $theater_id : 0; ?>;
	var mdata = {
		date: date_mult,
    theater: theater_id,
		escape_room: "<?php echo $escape_room; ?>",
		action:'amp_get_showtimes'
	};
	jQuery.post(ajaxurl,mdata,
		function(response_data){
			if (response_data.success){
				jQuery("#movie_list").html(response_data.result);
			}
			else {
				jQuery("#movie_list").html("<p>No showtime currently available.</p>");
			}
			jQuery("#display_date").html(response_data.date);
      jQuery('.location-movie-slider').slick({
          dots: true,
          infinite: false,
          speed: 300,
          slidesToShow: 2,
          slidesToScroll: 2,
          responsive: [
            {
              breakpoint: 700,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
		}, "json");
}

</script>
