<?php $dates = Adbay_Movie_Manager::create_theater_date_block($theater_id); ?>

<!-- Start Movie-Listing -->

<div class="movie-listing">



	<!-- Start Movie-Times -->

    <div class="movie-times">

        <strong>Showtimes for: </strong>



        	<!-- Start Display_Date -->

        	<div id="amusement_display_date">

			<?php echo date('l, F d, Y',strtotime($dates[0]['date'])); ?>

            </div><br>

            <!-- End Display_Date -->

            <?php

		$x=0;

		foreach($dates as $date){

			if($x==0)

			{

				echo '<button type="button" class="amusement_date_picker" value="'.$date['date'].'" disabled="disabled">'.$date['button'].'</button>';

				$x++;

			}

			else

			{

				echo '<button type="button" class="amusement_date_picker" value="'.$date['date'].'">'.$date['button'].'</button>';

			}

		}?>



    </div>

    <!-- End Movie-Times -->

            <!-- Start Movies -->

            <ul class="movies" id="amusement_list">



            </ul>

            <!-- End Movies -->



</div>

<!-- End Movie-Listing -->



<script type="text/javascript">

var ajax_url="<?php echo admin_url('admin-ajax.php'); ?>";

jQuery(function($){

	var buttons = $(".amusement_date_picker");

	buttons.click(function(){

		buttons.removeAttr("disabled");;

		$(this).attr("disabled","disabled");

		var date_mult = $(this).val();

		get_amusement_times(date_mult);

	});

	get_amusement_times(buttons.eq(0).val());

});



function get_amusement_times(date_mult) {

	var theater_id = <?php echo (!empty($theater_id)) ? $theater_id : 0; ?>;

	var mdata = {

		date: date_mult,

		theater: theater_id,

		type:'amusement',

		action:'amp_get_showtimes'

	};

	jQuery.post(ajax_url,mdata,

		function(response_data){

			if (response_data.success){

				jQuery("#amusement_list").html(response_data.result);

			}

			else {

				jQuery("#amusement_list").html("<p>No showtime currently available.</p>");

			}

			jQuery("#amusement_display_date").html(response_data.date);

		}, "json");

}

</script>