<?php /*
THE TEMPLATE FOR DISPLAYING 404 PAGES (BROKEN LINKS)
*/ ?>

<?php get_header(); ?>

<main class="page-contents full-width">

	<section class="not-found">
      <div id="error-option-404" class="error-option">
        <h1>404</h1>
        <h2>Sorry, the page you requested could not be found.</h2>
        <p>You can either try using our main navigation above or try a key word search using the form below</p>
        <?php get_search_form(); ?>
      </div>
	</section>

</main>

<?php get_footer(); ?>
