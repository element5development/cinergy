<?php /*
THE FOOTER TEMPLATE FOR OUR THEME
*/ ?>
      
<!-- SITE NAVIGATION -->
  <?php get_template_part( 'template-parts/footer/content', 'page-footer' ); ?>

  <?php wp_footer(); ?>
</body>
</html>