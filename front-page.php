<?php /*
HOME PAGE TEMPLATE 
*/ ?>

<?php get_header(); ?>

<main class="page-contents full-width">

  <div class="pge-bg dark-bg">

    <!-- PAGE TITLE -->
    <?php get_template_part( 'template-parts/pages/content', 'page-title' ); ?>

    <!-- HOME PAGE LEARN MORE -->
    <div class="home-additions flex-container">
      <svg id="close-additions" xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37">
        <path d="M21.77 18.5l4.08-4.08a2.32 2.32 0 0 0-3.27-3.27l-4.08 4.08-4.2-4.08c-.9-.9-2.36-.9-3.27 0-.9.9-.67 2.37.24 3.27l3.96 4.08-4.2 4.19a2.32 2.32 0 0 0 3.27 3.28l4.2-4.2 4.19 4.2a2.32 2.32 0 0 0 3.27-3.28zm-19.46 0a16.2 16.2 0 1 1 32.38 0 16.2 16.2 0 0 1-32.38 0zM0 18.5a18.5 18.5 0 1 0 37 0 18.5 18.5 0 0 0-37 0z"></path>
      </svg>
      <?php if( have_rows('additions') ) {
        while ( have_rows('additions') ) { the_row(); 
          $post_object = get_sub_field('post');
          if( $post_object ) { $post = $post_object; setup_postdata( $post ); ?>
            <?php 
              if ( get_the_post_thumbnail_url() ) {
                $thumb_url = get_the_post_thumbnail_url();
              } elseif ( $post->post_type == "food" ) {
                if ( get_field('section') == 'Starters' || get_field('section') == 'Shareables' || get_field('section') == 'Big Group Shareables' ) {
                  $thumb_url = get_template_directory_uri().'/dist/images/starters-shareables.jpg';
                } elseif ( get_field('section') == 'Burgers & Sandwiches' ) {
                  $thumb_url = get_template_directory_uri().'/dist/images/burgers.jpg';
                } elseif ( get_field('section') == 'Tacos, Salads & More' ) {
                  $thumb_url = get_template_directory_uri().'/dist/images/tacos.jpg';
                } elseif ( get_field('section') == 'Hand-stretched Pizzas' || get_field('section') == '10" & 16" Pizzas' ) {
                  $thumb_url = get_template_directory_uri().'/dist/images/pizza.jpg';
                } elseif ( get_field('section') == 'Kids Menu' ) {
                  $thumb_url = get_template_directory_uri().'/dist/images/kids.jpg';
                } else {
                  $thumb_url = get_template_directory_uri().'/dist/images/desserts.jpg';
                }
              } else {
                $thumb_url = site_url().'/wp-content/uploads/2017/05/movies.jpg';
              }
            ?>
            <div class="one-fifth contracted" <?php if ( $post->post_type != "adbay_movie" ) { ?> style="background-image: url('<?php echo $thumb_url ?>');" <?php } ?> >
              <?php if ( $post->post_type == "adbay_movie" ) {
                $meta = get_post_meta(get_the_ID());
                movie_db_render_backdrop( get_the_title(), 'large' ); 
              } ?>

							<?php if ( $post_object->ID == 391 ) : //MOVIES CTA
								echo '<a href="'.get_permalink($post_object->ID).'#content-anchor" class="btn primary-btn"><span>Movie Showtimes</span></a>';
							elseif ( $post_object->ID == 1465 ) : //PARTIES CTA
								echo '<a href="'.get_permalink($post_object->ID).'" class="btn primary-btn"><span>Event Possibilities</span></a>';
							else:
								echo '<p class="menu-title">'. get_the_title() .'</p>';
							endif; ?>
              <div class="full-content flex-container" <?php if ( $post->post_type != "adbay_movie" ) { ?> style="background-image: url('<?php echo $thumb_url ?>');" <?php } ?> >
                <?php if ( $post->post_type == "adbay_movie" ) {
                  movie_db_render_backdrop( get_the_title(), 'large' ); 
                } ?>
                <div class="contents">
                  <h2><?php the_title(); ?></h2>
                  <?php if ( $post->post_type == "adbay_movie") { ?>
                    <p class="subheading"><?php echo $meta['am-rating'][0]; ?> | <?php echo $meta['am-runtime'][0]; ?> Min</p>
                    <p><?php echo $meta['am-description'][0]; ?></p>
                  <?php } elseif (  $post->post_type == "food" ) { ?>
                    <p class="subheading"><?php the_field('section'); ?></p>
                    <?php the_content(); ?>
                  <?php } else { ?>
                    <p><?php the_field('intro') ?></p>
                  <?php } ?>
                </div>
                <div class="btn-contents">
                  <?php if ( $post->post_type == "adbay_movie") { ?>
                    <h2>See All Movies</h2>
                    <a href="/movies/" class="btn primary-btn arrow"><span>Now Showing 
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                        <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                      </svg>
                      </span>
                    </a>
                    <a href="/movies/?filter=Coming%20Soon" class="btn secondary-btn arrow"><span>Coming Soon
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                        <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                      </svg>
                      </span>
                    </a>
                  <?php } elseif (  $post->post_type == "food" ) { ?>
                    <h2>View Menu</h2>
                    <a href="/eat-drink/" class="btn primary-btn arrow"><span>Full Menu 
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                        <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                      </svg>
                      </span>
                    </a>
									<?php } elseif (  $post_object->ID == 391 ) { ?>
										<h2>See All Movies</h2>
										<a href="<?php echo get_permalink($post_object->ID); ?>#content-anchor" class="btn primary-btn arrow"><span>View Showtimes 
											<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
												<path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
											</svg>
											</span>
										</a>
										<a href="<?php echo get_permalink($post_object->ID); ?>/?filter=Coming%20Soon#content-anchor" class="btn secondary-btn arrow"><span>Coming Soon 
											<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
												<path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
											</svg>
											</span>
										</a>
									<?php } elseif (  $post_object->ID == 1465 ) { ?>
										<h2>Event Possibilities</h2>
										<a href="<?php echo get_permalink($post_object->ID); ?>" class="btn primary-btn arrow"><span>View Possibilities 
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                        <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                      </svg>
                      </span>
                    </a>
										<?php if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC 
												?><a target="_blank" href="https://cinergycopperascove.agilebooking.com/bookings/index.asp" class="btn secondary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
										} else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID 
												?><a target="_blank" href="https://cinergymidland.agilebooking.com/bookings/index.asp" class="btn secondary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
										} else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES
												?><a target="_blank" href="https://cinergyodessa.agilebooking.com/bookings/index.asp" class="btn secondary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
										} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION Amarillo 
											?><a target="_blank" href="https://cinergyamarillo.agilebooking.com/bookings/index.asp" class="btn secondary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
										} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION Tulsa
											?><a target="_blank" href="https://cinergytulsa.agilebooking.com/bookings/index.asp" class="btn secondary-btn arrow" onClick="ga('send', 'event', { eventCategory: 'Book An Event', eventAction: 'Click', eventLabel: 'Lead'});"><span>book a birthday<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path></svg></span></a><?php
										} else { //NO LOCATION IS SET 
										} 
										?>
									<?php } elseif (  $post->post_type == "page" ) { ?>
										<a href="/play/" class="btn primary-btn arrow"><span>Learn More 
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                        <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                      </svg>
                      </span>
                    </a>
									<?php } else { ?>
                    <h2>See More</h2>
                    <a href="/play/" class="btn primary-btn arrow"><span>All Attractions 
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                        <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                      </svg>
                      </span>
                    </a>
                  <?php } ?>



                </div>
              </div>
            </div>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
          <?php }
        }
      } ?>
    </div>

    <!-- FIRST PAGE CTA -->
    <?php $post_object = get_field('parties_&_events_cta'); ?>
    <?php if( $post_object ) { $post = $post_object; setup_postdata( $post ); ?>
      <?php 
        if (get_the_post_thumbnail_url()) {
          $thumb_url = get_the_post_thumbnail_url();
        } else {
          $thumb_url = site_url().'/wp-content/uploads/2017/05/movies.jpg';
        }
      ?>
      <div class="cta-container right-contents full-width dark-bg" style="background-image: url('<?php echo $thumb_url ?>');">
        <div class="flex-container max-width">
          <div class="one-half"></div>
          <div class="one-half">
            <p class="label">the event possibilities are limitless!</p>
            <h2><?php the_title(); ?>
            <span><?php the_field('subheading'); ?></span></h2>
            <?php the_excerpt(); ?>
            <p class="btn-container">
              <a href="/parties-events/" class="btn secondary-btn arrow"><span>Learn More
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                  <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                </svg>
                </span>
              </a>
            </p>
          </div>
        </div>
        <div class="overlay"></div>
      </div>
      <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php } ?>

    <!-- SERVICES -->
    <?php if( have_rows('services') ) { ?>
      <div class="services full-width">
        <h2>What We Offer</h2>
        <div class="flex-container max-width">
          <?php while ( have_rows('services') ) { the_row(); ?>
              <div class="service-preview post-preview-container one-third">
                <?php $image = get_sub_field('icon'); ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <h3><?php the_sub_field('title'); ?></h3>
                <?php the_sub_field('description') ?>
              </div>
          <?php } ?>
        </div>
      </div>
    <?php } ?>

    <!-- SECOND FEATURED SPECIAL -->
    <?php $post_object = get_field('menu_cta'); ?>
    <?php if( $post_object ) { $post = $post_object; setup_postdata( $post ); ?>
      <?php 
        if (get_the_post_thumbnail_url()) {
          $thumb_url = get_the_post_thumbnail_url();
        } else {
          $thumb_url = site_url().'/wp-content/uploads/2017/05/movies.jpg';
        }
      ?>
      <div class="cta-container right-contents full-width dark-bg" style="background-image: url('<?php echo $thumb_url ?>');">
        <div class="flex-container max-width">
          <div class="one-half"></div>
          <div class="one-half">
            <p class="label">in theatre drinks & dining</p>
            <h2><?php the_title(); ?>
            <span><?php the_field('subheading'); ?></span></h2>
            <?php the_excerpt(); ?>
            <p class="btn-container">
              <a href="/eat-drink/" class="btn secondary-btn arrow">
                <span>
                  View Full Menu
                  <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                    <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                  </svg>
                </span>
              </a>
            </p>
          </div>
        </div>
        <div class="overlay"></div>
      </div>
      <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php } ?>
  </div>

  <!-- SPECIALS SLIDER -->
  <?php if ( get_field('special_slider_display')  == 'yes' ) { //NOT FUN CARDS?>
    <?php get_template_part( 'template-parts/elements/content', 'specials-slider' ); ?>
  <?php } ?>

  <!-- PAGE GALLERY -->
  <?php if ( get_field('gallery') ) { ?>
    <?php get_template_part( 'template-parts/elements/content', 'page-gallery' ); ?>
  <?php } ?>

  <!-- NEWSLETTER CTA -->
  <?php if ( get_field('newsletter_display')  == 'yes' ) { //NOT FUN CARDS?>
    <div class="newsletter cta-container full-width vertical-align-parent dark-bg">
      <a id="newsletter" class="styleguide-anchor"></a>
      <div class="vertical-align-child">
        <div class="max-width">
          <h2>Join the Magic<br>Be Cinergy Elite!</h2>
          <p>Free Popcorn, $5 Elite reward after 300 points, birthday movie ticket, exclusive content, sneak peeks, special offers and so much more!</p>
					<a href="https://tickets.cinergycinemas.com/Browsing/Loyalty/clubs/1" class="btn primary-btn" tabindex="0"><span>Join Now</span></a>
				</div>
      </div>
      <div class="overlay"></div>
      <div class="video-wrap" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/newsletter-bg.jpg');"> 
        <video muted="" autoplay="" loop="" poster="<?php echo get_template_directory_uri(); ?>/dist/images/newsletter-bg.jpg" class="bgvid"> 
					<source src="<?php echo get_template_directory_uri(); ?>/dist/images/newsletter-bg.mp4" type="video/mp4"> 
					<source src="<?php echo get_template_directory_uri(); ?>/dist/images/newsletter-bg.webm" type="video/webm"> 
        </video> 
      </div>
    </div>
  <?php } ?>

</main>

<?php get_footer(); ?>