<?php /*
Template Name: Landing
*/ ?>

<?php get_header('lander'); ?>

<?php $image_bg = get_field('call_to_action_background'); ?>

<!-- NOTIFICATION BAR -->
<?php if ( get_field('notification') ): ?>
<div class="notification-bar" style="background-color: <?php the_field('cta_color'); ?>; color: <?php the_field('background_color'); ?>;">
	<p style="color: inherit;">
		<?php the_field('notification'); ?>
	</p>
	<svg id="close-notification" xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37" fill="<?php the_field('background_color'); ?>">
		<path d="M21.77 18.5l4.08-4.08a2.32 2.32 0 0 0-3.27-3.27l-4.08 4.08-4.2-4.08c-.9-.9-2.36-.9-3.27 0-.9.9-.67 2.37.24 3.27l3.96 4.08-4.2 4.19a2.32 2.32 0 0 0 3.27 3.28l4.2-4.2 4.19 4.2a2.32 2.32 0 0 0 3.27-3.28zm-19.46 0a16.2 16.2 0 1 1 32.38 0 16.2 16.2 0 0 1-32.38 0zM0 18.5a18.5 18.5 0 1 0 37 0 18.5 18.5 0 0 0-37 0z"></path>
	</svg>
</div>
<?php endif; ?>

<!-- GEO -->
<script>
  //ASK FOR LOCATION ON AGE LOAD IF NO COOKIE IS SET
  document.addEventListener("DOMContentLoaded", function() {
    if ( Cookies.get('CinergyLocation') ){
     //COOKIE SET
    } else {
     getLocation();
    }
  });
  function getLocation() { //REQUEST POSITION
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(savePosition);
    } else {
      //NO PERMISSION THEN NO AUTOMATED LOCATION
      Cookies.set('CinergyLocation', 'location-none' , { expires: 1, path: '/' }); //set 24hour location cookie to no location
      $('body').addClass('location-none'); //set body class no location
      location.reload();
    }
  }
  function savePosition(position) {
    //LAT & LONG PROVIDED TO DETERMINE ZIP CODE
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var latlng = new google.maps.LatLng(lat, lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          var zip = results[0].address_components[6].long_name; 
          if ( zip === '76522' || zip === '76544' || zip === '76549' || zip === '76539') { //Cinergy Copperas Cove
            Cookies.set('CinergyLocation', 'location-cc' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-cc'); //set body class default location
            location.reload();
          } else if ( zip === '79701' || zip === '79702' || zip === '79703' || zip === '79704' || zip === '79705' || zip === '79706' || zip === '79707' ) { //Cinergy Midland
            Cookies.set('CinergyLocation', 'location-mid' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-mid'); //set body class default location
            location.reload();
          } else if ( zip === '79758' || zip === '79762' || zip === '79763' || zip === '79764' || zip === '79765' || zip === '79766') { //Cinergy Odessa
            Cookies.set('CinergyLocation', 'location-odes' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-odes'); //set body class default location
						location.reload();
					} else if ( zip === '74141' || zip === '74112' || zip === '74128' || zip === '74129' || zip === '74116' || zip === '74145' || zip === '74115' || zip === '74146' || zip === '74135' || zip === '74114' || zip === '74104' || zip === '74108' || zip === '74110' || zip === '74117' || zip === '74134' ) { //Cinergy Tulsa
            Cookies.set('CinergyLocation', 'location-tulsa' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-tulsa'); //set body class default location
            location.reload();
          } else { //IF THERE NO LOCATION NEARBY SET TO MAIN LOCATION OR SET TO ALL
            Cookies.set('CinergyLocation', 'location-none' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-none'); //set body class default location
            location.reload();
          }
        }
      }
    });
  }
  //MANUALLY SET LOCATION COOKIE
  jQuery(function() {
    jQuery("#location-cc, #location-cc-2").on('click', function(event) { //location Copperas Cove selected
      Cookies.set('CinergyLocation', 'location-cc'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-mid location-odes location-ama').addClass('location-cc'); //update body class location
    });
    jQuery("#location-mid, #location-mid-2").on('click', function(event) { //location Midland selected
      Cookies.set('CinergyLocation', 'location-mid'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-odes location-ama location-granbury location-mfalls').addClass('location-mid'); //update body class location
    });
    jQuery("#location-odes, #location-odes-2").on('click', function(event) { //location Odessa selected
      Cookies.set('CinergyLocation', 'location-odes'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-ama location-granbury location-mfalls').addClass('location-odes'); //update body class location
		});
		jQuery("#location-ama, #location-ama-2").on('click', function(event) { //location Amarillo selected
      Cookies.set('CinergyLocation', 'location-ama'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-granbury location-mfalls').addClass('location-ama'); //update body class location
		});
		jQuery("#location-tulsa, #location-tulsa-2").on('click', function(event) { //location Tulsa selected
      Cookies.set('CinergyLocation', 'location-tulsa'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-ama location-granbury location-mfalls').addClass('location-tulsa'); //update body class location
		});
		jQuery("#location-granbury, #location-granbury-2").on('click', function(event) { //location Granbury selected
      Cookies.set('CinergyLocation', 'location-granbury'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-ama location-mfalls').addClass('location-granbury'); //update body class location
		});
		jQuery("#location-mfalls, #location-mfalls-2").on('click', function(event) { //location mfalls selected
      Cookies.set('CinergyLocation', 'location-mfalls'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-ama location-granbury').addClass('location-mfalls'); //update body class location
    });
  })
</script>
<!-- GEO -->


<header class="page-title" style="background-image: url(<?php the_field('header_background'); ?>); background-color: <?php the_field('background_color'); ?>; color: <?php the_field('text_color'); ?>;">
	<!-- GEO -->
		<?php if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC 
			?><div class="locaiton-picker location-cc"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID 
			?><div class="locaiton-picker location-mid"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES
			?><div class="locaiton-picker location-odes"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION Amarillo
			?><div class="locaiton-picker location-ama"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION Tulsa
			?><div class="locaiton-picker location-tulsa"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION Granbury
			?><div class="locaiton-picker location-granbury"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION Marbel Falls
			?><div class="locaiton-picker location-mfalls"><?php
		} else { //NO LOCATION IS SET 
			?><div class="locaiton-picker location-none"><?php
		} ?>
      <svg id="location-pointer" xmlns="http://www.w3.org/2000/svg" width="25" height="32" viewBox="0 0 25 32">
        <path d="M20.65 18l-8.26 10.62L4.17 18a9.77 9.77 0 0 1-2.11-6c0-5.52 4.62-10 10.33-10 5.7 0 10.32 4.48 10.32 10 0 2.26-.81 4.32-2.11 6zM0 12c0 2.19.62 4.23 1.67 6h.02L12.4 32l10.7-14a11.61 11.61 0 0 0 1.67-6c0-6.63-5.54-12-12.38-12C5.55 0 0 5.37 0 12z"></path>
        <path d="M8.06 12c0-2.21 1.85-4 4.13-4a4.07 4.07 0 0 1 4.13 4c0 2.21-1.85 4-4.13 4a4.07 4.07 0 0 1-4.13-4zM6 12c0 3.31 2.77 6 6.19 6a6.1 6.1 0 0 0 6.2-6c0-3.31-2.78-6-6.2-6A6.1 6.1 0 0 0 6 12z"></path>
      </svg>
      <div class='pulse'></div>
      <p class="active">
        Your Location:<br/>
        <span id="demo">
          <?php
            if ($_COOKIE['CinergyLocation'] == 'location-cc') { echo 'Copperas Cove'; }
            else if ($_COOKIE['CinergyLocation'] == 'location-mid') { echo 'Midland'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-odes') { echo 'Odessa'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-ama') { echo 'Amarillo'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-tulsa') { echo 'Tulsa'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-granbury') { echo 'Granbury'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-mfalls') { echo 'Marble Falls'; }
            else { echo 'not set'; }
          ?>
        </span>
      </p>
      <p class="location-options label">Set my location</p>
      <div class="location-option-container">
        <div id="location-cc" class="btn primary"><span>Copperas Cove</span></div>
        <div id="location-mid" class="btn primary"><span>Midland</span></div>
        <div id="location-odes" class="btn primary"><span>Odessa</span></div>
				<div id="location-ama" class="btn primary"><span>Amarillo</span></div>
				<div id="location-tulsa" class="btn primary"><span>Tulsa</span></div>
				<div id="location-granbury" class="btn primary"><span>Granbury</span></div>
				<div id="location-mfalls" class="btn primary"><span>Marble Falls</span></div>
      </div>
    </div>

	<!-- GEO -->
	<a class="company" href="/">
		<img src="/wp-content/uploads/2016/10/Cinergy-Logo-White.svg" alt="Cinergy" />
	</a>
	<div class="block">
		<?php if ( get_field('logo__icon') ) : ?>
			<?php $logo = get_field('logo__icon'); ?>
			<div class="logo">
				<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
			</div>
		<?php endif; ?>
		<h1 style="color: inherit;">
			<?php the_field('page_title'); ?>
		</h1>
		<?php if ( get_field('page_description') ): ?>
		<p style="color: inherit;">
			<?php the_field('page_description'); ?>
		</p>
		<?php endif; ?>
		<nav>
			<a class="secondary-btn smoothScroll" href="#anchor-three" style="color: inherit;">
				<?php the_field('form_headline'); ?>
			</a>
			<a class="primary-btn smoothScroll" href="#anchor-one" style="background-color: <?php the_field('cta_color'); ?>; color: <?php the_field('background_color'); ?>;">
			<?php the_field('items_headline'); ?>
			</a>
			<a class="secondary-btn smoothScroll" href="#anchor-two" style="color: inherit;">
				<?php the_field('single_column_headline'); ?>
			</a>
		</nav>
	</div>
</header>
<hr style="background-color: <?php the_field('cta_color'); ?>;">

<section class="two-column" style="background-color: <?php the_field('background_color'); ?>; color: <?php the_field('text_color'); ?>;">
	<div class="block">
		<?php the_field('left_column'); ?>
	</div>
	<div class="block">
		<?php the_field('right_column'); ?>
	</div>
</section>

<?php if ( have_rows('items') ) : ?>
<section class="items-block" style="background-color: <?php the_field('background_color'); ?>; color: <?php the_field('text_color'); ?>;">
	<a id="anchor-one" class="anchor"></a>
	<h2>
		<?php the_field('items_headline'); ?>
	</h2>
	<div class="items">
		<?php if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION Midland ?>
			<?php echo do_shortcode('[showtimes theater_id="103" escape_room="yes"][/showtimes]'); ?>
		<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION Amarillo ?>
			<?php echo do_shortcode('[showtimes theater_id="105" escape_room="yes"][/showtimes]'); ?>
		<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION Tulsa ?>
			<?php echo do_shortcode('[showtimes theater_id="106" escape_room="yes"][/showtimes]'); ?>
		<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION Odessa ?>
			<h4 style="text-align: center; margin-bottom: 40px;">This Location currently has no escape rooms.</h4>
		<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION Copperas Cove ?>
			<h4 style="text-align: center; margin-bottom: 40px;">This Location currently has no escape rooms.</h4>
		<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION Copperas Cove ?>
			<h4 style="text-align: center; margin-bottom: 40px;">This Location currently has no escape rooms.</h4>
		<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION Copperas Cove ?>
			<h4 style="text-align: center; margin-bottom: 40px;">This Location currently has no escape rooms.</h4>
		<?php } else { //NO LOCATION IS SET ?>
      <div class="location-option-container">
				<div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
				<div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
				<div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
				<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
				<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
				<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
				<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
      </div>
		<?php } ?>
	</div>
</section>
<?php endif; ?>

<?php if ( get_field('form') ) : ?>
<section class="form-block" style="background-image: url('<?php echo $image_bg['url']; ?>'); background-color: <?php the_field('background_color'); ?>; color: <?php the_field('text_color'); ?>;">
<a id="anchor-three" class="anchor"></a>
	<div class="block">
		<h2><?php the_field('form_headline'); ?></h2>
		<?php the_field('form'); ?>
	</div>
	<div class="overlay"></div>
</section>
<?php endif; ?>

<?php if ( get_field('single_column_headline') ) : ?>
<section class="single-block" style="background-color: <?php the_field('background_color'); ?>; color: <?php the_field('text_color'); ?>;">
	<a id="anchor-two" class="anchor"></a>
	<div class="block">
		<h2>
			<?php the_field('single_column_headline'); ?>
		</h2>
		<?php the_field('single_column'); ?>
	</div>
</section>
<?php endif; ?>

<footer style="background-color: <?php the_field('background_color'); ?>; color: <?php the_field('text_color'); ?>;">
	<div class="location-links">
		<div class="max-width">
			View Locations:
			<?php
			$childArgs = array(
					'sort_order' => 'ASC',
					'child_of' => '1481'
			);
			$childList = get_pages($childArgs);
			foreach ($childList as $child) { ?>
				<a href="<?php echo get_permalink($child->ID); ?>">
					<?php the_field('location_name', $child->ID); ?>
				</a>
				<?php } ?>
		</div>
	</div>
	<nav>
		<div class="max-width">
			<?php wp_nav_menu( array( 'theme_location' => 'primary-nav' ) ); ?>
		</div>
	</nav>
	<div class="copyright-container">
		<div class="max-width">
			<p id="copyright">©Copyright
				<?php echo date('Y'); ?> Cinergy Entertainment Group. All Rights Reserved.
		</div>
	</div>
	<div class="overlay"></div>
</footer>

<?php get_footer('lander'); ?>