<?php /*
FOOTER CONTENT
*/ ?>

<section class="social-footer">
	<div class="flex-container max-width">
		<h2>Social Stream</h2>
		<div class="social-links">
			<b>Follow us</b>
			<?php if ( get_field('facebook-url', 'option') ) { ?>
				<a href="<?php the_field('facebook-url', 'option'); ?>" target="_blank" class="facebook social-link">
					<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
						<path fill="none" stroke="#3b5998" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
						<path class="color-fill" fill="#fff" d="M26.93 13.51v2.9s-2.13-.22-2.67.6c-.29.45-.12 1.76-.14 2.7h2.83c-.24 1.1-.41 1.85-.59 2.8H24.1v9.01h-3.93v-8.97H18.5v-2.84h1.65c.09-2.07.12-4.12 1.15-5.17 1.16-1.18 2.26-1.03 5.63-1.03"></path>
					</svg>
				</a>
			<?php } ?>
			<?php if ( get_field('instagram-url', 'option') ) { ?>
				<a href="<?php the_field('instagram-url', 'option'); ?>" target="_blank" class="instagram social-link">
					<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
						<defs>
							<linearGradient xmlns="http://www.w3.org/2000/svg" id="a" x1="0%" y1="0%" x2="0%" y2="100%">
								<stop offset="0" stop-color="#60f"></stop>
								<stop offset=".1" stop-color="#3771c8"></stop>
								<stop offset=".3" stop-color="#c837ab"></stop>
								<stop offset=".87" stop-color="#ff543e"></stop> 
								<stop offset="1" stop-color="#fd5"></stop>
							</linearGradient>
						</defs>
						<path id="border" fill="none" stroke="url(#a)" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
						<path class="color-fill" fill="#fff" d="M19.2 14.55c-.85.04-1.43.17-1.94.37a3.91 3.91 0 0 0-2.34 2.32c-.2.51-.33 1.09-.37 1.94-.04.84-.05 1.11-.05 3.27 0 2.17.01 2.44.05 3.28.04.85.17 1.43.37 1.94.2.52.48.96.92 1.4.45.45.89.72 1.42.92.51.2 1.09.33 1.94.37.85.04 1.13.05 3.3.05s2.44-.01 3.3-.05a5.85 5.85 0 0 0 1.94-.37 4 4 0 0 0 2.34-2.32c.2-.51.33-1.09.37-1.94.04-.84.05-1.11.05-3.28 0-2.16-.01-2.43-.05-3.27a5.85 5.85 0 0 0-.37-1.94 4.13 4.13 0 0 0-2.34-2.32 5.85 5.85 0 0 0-1.94-.37c-.86-.04-1.13-.05-3.3-.05s-2.45.01-3.3.05zm6.53 1.43c.78.04 1.2.16 1.49.27a2.57 2.57 0 0 1 1.51 1.51c.11.28.24.7.28 1.48.04.84.05 1.09.05 3.21 0 2.13-.01 2.38-.05 3.22a4.5 4.5 0 0 1-.28 1.48 2.57 2.57 0 0 1-1.51 1.51c-.29.1-.71.23-1.49.27-.84.04-1.1.05-3.23.05-2.14 0-2.39-.01-3.23-.05a4.9 4.9 0 0 1-1.49-.27 2.57 2.57 0 0 1-.92-.6 2.54 2.54 0 0 1-.6-.91 4.32 4.32 0 0 1-.27-1.48c-.04-.84-.05-1.09-.05-3.22 0-2.12.01-2.37.05-3.21.03-.78.16-1.2.27-1.48.15-.37.32-.63.6-.91.28-.28.55-.45.92-.6a4.6 4.6 0 0 1 1.49-.27c.84-.04 1.09-.05 3.23-.05 2.13 0 2.39.01 3.23.05z"></path>
						<path class="color-fill" fill="#fff" d="M19.94 22.58a2.66 2.66 0 0 1 2.67-2.65 2.66 2.66 0 1 1 0 5.31 2.66 2.66 0 0 1-2.67-2.66zm-1.44.01a4.1 4.1 0 0 0 8.21 0 4.1 4.1 0 1 0-8.21 0zM27.42 18.45a.96.96 0 0 1-1.92 0 .96.96 0 0 1 1.92 0"></path>
					</svg>
				</a>
			<?php } ?>
			<?php if ( get_field('twitter-url', 'option') ) { ?>
				<a href="<?php the_field('twitter-url', 'option'); ?>" target="_blank" class="twitter social-link">
					<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
						<path fill="none" stroke="#00aced" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
						<path class="color-fill" fill="#fff" d="M25.7 17.5a3.23 3.23 0 0 1 2.44.95 5.1 5.1 0 0 0 1.95-.73 3 3 0 0 1-.99 1.42c-.11.08-.21.19-.34.24v.01c.67-.01 1.22-.31 1.74-.47v.01a4.9 4.9 0 0 1-1.04 1.18l-.48.38c0 .69-.01 1.36-.15 1.94-.77 3.4-2.8 5.71-6.03 6.7-1.15.36-3.02.5-4.35.18a11.3 11.3 0 0 1-1.81-.58 7.1 7.1 0 0 1-.87-.45l-.27-.16c.3.01.65.09.98.04.31-.05.6-.04.88-.1a6.5 6.5 0 0 0 1.85-.65c.26-.15.65-.33.83-.54a3.1 3.1 0 0 1-2.87-2.1c.31.03 1.17.11 1.38-.06a2.29 2.29 0 0 1-1.01-.4 2.91 2.91 0 0 1-1.45-2.59l.32.14c.2.09.4.13.64.18.11.02.31.08.42.04h-.01c-.16-.18-.41-.3-.56-.49a3.03 3.03 0 0 1-.36-3.54l.01.01c.06.12.19.21.28.32a9.06 9.06 0 0 0 6.06 2.83 3.01 3.01 0 0 1 2.44-3.67l.37-.04"></path>
					</svg>
				</a>
			<?php } ?>
			<?php if ( get_field('youtube-url', 'option') ) { ?>
				<a href="<?php the_field('youtube-url', 'option'); ?>" target="_blank" class="youtube social-link">
					<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
						<path fill="none" stroke="#b00" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
						<path class="color-fill" fill="#fff" d="M22.46 25.55l-.03-5.04 3.51 2.54zm-4.08-8.05c-1.36 0-2.88.91-2.88 2.24v6.62c0 1.33 1.52 2.14 2.88 2.14h10.65c1.36 0 2.46-.81 2.46-2.14v-6.62c0-1.33-1.1-2.24-2.46-2.24z"></path>
					</svg>
				</a>
			<?php } ?>
		</div>
	</div>
	<div class="stream">
		<?php echo do_shortcode('[wdi_feed id="1"]'); ?>
	</div>
</section>
<footer id="footer" class="dark-bg">
	<div class="location-links">
		<div class="max-width">
		View Locations: <!--<a href="/locations/amarillo/">Amarillo</a> <span>|</span> --> <a href="/locations/copperas-cove/ ">Copperas Cove</a> <span>|</span> <a href="/locations/midland/">Midland</a> <span>|</span> <a href="/locations/odessa/">Odessa</a>
		</div>
	</div>
	<nav>
		<div class="max-width">
			<?php wp_nav_menu( array( 'theme_location' => 'primary-nav' ) ); ?>
		</div>
	</nav> 
	<div class="copyright-container">
		<div class="max-width">
			<p id="copyright">©Copyright <?php echo date('Y'); ?> Cinergy Entertainment Group. All Rights Reserved.
			<p id="e5-credit"><a href="/privacy-policy/">Privacy Policy</a><a href="/policy-faq/">Policy & FAQ</a><a href="/advertising/ ">Advertising</a><a href="https://element5digital.com/">Crafted by Element5</a></p>
		</div>
	</div>
</footer>