<?php //GET FEATURED IMAGE
if ( has_post_thumbnail() ) {
  $thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  $thumb_url = $thumb_url_array[0];
} elseif ( 'adbay_movie' == get_post_type() ) {
  $thumb_url_array = get_field('default_movie_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} elseif ( 'deals' == get_post_type() ) {
  $thumb_url_array = get_field('default_special_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} elseif ( 'post' == get_post_type() ) {
  $thumb_url_array = get_field('default_post_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else {
  $thumb_url_array = get_field('default_page_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
}
?> 

<?php if ( 'deals' == get_post_type() ) { ?>
	<div class="special-preview post-preview-container one-third">
		<p class="label">
			<?php
				$categories = get_the_category();
				$separator = ', ';
				$output = '';
				if ( ! empty( $categories ) ) {
						foreach( $categories as $category ) {
								$output .= esc_html( $category->name ) . $separator;
						}
						echo trim( $output, $separator );
				}
			?>
		</p>
		<?php 
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'small', true);
			$thumb_url = $thumb_url_array[0];
		?>
		<img src="<?php echo $thumb_url; ?>" alt="special preview" />
		<h3><?php the_title(); ?></h3>
		<p class="sub-heading"><?php the_field('subheading'); ?></p>
		<div class="details">
			<p>
				<?php the_field('summary'); ?>
				<?php if ( get_field('link_to') ) { ?>
					<a href="<?php the_field('link_to');?>">Learn More</a>
				<?php } ?>
			</p>
			<p class="legal-text"><?php the_field('legal_copy'); ?></p>
		</div>
	</div>
<?php } else if ( 'adbay_movie' == get_post_type() ) { ?>
  <?php $meta = get_post_meta(get_the_ID()); ?>
  <a href="<?php the_permalink(); ?>" class="post-preview post-preview-container flex-container max-width">
    <div class="image one-third" style="background-image: url(
      <?php
        if(file_exists(MOVIE_BASE_DIR."/images/movies/".$meta['am-image'][0])) {
          $poster =plugins_url('adbay-movies/images/movies/'.$meta['am-image'][0]);
        } else {
          $poster ='https://tickets.cinergycinemas.com/CDN/Image/Entity/FilmPosterGraphic/'.$meta['am-image'][0];
        } echo $poster;
      ?>
    );">
      <div class="overlay vertical-align-parent">
        <div class="cta vertical-align-child">
          <p>read the rest</p>
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
            <path fill="#9ecb51" d="M12.5 14.57L7.53 9.6a.8.8 0 0 0-1.11 0 .8.8 0 0 0 0 1.11l5.53 5.52c.3.31.8.31 1.1 0l5.53-5.52a.8.8 0 0 0 0-1.11.8.8 0 0 0-1.11 0zM1.56 12.5a10.94 10.94 0 1 1 21.89 0 10.94 10.94 0 0 1-21.89 0zM0 12.5a12.5 12.5 0 1 0 25 0 12.5 12.5 0 0 0-25 0z"></path>
          </svg>
        </div>
      </div>
    </div>
    <div class="info one-third">
      <h1><?php the_title(); ?></h1>
    </div>
    <div class="details one-third">
      <?php the_excerpt() ?>
    </div>
  </a>
<?php } else { ?>
  <a href="<?php the_permalink(); ?>" class="post-preview post-preview-container flex-container max-width">
    <div class="image one-third" style="background-image: url('<?php echo $thumb_url; ?>');">
      <div class="overlay vertical-align-parent">
        <div class="cta vertical-align-child">
          <p>read the rest</p>
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
            <path fill="#9ecb51" d="M12.5 14.57L7.53 9.6a.8.8 0 0 0-1.11 0 .8.8 0 0 0 0 1.11l5.53 5.52c.3.31.8.31 1.1 0l5.53-5.52a.8.8 0 0 0 0-1.11.8.8 0 0 0-1.11 0zM1.56 12.5a10.94 10.94 0 1 1 21.89 0 10.94 10.94 0 0 1-21.89 0zM0 12.5a12.5 12.5 0 1 0 25 0 12.5 12.5 0 0 0-25 0z"></path>
          </svg>
        </div>
      </div>
    </div>
    <div class="info one-third">
      <?php if ( 'post' == get_post_type() ) { ?>
        <p class="date"><?php the_date() ?></p>
      <?php } ?>
      <h1><?php the_title(); ?></h1>
    </div>
    <div class="details one-third">
      <p class="label"><?php
        $category_detail=get_the_category( $post->ID );
        foreach($category_detail as $cd){
          echo '<span>'.$cd->cat_name.'</span>';
        }
      ?></p>
      <?php the_excerpt() ?>
    </div>
  </a>
<?php } ?>