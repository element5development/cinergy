<?php /*
Simple CTA Section
*/ ?>
<?php $image = get_field('simple_cta_section_background_image'); ?>
<div class="cta-container full-width vertical-align-parent dark-bg" style="background-image: url('<?php echo $image['url']; ?>');">
  <div class="vertical-align-child max-width">
    <h2><?php the_field('simple_cta_section_title'); ?>
    <span><?php the_field('simple_cta_section_subheading'); ?></span></h2>
    <p class="btn-container">
      <a href="<?php the_field('simple_cta_section_cta_link'); ?>" class="btn primary-btn arrow"><span><?php the_field('simple_cta_section_cta_text'); ?>
        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
          <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
        </svg>
        </span>
      </a>
    </p>
  </div>
  <div class="overlay"></div>
</div>