<?php /*
SPECIALS SLIDER
*/ ?>

<section class="special-slider-container"> 
   <?php
      if ($_COOKIE['CinergyLocation'] == 'location-cc') { echo "<h2>Copperas Cove's Specials</h2>"; }
      else if ($_COOKIE['CinergyLocation'] == 'location-mid') { echo "<h2>Midland's Specials</h2>"; }
			else if ($_COOKIE['CinergyLocation'] == 'location-odes') { echo "<h2>Odessa's Specials</h2>"; }
			else if ($_COOKIE['CinergyLocation'] == 'location-ama') { echo "<h2>Amarillo's Specials</h2>"; }
			else if ($_COOKIE['CinergyLocation'] == 'location-tulsa') { echo "<h2>Tulsa's Specials</h2>"; }
			else if ($_COOKIE['CinergyLocation'] == 'location-granbury') { echo "<h2>Granbury's Specials</h2>"; }
			else if ($_COOKIE['CinergyLocation'] == 'location-mfalls') { echo "<h2>Marble Falls' Specials</h2>"; }
      else { echo '<h2>Specials</h2>'; }
    ?>

    <?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED ?>
      <h3 class="align-center">Select a location to view specials</h3>
      <div class="location-option-container">
        <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
        <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
        <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
				<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
				<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
				<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
				<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
      </div>
      <div class="overlay"></div>
    <?php } else { //LOCATION SET ?>
			<?php //QUERY ALL FEATURED SPECIALS
				$args = array( 
					'posts_per_page'  => -1, 
					'post_type' => 'Deals',
					'meta_query' => array(
						array(
							'key' => 'location_based', // name of custom field
							'value' => $_COOKIE["CinergyLocation"], // matches exactly "red"
							'compare' => 'LIKE'
						)
					));
				$query = new WP_Query( $args );
			?>
			<?php if ( $query->have_posts() ) { ?>
				<div class="special-slider-bg">
					<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<?php //GET FEATURED IMAGE
						if ( has_post_thumbnail() ) {
							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'small', true);
							$thumb_url = $thumb_url_array[0];
						} else {
							$thumb_url_array = get_field('default_special_image', 'options'); 
							$thumb_url = $thumb_url_array['url'];
						}
						?>
						<div class="special-bg" style="background-image:url('<?php echo $thumb_url; ?>');"></div>
					<?php } ?>
				</div>
			<?php } else { ?>
				<h3>This location currently has no active specials.</h3>
			<?php } wp_reset_postdata(); ?> 
    <?php } ?>

    <?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED ?>
    <?php } else { //LOCATION SET ?>
			<?php //QUERY ALL FEATURED SPECIALS
				$args = array( 
					'posts_per_page'  => -1, 
					'post_type' => 'Deals',
					'meta_query' => array(
						array(
							'key' => 'location_based', // name of custom field
							'value' => $_COOKIE["CinergyLocation"], // matches exactly "red"
							'compare' => 'LIKE'
						)
					));
				$query = new WP_Query( $args );
			?>
			<?php if ( $query->have_posts() ) { ?>
				<div class="special-slider">
					<?php while ( $query->have_posts() ) { $query->the_post(); ?>
						<?php $locations = get_field('location_based'); ?>
						<?php foreach( $locations  as $location ) { //CHECK ALL LOCATIONS ?>
							<?php if ( $_COOKIE["CinergyLocation"] == $location ) { //ONLY DISPLAY IF ONE OF THE LOCATIONS EQUALS SET LOCATION ?>
								<?php //GET FEATURED IMAGE
								if ( has_post_thumbnail() ) {
									$thumb_id = get_post_thumbnail_id();
									$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
									$thumb_url = $thumb_url_array[0];
								} else {
									$thumb_url_array = get_field('default_special_image', 'options'); 
									$thumb_url = $thumb_url_array['url'];
								}
								?>
								<div class="special-preview post-preview-container">
									<p class="label">
										<?php $categories = get_the_category(); ?>
										<?php foreach( $categories as $category ) { ?>
											<span><?php echo $category->name; ?></span>
										<?php } ?>
									</p>
									<img src="<?php echo $thumb_url; ?>" alt="special preview" />
									<h3><?php the_title(); ?></h3>
									<p class="sub-heading"><?php the_field('subheading'); ?></p>
									<div class="details">
										<p>
											<?php the_field('summary'); ?>
											<?php if ( get_field('link_to') ) { ?>
												<a href="<?php the_field('link_to');?>">Learn More</a>
											<?php } ?>
										</p>
										<p class="legal-text"><?php the_field('legal_copy'); ?></p>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</div>
			<?php } else { ?>
			<?php } wp_reset_postdata(); ?> 
    <?php } ?>

</section>