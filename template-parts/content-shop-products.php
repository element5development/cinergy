<?php /*
DISPLAY PRODUCT SELECTED IN THE ACF FIELD
*/ ?>

<?php $post_objects = get_field('shop_products');

if( $post_objects ): ?>
    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <div class="single-product one-half">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );?>
          <img src="<?php  echo $image[0]; ?>" alt="Cinergy Fun Card" data-id="<?php echo $loop->post->ID; ?>">
          <p><?php the_title(); ?></p>
          <a href="<?php the_permalink(); ?>" class="btn primary-btn arrow"><span>purchase
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
              <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
            </svg>
          </span>
          </a> 
        </div>
    <?php endforeach; ?>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif;

/*
*  Loop through post objects (assuming this is a multi-select field) ( don't setup postdata )
*  Using this method, the $post object is never changed so all functions need a seccond parameter of the post ID in question.
*/

$post_objects = get_field('post_objects');

if( $post_objects ): ?>
    <?php foreach( $post_objects as $post_object): ?>
        <div class="single-product">
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );?>
          <img src="<?php  echo $image[0]; ?>" alt="Cinergy Fun Card" data-id="<?php echo $loop->post->ID; ?>">
          <p><?php echo get_the_title($post_object->ID); ?></p>
          <a href="<?php echo get_permalink($post_object->ID); ?>" class="btn primary-btn arrow"><span>purchase
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
              </svg>
            </span>
          </a> 
        </div>
    <?php endforeach; ?>
<?php endif;

?>