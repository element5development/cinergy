<?php /*
CONTACT FORM END OF PAGE
*/ ?>
<section class="gallery-slider full-width dark-bg">
  <div class="gallery-container">
    <?php $images = get_field('gallery'); ?>
    <?php foreach( $images as $image ): ?>
      <div class="slide" style="background-image: url('<?php echo $image['url']; ?>');"></div>
    <?php endforeach; ?>
  </div>
  <div class="overlay"></div>
</section>