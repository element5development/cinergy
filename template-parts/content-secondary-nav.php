<?php /*
DISPLAY SECONDARY ANCHOR NAV
*/ ?>

<?php if ( is_home() || is_archive() ) { ?>
  <nav class="secondary-nav">
    <div class="max-width">
     <ul>
      <li class="menu-item"><a href="/blog">ALL</a></li>
      <?php $categories= get_categories( array( 'parent' => 2 ) ); 
        foreach ( $categories as $category ) {
          printf( '<li class="menu-item"><a href="%1$s">%2$s</a></li>',
            esc_url( get_category_link( $category->term_id ) ),
            esc_html( $category->name )
          );
        }
      ?>
     </ul>
    </div>
  </nav>
<?php } elseif ( is_page(1464) || $post->post_parent == '1464' ) { //ABOUT EXPANSION ?>
  <nav class="secondary-nav">
    <div class="max-width">
      <?php wp_nav_menu( array( 'theme_location' => 'about-nav' ) ); ?>
    </div>
  </nav>
<?php } elseif ( is_page(391) ) { //MOVIES ?>
  <?php if ( $_GET['filter'] ) {
    $filter_format = $_GET['filter'];
  } else { 
    $filter_format = 'Now Showing';
  } ?>
  <h2 class="movies-filter-title"><?php echo $filter_format ?>
    <?php
      if ($_COOKIE['CinergyLocation'] == 'location-cc') { echo 'at Copperas Cove'; }
      else if ($_COOKIE['CinergyLocation'] == 'location-mid') { echo 'at Midland'; }
			else if ($_COOKIE['CinergyLocation'] == 'location-odes') { echo 'at Odessa'; }
			else if ($_COOKIE['CinergyLocation'] == 'location-ama') { echo 'at Amarillo'; }
			else if ($_COOKIE['CinergyLocation'] == 'location-tulsa') { echo 'at Tulsa'; }
			else if ($_COOKIE['CinergyLocation'] == 'location-granbury') { echo 'at Granbury'; }
			else if ($_COOKIE['CinergyLocation'] == 'location-mfalls') { echo 'at Marble Falls'; }
      else { }
    ?>
  </h2>
  <nav class="secondary-nav">
    <div class="max-width">
     <ul>
       <li class="menu-item <?php if ( is_null($_GET['filter']) ) { echo 'current_page_item'; } ?>"><a href="/movies/">Now Showing</a></li>
       <li class="menu-item <?php if ( $_GET['filter'] == 'Coming Soon' ) { echo 'current_page_item'; } ?>"><a href="/movies/?filter=Coming Soon">Coming Soon</a></li>
       <li class="menu-item <?php if ( $_GET['filter'] == 'Sensory-Friendly' ) { echo 'current_page_item'; } ?>"><a href="/movies/?filter=Sensory-Friendly">Sensory-Friendly</a></li>
     </ul>
    </div>
  </nav>
<?php } elseif ( is_page(1467) ) { //SPECIALS ?>
  <?php if ( $_COOKIE["CinergyLocation"] != 'location-none' ) { //LOCATION SET ?>
    <nav class="secondary-nav">
      <div class="max-width">
       <ul>
         <li class="menu-item"><a href="/specials/">ALL</a></li>
         <li class="menu-item"><a href="/specials/?cat=19">Attractions</a></li>
         <li class="menu-item"><a href="/specials/?cat=34">Discounts</a></li>
         <li class="menu-item"><a href="/specials/?cat=24">Game Floor</a></li>
         <li class="menu-item"><a href="/specials/?cat=25">Movies</a></li>
         <li class="menu-item"><a href="/specials/?cat=26">Promotions</a></li>
       </ul>
      </div>
    </nav>
  <?php } ?>
<?php } elseif ( is_page(1487) ) { //GAMES & ATTRACTIONS ?>
  <nav class="secondary-nav">
    <div class="max-width">
      <ul>
        <?php if ( $_COOKIE["CinergyLocation"] != 'location-none' ) { //LOCATION SET ?>
          <?php //QUERY ALL FEATURED SPECIALS
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Play',
              );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) { $query->the_post(); ?>
              <?php $locations = get_field('location_based'); ?>
              <?php foreach( $locations  as $location ) { //CHECK ALL DEAL LOCATIONS ?>
                <?php if ( $_COOKIE["CinergyLocation"] == $location ) { //ONLY DISPLAY IF ONE OF THE LOCATIONS EQUALS SET LOCATION ?>
                  <?php $post_slug = get_post_field( 'post_name', get_post() ); ?>
                  <li class="menu-item"><a class="smoothScroll" href="/play/#<?php echo $post_slug; ?>"><?php the_title(); ?></a></li>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          <?php } ?>
          <?php wp_reset_postdata(); ?> 
        <?php } else { //NO LOCATION IS SET ?>
          <?php //QUERY ALL FEATURED SPECIALS
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Play',
              'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key'     => 'location_based',
                    'value'   => 'location-cc',
                    'compare' => 'LIKE',
                ),
                array(
                    'key'     => 'location_based',
                    'value'   => 'location-mid',
                    'compare' => 'LIKE',
                ),
                array(
                    'key'     => 'location_based',
                    'value'   => 'location-odes',
                    'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-ama',
									'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-tulsa',
									'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-granbury',
									'compare' => 'LIKE',
								),
								array(
									'key'     => 'location_based',
									'value'   => 'location-mfalls',
									'compare' => 'LIKE',
								),
              ),
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) { $query->the_post(); ?>
              <?php $post_slug = get_post_field( 'post_name', get_post() ); ?>
              <li class="menu-item"><a class="smoothScroll" href="/play/#<?php echo $post_slug; ?>"><?php the_title(); ?></a></li>
            <?php } ?>
          <?php } ?>
          <?php wp_reset_postdata(); ?> 
        <?php } ?>
      </ul>
    </div>
  </nav>
<?php } else { ?>
<?php } ?>