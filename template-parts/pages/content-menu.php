<?php /*
DISPLAY MENU
*/ ?>

<!-- ************************* -->
<!-- ************************* -->
<!--        SECONDARY NAV      -->
<!-- ************************* -->
<!-- ************************* -->
<?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED OR UNSET ?>
<?php } else { //LOCATION IS SET ?>
  <nav class="secondary-nav">
    <h2 class="max-width" style="margin-bottom: 2rem;">Menu</h2>
    <div class="max-width">
      <ul>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Starters',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#starters" class="smoothScroll">Starters</a></li>
          <?php } ?>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Shareables',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#shareables" class="smoothScroll">Shareables</a></li>
          <?php } ?>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Big Group Shareables',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#big-group" class="smoothScroll">Big Group Shareables</a></li>
          <?php } ?>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Burgers & Sandwiches',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#burgers" class="smoothScroll">Burgers & Sandwiches</a></li>
          <?php } ?>
                    <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Tacos, Salads & More',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#tacos" class="smoothScroll">Tacos, Salads & More</a></li>
          <?php } ?>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Hand-stretched Pizzas',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#hand-pizza" class="smoothScroll">Hand-stretched Pizzas</a></li>
          <?php } ?>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => '10" & 16" Pizzas',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#pizza" class="smoothScroll">10" & 16" Pizzas</a></li>
          <?php } ?>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Kids Menu',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#kids" class="smoothScroll">Kids Menu</a></li>
          <?php } ?>
          <?php 
            $args = array( 
              'posts_per_page'  => -1, 
              'post_type' => 'Food',
              'meta_query'  => array(
                'relation'    => 'AND',
                array(
                  'key'   => 'location_based',
                  'value'   => $_COOKIE["CinergyLocation"],
                  'compare' => 'LIKE'
                ),
                array(
                  'key'    => 'section',
                  'value'  => 'Desserts',
                  'compare' => '='
                ),
              )
            );
            $query = new WP_Query( $args );
          ?>
          <?php if ( $query->have_posts() ) { ?>
            <li class="menu-item"><a href="#desserts" class="smoothScroll">Desserts</a></li>
          <?php } ?>
     </ul>
    </div>
  </nav>
<?php } ?>
<!-- ************************* -->
<!-- ************************* -->
<!--           MENU            -->
<!-- ************************* -->
<!-- ************************* -->
<?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED OR UNSET ?>
  <div class="location-forced menu-locations">
    <h2>Select a location to view menu</h2>
    <div class="location-option-container">
      <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
      <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
      <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
			<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
			<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
			<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
			<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
    </div>
  </div>
<?php } else { //LOCATION IS SET ?>
  <!-- ********************* -->
  <!--        STARTERS       -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Starters',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left">
      <a id="starters" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Starters</h2></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/starters-shareables.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!--       Shareables      -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Shareables',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left">
      <a id="shareables" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Shareables</h2></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/starters-shareables.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!--  BIG GROUP SHAREABLES -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Big Group Shareables',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/starters-shareables.jpg');">
      <a id="big-group" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Big Group Shareables</h2><p class="label">Super-size appetizers – not available in auditoriums.</p></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/starters-shareables.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!--  BURGERS & SANDWICHES -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Burgers & Sandwiches',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left">
      <a id="burgers" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Burgers & Sandwiches</h2><p class="label">Served with seasoned fries.</p></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/burgers.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!--  TACOS, SALADS & MORE -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Tacos, Salads & More',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left">
      <a id="tacos" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Tacos, Salads & More</h2></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/tacos.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!-- HAND-STRETCHED PIZZAS -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Hand-stretched Pizzas',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left">
      <a id="hand-pizza" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Hand-stretched Pizzas</h2><p class="label">Pizzas are 10" hand-rolled pies.</p></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/pizza.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!--    10" & 16" PIZZAS   -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => '10" & 16" Pizzas',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/pizza.jpg');">
      <a id="pizza" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>10" & 16" Pizzas</h2><p class="label">Pizzas are 10" and 16" hand-stretched pies. 16" pizzas not available in auditoriums.</p></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/pizza.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!--        KIDS MENU      -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Kids Menu',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left">
      <a id="kids" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Kids Menu</h2></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/kids.jpg');"></div>
    </div>
  <?php } ?>
  <!-- ********************* -->
  <!--        DESSERTS       -->
  <!-- ********************* -->
  <?php 
    $args = array( 
      'posts_per_page'  => -1, 
      'post_type' => 'Food',
      'meta_query'  => array(
        'relation'    => 'AND',
        array(
          'key'   => 'location_based',
          'value'   => $_COOKIE["CinergyLocation"],
          'compare' => 'LIKE'
        ),
        array(
          'key'    => 'section',
          'value'  => 'Desserts',
          'compare' => '='
        ),
      )
    );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <div class="menu-section two_col_basic left">
      <a id="desserts" class="styleguide-anchor"></a>
      <div class="flex-container max-width">
        <div class="one-half"><span class="sticky"><h2>Desserts</h2></span></div>
        <div class="one-half">
          <?php while ( $query->have_posts() ) { $query->the_post(); ?>
            <div class="single-food-item">
              <h4><?php the_title(); ?><span><?php the_field('price'); ?></span></h4>
              <?php the_content(); ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="image-bg" style="background-image:url('<?php echo get_template_directory_uri(); ?>/dist/images/desserts.jpg');"></div>
    </div>
  <?php } ?>
  
  <?php wp_reset_postdata(); ?> 
<?php } ?>