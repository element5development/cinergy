<?php /*
DISPLAY PAGE TITLE, PAGE ICON, PAGE DESCRIPTION AND FEATURED IMAGE
*/ ?>

<?php //GET BACKGROUND IMAGE
if ( is_woocommerce() ) {
  $thumb_url_array = get_field('default_shop_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else if ( has_post_thumbnail() ) {
  $thumb_id = get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  $thumb_url = $thumb_url_array[0];
} else if ( is_home() || is_archive() ) {
  $thumb_url_array = get_field('featured_news_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else if ( is_single() ) {
  $thumb_url_array = get_field('default_post_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
} else {
  $thumb_url_array = get_field('default_page_image', 'options'); 
  $thumb_url = $thumb_url_array['url'];
}
?>


<?php if ( is_home() || is_archive() && !is_woocommerce() ) { ?>
  <?php 
    $args = array( 
      'posts_per_page'  => 1, 
      'meta_key'    => 'featured',
      'meta_value'  => 'yes'
      );
    $query = new WP_Query( $args );
  ?>
  <?php if ( $query->have_posts() ) { ?>
    <?php while ( $query->have_posts() ) { $query->the_post(); ?>
      <?php if ( has_post_thumbnail() ) {
        $thumb_id = get_post_thumbnail_id();
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
        $thumb_url = $thumb_url_array[0];
      } else if ( is_single() ) {
        $thumb_url_array = get_field('default_post_image', 'options'); 
        $thumb_url = $thumb_url_array['url'];
      } ?>
      <section class="page-title-container full-slider full-width dark-bg"  style="background-image: url('<?php echo $thumb_url; ?>');">
        <div class="max-width">
          <div class="contents">
            <h1><?php the_title(); ?></h1>
            <p class="details"><?php the_excerpt(); ?></p>
            <p class="btn-container">
              <a href="<?php the_permalink(); ?>" class="btn primary-btn arrow"><span>read the rest
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                  <path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M12 2v10h-10"></path>
                </svg>
                </span>
              </a>
            </p>  
          </div>
        </div>
        <div class="overlay"></div>
      </section>
    <?php } ?>
  <?php } ?>

<?php } else if ( is_search() ) { ?>

  <section class="page-title-container full-width dark-bg" style="background-image: url('<?php echo $thumb_url; ?>');">
    <div class="max-width">
      <h1>Search Results</h1>
    </div>
    <div class="overlay"></div>
  </section>

<?php } else if ( get_field('title_format') == 'full_slider' ) { ?>

  <section class="page-title-container full-slider full-width dark-bg">
    <div class="page-title-slider">
      <?php if( have_rows('slides') ) {
        while ( have_rows('slides') ) : the_row(); ?>

					<?php 
						if ( isset($_COOKIE['CinergyLocation']) ) {

						} else {
							$_COOKIE['CinergyLocation'] = 'location-none';
						}
					?>
					<?php if ( in_array( $_COOKIE['CinergyLocation'], get_sub_field('active_locations') ) ){ ?>
						<?php if ( get_sub_field('slide_format') == 'Movie' ) { ?>
							<?php $image = get_sub_field('background_image'); ?>
							<div class="movie" style="background-image: url('<?php echo $image['url']; ?>');">
								<div class="max-width">
									<div class="contents">
										<h1><?php the_sub_field('movie_title'); ?></h1>
										<p class="details"><?php the_sub_field('movie_info'); ?></p>
										<?php if ( get_sub_field('movie_rating_imdb') & get_sub_field('movie_rating_rotten') ) { ?>
											<p class="reviews">Imdb <b><?php the_sub_field('movie_rating_imdb'); ?></b><span></span>Rotten Tomatoes <b><?php the_sub_field('movie_rating_rotten'); ?></b></p>
										<?php } ?>
										<?php if ( get_sub_field('primary_link') || get_sub_field('secondary_link') ) { ?>
											<p class="btn-container">
												<?php if ( get_sub_field('primary_link') ) { ?>
													<a href="<?php the_sub_field('primary_link'); ?>" class="btn primary-btn"><span><?php the_sub_field('primary_text'); ?></span></a>
												<?php } ?>
												<?php if ( get_sub_field('secondary_link') ) { ?>
													<a target="blank" href="<?php the_sub_field('secondary_link'); ?>" class="btn secondary-btn play" data-lity><span><?php the_sub_field('secondary_text'); ?>
														<!-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
															<defs>
																<clipPath id="a"><path d="M189 40V16h24v24zm3-10.35l17.54 7.13L203.83 19z"/></clipPath>
															</defs>
															<path stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" d="M203.83 19l5.71 17.78L192 29.65z" clip-path="url(&quot;#a&quot;)" transform="translate(-189 -16)"></path>
														</svg> -->
														</span>
													</a>
												<?php } ?>
											</p>
										<?php } ?>
									</div>
								</div>
								<div class="overlay"></div>
							</div>
						<?php } else { ?>
							<?php $image = get_sub_field('background_image'); ?>
							<div class="other" style="background-image: url('<?php echo $image['url']; ?>');">
								<div class="max-width">
									<div class="contents">
										<h1><?php the_sub_field('slide_title'); ?></h1>
										<?php if ( get_sub_field('slide_details') ) { ?>
											<p class="details"><?php the_sub_field('slide_details'); ?></p>
										<?php } ?>
										<?php if ( get_sub_field('primary_link') || get_sub_field('secondary_link') ) { ?>
											<p class="btn-container">
												<?php if ( get_sub_field('primary_link') ) { ?>
													<a href="<?php the_sub_field('primary_link'); ?>" class="btn primary-btn"><span><?php the_sub_field('primary_text'); ?></span></a>
												<?php } ?>
												<?php if ( get_sub_field('secondary_link') ) { ?>
													<a href="<?php the_sub_field('secondary_link'); ?>" class="btn secondary-btn"><span><?php the_sub_field('secondary_text'); ?></span></a>
												<?php } ?>
											</p>
										<?php } ?>
									</div>
								</div>
								<div class="overlay"></div>
							</div> 
						<?php } ?>
					<?php } ?>

        <?php endwhile;
      } ?>
    </div>
  </section>

<?php } else if ( get_field('title_format') == 'img_slider' ) { ?>

    <section class="page-title-container full-width dark-bg">
      <div class="page-title-slider">
        <?php if( have_rows('simple_background_images') ) {
          while ( have_rows('simple_background_images') ) : the_row(); ?>
            <?php $image = get_sub_field('bg_image'); ?>
            <div style="background-image: url('<?php echo $image['url']; ?>');">
              <div class="max-width">
                <p class="slide-description"><?php the_sub_field('image_description'); ?></p>
              </div>
              <div class="overlay"></div>
            </div>
          <?php endwhile;
        } ?>
      </div>
      <div class="max-width">
        <?php if ( is_page(1467) ) { //specials page?>
          <?php
            if ($_COOKIE['CinergyLocation'] == 'location-cc') { echo "<h1>Copperas Cove's Specials</h1>"; }
            else if ($_COOKIE['CinergyLocation'] == 'location-mid') { echo "<h1>Midland's Specials</h1>"; }
						else if ($_COOKIE['CinergyLocation'] == 'location-odes') { echo "<h1>Odessa's Specials</h1>"; }
						else if ($_COOKIE['CinergyLocation'] == 'location-ama') { echo "<h1>Amarillo's Specials</h1>"; }
						else if ($_COOKIE['CinergyLocation'] == 'location-tulsa') { echo "<h1>Tulsa's Specials</h1>"; }
						else if ($_COOKIE['CinergyLocation'] == 'location-granbury') { echo "<h1>Granbury's Specials</h1>"; }
						else if ($_COOKIE['CinergyLocation'] == 'location-mfalls') { echo "<h1>Marble Falls' Specials</h1>"; }
            else { echo '<h1>'.get_the_title().'</h1>'; }
          ?>
        <?php } else { ?>
          <h1><?php the_title(); ?></h1>
        <?php } ?>
        <?php if ( get_field('single_subheading') ) { ?>
          <h2 class="subheading"><?php the_field('single_subheading'); ?></h2>
        <?php } ?>
      </div>
    </section>

<?php } else { ?>

  <section class="page-title-container full-width dark-bg" style="background-image: url('<?php echo $thumb_url; ?>');">
    <div class="max-width">
      <?php if ( is_woocommerce() && is_archive() ) { ?>
        <h1>Fun Cards</h1>
      <?php } else { ?>
        <h1><?php the_title(); ?></h1>
      <?php } ?>
    </div>
    <div class="overlay"></div>
  </section>
  
<?php } ?>