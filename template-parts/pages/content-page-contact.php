<?php /*
CONTACT FORM END OF PAGE
*/ ?>
<section class="contact-container">
  <a id="contact-section" class="styleguide-anchor"></a>
  <div class="flex-container max-width">
    <div class="one-half">
      <h2><?php the_field('contact_section_heading'); ?></h2>
    </div>
    <div class="one-half">
      <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
    </div>
  </div>
</section>