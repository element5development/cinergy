<?php /*
MAIN NAVIGATION
*/ ?>

<!-- REQUEST PERMISSION TO USE LOCATION & SET LOCATION COOKIE -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-aqgfh2k7QPdRRUJ4Jc31vkFpbvxlr_c&callbacksensor=false"></script>
<script>
  //ASK FOR LOCATION ON AGE LOAD IF NO COOKIE IS SET
  document.addEventListener("DOMContentLoaded", function() {
    if ( Cookies.get('CinergyLocation') ){
     //COOKIE SET
    } else {
     getLocation();
    }
  });
  function getLocation() { //REQUEST POSITION
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(savePosition);
    } else {
      //NO PERMISSION THEN NO AUTOMATED LOCATION
      Cookies.set('CinergyLocation', 'location-none' , { expires: 1, path: '/' }); //set 24hour location cookie to no location
      $('body').addClass('location-none'); //set body class no location
      location.reload();
    }
  }
  function savePosition(position) {
    //LAT & LONG PROVIDED TO DETERMINE ZIP CODE
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var latlng = new google.maps.LatLng(lat, lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          var zip = results[0].address_components[6].long_name; 
          if ( zip === '76522' || zip === '76544' || zip === '76549' || zip === '76539') { //Cinergy Copperas Cove
            Cookies.set('CinergyLocation', 'location-cc' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-cc'); //set body class default location
            location.reload();
          } else if ( zip === '79701' || zip === '79702' || zip === '79703' || zip === '79704' || zip === '79705' || zip === '79706' || zip === '79707' ) { //Cinergy Midland
            Cookies.set('CinergyLocation', 'location-mid' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-mid'); //set body class default location
            location.reload();
          } else if ( zip === '79758' || zip === '79762' || zip === '79763' || zip === '79764' || zip === '79765' || zip === '79766') { //Cinergy Odessa
            Cookies.set('CinergyLocation', 'location-odes' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-odes'); //set body class default location
            location.reload();
					} else if ( zip === '79107' || zip === '79106' || zip === '79101' || zip === '79102' || zip === '79104' || zip === '79103' || zip === '79109' || zip === '79110' || zip === '79124' || zip === '79108' || zip === '79118' || zip === '79121' ) { //Cinergy Amarillo
            Cookies.set('CinergyLocation', 'location-ama' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-ama'); //set body class default location
						location.reload();
					} else if ( zip === '74141' || zip === '74112' || zip === '74128' || zip === '74129' || zip === '74116' || zip === '74145' || zip === '74115' || zip === '74146' || zip === '74135' || zip === '74114' || zip === '74104' || zip === '74108' || zip === '74110' || zip === '74117' || zip === '74134' ) { //Cinergy Tulsa
            Cookies.set('CinergyLocation', 'location-tulsa' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-tulsa'); //set body class default location
            location.reload();
          } else { //IF THERE NO LOCATION NEARBY SET TO MAIN LOCATION OR SET TO ALL
            Cookies.set('CinergyLocation', 'location-none' , { expires: 1, path: '/' }); //set 24hour location cookie to default
            $('body').addClass('location-none'); //set body class default location
            location.reload();
          }
        }
      }
    });
  }
  //MANUALLY SET LOCATION COOKIE
  jQuery(function() {
    jQuery("#location-cc, #location-cc-2").on('click', function(event) { //location Copperas Cove selected
      Cookies.set('CinergyLocation', 'location-cc'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-mid location-odes location-ama').addClass('location-cc'); //update body class location
    });
    jQuery("#location-mid, #location-mid-2").on('click', function(event) { //location Midland selected
      Cookies.set('CinergyLocation', 'location-mid'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-odes location-ama location-granbury location-mfalls').addClass('location-mid'); //update body class location
    });
    jQuery("#location-odes, #location-odes-2").on('click', function(event) { //location Odessa selected
      Cookies.set('CinergyLocation', 'location-odes'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-ama location-granbury location-mfalls').addClass('location-odes'); //update body class location
		});
		jQuery("#location-ama, #location-ama-2").on('click', function(event) { //location Amarillo selected
      Cookies.set('CinergyLocation', 'location-ama'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-granbury location-mfalls').addClass('location-ama'); //update body class location
		});
		jQuery("#location-tulsa, #location-tulsa-2").on('click', function(event) { //location Tulsa selected
      Cookies.set('CinergyLocation', 'location-tulsa'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-ama location-granbury location-mfalls').addClass('location-tulsa'); //update body class location
		});
		jQuery("#location-granbury, #location-granbury-2").on('click', function(event) { //location Granbury selected
      Cookies.set('CinergyLocation', 'location-granbury'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-ama location-mfalls').addClass('location-granbury'); //update body class location
		});
		jQuery("#location-mfalls, #location-mfalls-2").on('click', function(event) { //location mfalls selected
      Cookies.set('CinergyLocation', 'location-mfalls'); //override location cookie
      location.reload();
      jQuery('body').removeClass('location-none location-cc location-mid location-odes location-ama location-granbury').addClass('location-mfalls'); //update body class location
    });
  })
</script>



<?php if (  is_page_template( 'template-styleguide.php' ) ) { ?>
  <header id="sticky-nav" class="primary-nav full-width dark-bg">
    <div class="max-width clearfix">
      <a href="<?php echo site_url(); ?>"><img id="nav-logo" src="<?php echo site_url(); ?>/wp-content/uploads/2016/10/Cinergy-Logo-White.svg" alt="Cinergy Logo White"/></a>
      <h2>Styleguide</h2>
    </div>
  </header>
<?php } else { ?>

  <header id="sticky-nav" class="primary-nav flex-container full-width<?php if ( get_field('notification_bar_text', 'options') ) : ?> notification-on<?php endif; ?>">
		
		<?php get_template_part('template-parts/elements/notification-bar'); ?>

		<?php if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { //LOCATION CC 
			?><div class="locaiton-picker location-cc"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { //LOCATION MID 
			?><div class="locaiton-picker location-mid"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-odes' ) { //LOCATION ODES
			?><div class="locaiton-picker location-odes"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-ama' ) { //LOCATION Amarillo
			?><div class="locaiton-picker location-ama"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa' ) { //LOCATION Tulsa
			?><div class="locaiton-picker location-tulsa"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-granbury' ) { //LOCATION Granbury
			?><div class="locaiton-picker location-granbury"><?php
		} else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls' ) { //LOCATION Marbel Falls
			?><div class="locaiton-picker location-mfalls"><?php
		} else { //NO LOCATION IS SET 
			?><div class="locaiton-picker location-none"><?php
		} ?>
      <svg id="location-pointer" xmlns="http://www.w3.org/2000/svg" width="25" height="32" viewBox="0 0 25 32">
        <path d="M20.65 18l-8.26 10.62L4.17 18a9.77 9.77 0 0 1-2.11-6c0-5.52 4.62-10 10.33-10 5.7 0 10.32 4.48 10.32 10 0 2.26-.81 4.32-2.11 6zM0 12c0 2.19.62 4.23 1.67 6h.02L12.4 32l10.7-14a11.61 11.61 0 0 0 1.67-6c0-6.63-5.54-12-12.38-12C5.55 0 0 5.37 0 12z"></path>
        <path d="M8.06 12c0-2.21 1.85-4 4.13-4a4.07 4.07 0 0 1 4.13 4c0 2.21-1.85 4-4.13 4a4.07 4.07 0 0 1-4.13-4zM6 12c0 3.31 2.77 6 6.19 6a6.1 6.1 0 0 0 6.2-6c0-3.31-2.78-6-6.2-6A6.1 6.1 0 0 0 6 12z"></path>
      </svg>
      <div class='pulse'></div>
      <p class="active">
        Your Location:<br/>
        <span id="demo">
          <?php
            if ($_COOKIE['CinergyLocation'] == 'location-cc') { echo 'Copperas Cove'; }
            else if ($_COOKIE['CinergyLocation'] == 'location-mid') { echo 'Midland'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-odes') { echo 'Odessa'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-ama') { echo 'Amarillo'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-tulsa') { echo 'Tulsa'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-granbury') { echo 'Granbury'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-mfalls') { echo 'Marble Falls'; }
            else { echo 'not set'; }
          ?>
        </span>
      </p>
      <p class="location-options label">Set my location</p>
      <div class="location-option-container">
        <div id="location-cc" class="btn primary"><span>Copperas Cove</span></div>
        <div id="location-mid" class="btn primary"><span>Midland</span></div>
        <div id="location-odes" class="btn primary"><span>Odessa</span></div>
				<div id="location-ama" class="btn primary"><span>Amarillo</span></div>
				<div id="location-tulsa" class="btn primary"><span>Tulsa</span></div>
				<div id="location-granbury" class="btn primary"><span>Granbury</span></div>
				<div id="location-mfalls" class="btn primary"><span>Marble Falls</span></div>
      </div>
    </div>
    <div class="bar-container">
      <div class="bottom-bar">
        <a href="<?php echo site_url(); ?>"><img id="nav-logo" src="<?php echo site_url(); ?>/wp-content/uploads/2016/10/Cinergy-Logo-White.svg" alt="Cinergy Logo White"/></a>
        <nav>
          <ul>
            <li id="tickets">
							<a href="<?php echo site_url(); ?>/movies/#content-anchor">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="38" height="33" viewBox="0 0 38 33">
									<defs>
										<path id="a" d="M1240.92 29.17c.17.18.4.27.62.27.21 0 .41-.07.57-.22a.83.83 0 0 0 .05-1.19l-.7-.76a.85.85 0 0 0-1.19-.05.85.85 0 0 0-.05 1.19z"/>
										<path id="b" d="M1242.92 32.17c.17.18.4.27.62.27.21 0 .41-.07.57-.22a.83.83 0 0 0 .05-1.19l-.7-.76a.85.85 0 0 0-1.19-.05.85.85 0 0 0-.05 1.19z"/>
										<path id="c" d="M1245.93 35.17c.16.18.39.27.61.27.21 0 .41-.07.57-.22a.83.83 0 0 0 .05-1.19l-.7-.76a.85.85 0 0 0-1.19-.05.85.85 0 0 0-.05 1.19z"/>
										<path id="d" d="M1250.97 51.31v-.16l-.02-.56a.84.84 0 0 0-1.68.08l.02.56.02.09-15.55.14 13.85-12.73 1.06-.05c.09.39.42.67.82.67h.05a.84.84 0 0 0 .79-.74l4.67-.2.13 3.07a.08.08 0 0 1-.04.02l-.44.19-.05.03c-.13.07-.26.16-.38.25l-.05.03a2.5 2.5 0 0 0-.41.38l-.29.35-.04.07c-.08.12-.16.25-.23.39l-.02.03c-.06.14-.12.29-.16.45l-.03.08c-.04.15-.08.31-.1.47v.08a2.9 2.9 0 0 0 .04 1.01l.02.09c.03.15.08.3.13.45l.04.08c.06.15.12.29.2.43l.03.03a3 3 0 0 0 .62.75l.07.07c.12.1.24.19.37.27l.06.03c.12.08.25.15.39.21l.06.03c.14.06.29.11.44.15l.05.01.15 3.36zm-21.45-2.73c.07-.1.12-.21.17-.32l.07-.14c.08-.19.14-.37.18-.57l.02-.13c.03-.15.05-.3.06-.46v-.21l-.02-.38-.04-.23-.1-.37a2.53 2.53 0 0 0-.25-.58l-.09-.18a3.5 3.5 0 0 0-.84-.91l-.17-.11a3.24 3.24 0 0 0-.35-.21l-.21-.09a2.42 2.42 0 0 0-.35-.12.98.98 0 0 0-.23-.06c-.12-.03-.25-.04-.37-.06l-.22-.02-.44.02-.14.02c-.2.02-.39.07-.58.13l-.14.06c-.12.05-.23.09-.34.15l-2.11-2.3 15.45-14.19c.15.11.33.17.51.17a.83.83 0 0 0 .7-1.28l3.45-3.18 2.08 2.26-.17.34-.06.12a4.1 4.1 0 0 0-.17.57l-.02.13-.05.45v.2l.03.39.04.21.1.37.07.2c.05.13.11.25.18.38l.09.15a3.59 3.59 0 0 0 .74.84l.09.06c.13.1.26.19.4.27l.05.02c.13.07.26.13.4.18l.09.03c.14.05.29.09.45.12l.08.01c.14.02.27.04.41.04h.08l.05.01.18-.02.33-.03.23-.05c.1-.02.21-.04.31-.08.08-.02.15-.06.23-.09.07-.03.13-.04.2-.08l2.29 2.5-3.51 3.22a.86.86 0 0 0-.81.17.84.84 0 0 0-.24.8l-.39.36h-.12c-.45.02-.8.4-.8.85l-14.24 13.08zm27.54-1.44l-.01-.08-.01-.07-.03-.09-.02-.07-.04-.07-.04-.07a.27.27 0 0 0-.05-.06l-.05-.06-.06-.05-.06-.05-.08-.04a.49.49 0 0 0-.07-.03c-.02-.02-.06-.02-.09-.03l-.06-.02a.88.88 0 0 0-.17-.02 1.67 1.67 0 0 1-.14-3.34c.06 0 .11-.02.16-.03l.04-.02a.36.36 0 0 0 .11-.04l.05-.03.09-.06.04-.04.07-.07.04-.05.05-.08c.02-.02.02-.04.03-.07l.04-.08.02-.07.02-.09v-.07c0-.03.01-.05 0-.07l-.2-4.62a.84.84 0 0 0-.87-.81l-6.24.27 4.1-3.76a.85.85 0 0 0 .05-1.19l-3.34-3.64-.02-.01a.91.91 0 0 0-.23-.17l-.03-.01a.68.68 0 0 0-.28-.07h-.02a.7.7 0 0 0-.29.03l-.01.01c-.1.03-.2.08-.28.15-.34.29-.77.42-1.19.39a1.6 1.6 0 0 1-1.12-.53 1.66 1.66 0 0 1-.31-1.78c.08-.2.2-.38.36-.54a.84.84 0 0 0 .18-.28l.02-.11c.02-.07.04-.13.04-.2l-.02-.13-.04-.18-.07-.12c-.03-.05-.05-.1-.09-.14l-3.13-3.41a.86.86 0 0 0-.58-.27.87.87 0 0 0-.61.22l-21.35 19.62a.86.86 0 0 0-.27.59c-.01.22.07.44.22.6l3.17 3.45.01.01c.07.07.16.14.25.18l.02.01c.09.04.19.06.29.07h.01c.11 0 .21-.01.31-.04v-.01c.1-.03.2-.08.28-.16a1.68 1.68 0 0 1 2.27 2.47.8.8 0 0 0-.18.27l-.03.11c-.02.07-.04.14-.04.21l.02.12c0 .07.01.13.03.19l.07.12.1.16 3.29 3.58c.17.18.39.27.62.27l.05-.01c.12.08.27.12.42.12h.01l24.25-.21a.8.8 0 0 0 .6-.26.8.8 0 0 0 .23-.61z"/><path id="e" d="M1249.9 42.96h.05a.86.86 0 0 0 .8-.89l-.07-1.27a.84.84 0 1 0-1.68.08l.07 1.28c.02.45.39.8.83.8"/><path id="f" d="M1249.8 45a.85.85 0 0 0-.8.88l.07 1.28c.02.45.39.8.83.8h.05a.86.86 0 0 0 .8-.89l-.07-1.27a.84.84 0 0 0-.88-.8"/>
									</defs>
									<use xlink:href="#a" transform="translate(-1220 -20)"/>
									<use xlink:href="#b" transform="translate(-1220 -20)"/>
									<use xlink:href="#c" transform="translate(-1220 -20)"/>
									<use xlink:href="#d" transform="translate(-1220 -20)"/>
									<use xlink:href="#e" transform="translate(-1220 -20)"/>
									<use xlink:href="#f" transform="translate(-1220 -20)"/>
								</svg>
								<span>Tickets</span>
							</a>
            </li>
            <li id="menu-open">
              <svg xmlns="http://www.w3.org/2000/svg" width="33" height="18" viewBox="0 0 33 18">
                <path d="M0 2.57h33V0H0zM0 10.57h33V8H0zM0 17.57h33V15H0z"></path>
              </svg>
							<span>Menu</span>
            </li>
						<li id="search-open">
              <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29" viewBox="0 0 29 29">
                <path d="M1.81 10.88a9.07 9.07 0 1 1 18.14 0 9.07 9.07 0 0 1-18.14 0zm17.9 6.32a10.88 10.88 0 1 0-2.51 2.51l8.72 8.71a1.77 1.77 0 0 0 2.5-2.5z"></path>
              </svg>
							<span>Search</span>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </header>
  <div class="open-nav">
    <div class="locaiton-picker">
      <svg id="location-pointer" xmlns="http://www.w3.org/2000/svg" width="25" height="32" viewBox="0 0 25 32">
        <path d="M20.65 18l-8.26 10.62L4.17 18a9.77 9.77 0 0 1-2.11-6c0-5.52 4.62-10 10.33-10 5.7 0 10.32 4.48 10.32 10 0 2.26-.81 4.32-2.11 6zM0 12c0 2.19.62 4.23 1.67 6h.02L12.4 32l10.7-14a11.61 11.61 0 0 0 1.67-6c0-6.63-5.54-12-12.38-12C5.55 0 0 5.37 0 12z"></path>
        <path d="M8.06 12c0-2.21 1.85-4 4.13-4a4.07 4.07 0 0 1 4.13 4c0 2.21-1.85 4-4.13 4a4.07 4.07 0 0 1-4.13-4zM6 12c0 3.31 2.77 6 6.19 6a6.1 6.1 0 0 0 6.2-6c0-3.31-2.78-6-6.2-6A6.1 6.1 0 0 0 6 12z"></path>
      </svg>
      <div class='pulse'></div>
      <p class="active">
        Your Location: 
        <span id="demo">
          <?php
            if ($_COOKIE['CinergyLocation'] == 'location-cc') { echo 'Copperas Cove'; }
            else if ($_COOKIE['CinergyLocation'] == 'location-mid') { echo 'Midland'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-odes') { echo 'Odessa'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-ama') { echo 'Amarillo'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-tulsa') { echo 'Tulsa'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-granbury') { echo 'Granbury'; }
						else if ($_COOKIE['CinergyLocation'] == 'location-mfalls') { echo 'Marble Falls'; }
            else { echo 'not set'; }
          ?>
        </span>
      </p>
      <p class="location-options label">Set my location</p>
      <div class="location-option-container">
        <div id="location-cc-2" class="btn primary"><span>Copperas Cove</span></div>
        <div id="location-mid-2" class="btn primary"><span>Midland</span></div>
        <div id="location-odes-2" class="btn primary"><span>Odessa</span></div>
				<div id="location-ama-2" class="btn primary"><span>Amarillo</span></div>
				<div id="location-tulsa-2" class="btn primary"><span>Tulsa</span></div>
				<div id="location-granbury-2" class="btn primary"><span>Granbury</span></div>
				<div id="location-mfalls-2" class="btn primary"><span>Marble Falls</span></div>
      </div>
    </div>
    <svg id="close-menu" xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37">
      <path d="M21.77 18.5l4.08-4.08a2.32 2.32 0 0 0-3.27-3.27l-4.08 4.08-4.2-4.08c-.9-.9-2.36-.9-3.27 0-.9.9-.67 2.37.24 3.27l3.96 4.08-4.2 4.19a2.32 2.32 0 0 0 3.27 3.28l4.2-4.2 4.19 4.2a2.32 2.32 0 0 0 3.27-3.28zm-19.46 0a16.2 16.2 0 1 1 32.38 0 16.2 16.2 0 0 1-32.38 0zM0 18.5a18.5 18.5 0 1 0 37 0 18.5 18.5 0 0 0-37 0z"></path>
    </svg>
    <div class="tile-menu flex-container max-width">
      <div class="discount-menu one-fifth"> 
        <div class="contents">
          <a href="<?php echo site_url(); ?>/specials/"><h1>See Our Specials</h1></a>
          <p>Check out our specials by location</p>
          <a href="<?php echo site_url(); ?>/specials/" class="btn secondary-btn">See all specials</a>
        </div>
        <div class="overlay"></div>
      </div>
      <div class="location-menu one-fifth">
        <div class="contents">
          <a href="<?php echo site_url(); ?>/locations/"><h1>Locations</h1></a>
          <ul>
						<?php
						$childArgs = array(
								'sort_order' => 'ASC',
								'child_of' => '1481'
						);
						$childList = get_pages($childArgs);
						foreach ($childList as $child) { ?>
							<li><a href="<?php echo get_permalink($child->ID); ?>"><?php the_field('location_name', $child->ID); ?></a></li>
						<?php } ?>
          </ul>
        </div>
        <div class="overlay"></div>
      </div>
      <div class="movies-menu one-fifth">
        <div class="contents">
          <a href="<?php echo site_url(); ?>/movies/#content-anchor"><h1>Movies</h1></a>
          <ul>
            <li><a href="<?php echo site_url(); ?>/movies/#content-anchor">Now Showing</a></li>
            <li><a href="<?php echo site_url(); ?>/movies/?filter=Coming%20Soon">Coming Soon</a></li>
            <li><a href="<?php echo site_url(); ?>/movies/?filter=Sensory-Friendly">Sensory Friendly</a></li>
          </ul>
        </div>
        <div class="overlay"></div>
      </div>
      <div class="play-menu one-fifth">
        <div class="contents">
          <a href="<?php echo site_url(); ?>/play/"><h1>Play</h1></a>
          <ul>
            <?php if ( $_COOKIE["CinergyLocation"] != 'location-none' ) { //LOCATION SET ?>
              <?php //QUERY ALL FEATURED SPECIALS
                $args = array( 
                  'posts_per_page'  => -1, 
                  'post_type' => 'Play',
                  );
                $query = new WP_Query( $args );
              ?>
              <?php if ( $query->have_posts() ) { ?>
                <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                  <?php $locations = get_field('location_based'); ?>
                  <?php foreach( $locations  as $location ) { //CHECK ALL DEAL LOCATIONS ?>
                    <?php if ( $_COOKIE["CinergyLocation"] == $location ) { //ONLY DISPLAY IF ONE OF THE LOCATIONS EQUALS SET LOCATION ?>
                      <?php $post_slug = get_post_field( 'post_name', get_post() ); ?>
                      <li><a href="<?php echo site_url(); ?>/play/#<?php echo $post_slug; ?>"><?php the_title(); ?></a></li>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
              <?php wp_reset_postdata(); ?> 
            <?php } else { //NO LOCATION IS SET ?>
              <?php //QUERY ALL FEATURED SPECIALS
                $args = array( 
                  'posts_per_page'  => -1, 
                  'post_type' => 'Play',
                  'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'location_based',
                        'value'   => 'location-cc',
                        'compare' => 'LIKE',
                    ),
                    array(
                        'key'     => 'location_based',
                        'value'   => 'location-mid',
                        'compare' => 'LIKE',
                    ),
                    array(
                        'key'     => 'location_based',
                        'value'   => 'location-odes',
                        'compare' => 'LIKE',
										),
										array(
											'key'     => 'location_based',
											'value'   => 'location-ama',
											'compare' => 'LIKE',
										),
										array(
											'key'     => 'location_based',
											'value'   => 'location-tulsa',
											'compare' => 'LIKE',
										),
										array(
											'key'     => 'location_based',
											'value'   => 'location-granbury',
											'compare' => 'LIKE',
										),
										array(
											'key'     => 'location_based',
											'value'   => 'location-mfalls',
											'compare' => 'LIKE',
										),
                  ),
                );
                $query = new WP_Query( $args );
              ?>
              <?php if ( $query->have_posts() ) { ?>
                <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                  <?php $post_slug = get_post_field( 'post_name', get_post() ); ?>
                  <li><a href="<?php echo site_url(); ?>/play/#<?php echo $post_slug; ?>"><?php the_title(); ?></a></li>
                <?php } ?>
              <?php } ?>
              <?php wp_reset_postdata(); ?> 
            <?php } ?>
          </ul>
        </div>
        <div class="overlay"></div>
      </div>
      <div class="food-menu one-fifth">
        <div class="contents">
          <a href="<?php echo site_url(); ?>/eat-drink/"><h1>Eat &<br> Drink</h1></a>
          <p>We offer In-Theatre Drinks & Dining!</p>
          <a href="<?php echo site_url(); ?>/eat-drink/" class="btn primary-btn"><span>See the menu</span></a>
        </div>
        <div class="overlay"></div>
      </div>
    </div>
    <nav class="max-width">
      <?php wp_nav_menu( array( 'theme_location' => 'primary-nav' ) ); ?>
    </nav> 
    <div style="clear: both"></div>
  </div>
  <div class="open-search">
    <svg id="close-search" xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37">
      <path d="M21.77 18.5l4.08-4.08a2.32 2.32 0 0 0-3.27-3.27l-4.08 4.08-4.2-4.08c-.9-.9-2.36-.9-3.27 0-.9.9-.67 2.37.24 3.27l3.96 4.08-4.2 4.19a2.32 2.32 0 0 0 3.27 3.28l4.2-4.2 4.19 4.2a2.32 2.32 0 0 0 3.27-3.28zm-19.46 0a16.2 16.2 0 1 1 32.38 0 16.2 16.2 0 0 1-32.38 0zM0 18.5a18.5 18.5 0 1 0 37 0 18.5 18.5 0 0 0-37 0z"></path>
    </svg>
    <div class="flex-container max-width">
      <?php get_search_form(); ?>
    </div>
    <div class="flex-container max-width">
      <div class="one-third">
        <h2>Popular Pages</h2>
        <ul>
          <li><a href="<?php echo site_url(); ?>/movies/">Movies</a></li>
          <li><a href="<?php echo site_url(); ?>/specials/">Specials</a></li>
          <li><a href="<?php echo site_url(); ?>/parties-events/">Parties & Events</a></li>
          <li><a href="<?php echo site_url(); ?>/locations/">Locations</a></li>
          <li><a href="<?php echo site_url(); ?>/eat-drink/">Eat & Drink</a></li>
        </ul>
      </div>
      <div class="one-third">
        <h2>Contact Us</h2>
        <p>
          No luck finding exactly what you need? Have additional questions you can't find the answer too? Then <a href="/contact/">message us</a> or if you rather talk to a real life person call us at 
          <?php if ( isset($_COOKIE["CinergyLocation"]) ) { 
            if ( $_COOKIE["CinergyLocation"] == 'location-cc' ) { ?>
              <a href="tel:<?php the_field('copperas_cove_phone','options'); ?>"><?php the_field('copperas_cove_phone','options'); ?></a>
            <?php } else if ( $_COOKIE["CinergyLocation"] == 'location-mid' ) { ?>
              <a href="tel:<?php the_field('midland_phone','options'); ?>"><?php the_field('midland_phone','options'); ?></a>
            <?php } else if ( $_COOKIE["CinergyLocation"] == 'location-odes') { ?>
             <a href="tel:<?php the_field('odessa_phone','options'); ?>"><?php the_field('odessa_phone','options'); ?></a>
						<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-ama') { ?>
							 <a href="tel:<?php the_field('amarillo_phone','options'); ?>"><?php the_field('amarillo_phone','options'); ?></a>
						<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-tulsa') { ?>
             	<a href="tel:<?php the_field('tulsa_phone','options'); ?>"><?php the_field('tulsa_phone','options'); ?></a>
						<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-granbury') { ?>
             	<a href="tel:<?php the_field('granbury_phone','options'); ?>"><?php the_field('granbury_phone','options'); ?></a>
						<?php } else if ( $_COOKIE["CinergyLocation"] == 'location-mfalls') { ?>
             	<a href="tel:<?php the_field('mfalls_phone','options'); ?>"><?php the_field('mfalls_phone','options'); ?></a>
						<?php } else {
              echo '[set location to see number]'; 
            }
          } else { 
              echo '[set location to see number]'; 
          } ?>
        </p>
      </div>
      <div class="one-third">
        <h2>Follow Us</h2>
        <div class="social-links">
				<?php if ( get_field('facebook-url', 'option') ) { ?>
            <a target="_blank" href="<?php the_field('facebook-url', 'option') ?>" class="facebook social-link">
              <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
                <path fill="none" stroke="#3b5998" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
                <path class="color-fill" fill="#fff" d="M26.93 13.51v2.9s-2.13-.22-2.67.6c-.29.45-.12 1.76-.14 2.7h2.83c-.24 1.1-.41 1.85-.59 2.8H24.1v9.01h-3.93v-8.97H18.5v-2.84h1.65c.09-2.07.12-4.12 1.15-5.17 1.16-1.18 2.26-1.03 5.63-1.03"></path>
              </svg>
            </a>
          <?php } ?>
          <?php if ( get_field('instagram-url', 'option') ) { ?>
            <a target="_blank" href="<?php the_field('instagram-url', 'option') ?>" class="instagram social-link">
              <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
                <path fill="none" stroke="#c13584" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
                <path class="color-fill" fill="#fff" d="M19.2 14.55c-.85.04-1.43.17-1.94.37a3.91 3.91 0 0 0-2.34 2.32c-.2.51-.33 1.09-.37 1.94-.04.84-.05 1.11-.05 3.27 0 2.17.01 2.44.05 3.28.04.85.17 1.43.37 1.94.2.52.48.96.92 1.4.45.45.89.72 1.42.92.51.2 1.09.33 1.94.37.85.04 1.13.05 3.3.05s2.44-.01 3.3-.05a5.85 5.85 0 0 0 1.94-.37 4 4 0 0 0 2.34-2.32c.2-.51.33-1.09.37-1.94.04-.84.05-1.11.05-3.28 0-2.16-.01-2.43-.05-3.27a5.85 5.85 0 0 0-.37-1.94 4.13 4.13 0 0 0-2.34-2.32 5.85 5.85 0 0 0-1.94-.37c-.86-.04-1.13-.05-3.3-.05s-2.45.01-3.3.05zm6.53 1.43c.78.04 1.2.16 1.49.27a2.57 2.57 0 0 1 1.51 1.51c.11.28.24.7.28 1.48.04.84.05 1.09.05 3.21 0 2.13-.01 2.38-.05 3.22a4.5 4.5 0 0 1-.28 1.48 2.57 2.57 0 0 1-1.51 1.51c-.29.1-.71.23-1.49.27-.84.04-1.1.05-3.23.05-2.14 0-2.39-.01-3.23-.05a4.9 4.9 0 0 1-1.49-.27 2.57 2.57 0 0 1-.92-.6 2.54 2.54 0 0 1-.6-.91 4.32 4.32 0 0 1-.27-1.48c-.04-.84-.05-1.09-.05-3.22 0-2.12.01-2.37.05-3.21.03-.78.16-1.2.27-1.48.15-.37.32-.63.6-.91.28-.28.55-.45.92-.6a4.6 4.6 0 0 1 1.49-.27c.84-.04 1.09-.05 3.23-.05 2.13 0 2.39.01 3.23.05z"></path>
                <path class="color-fill" fill="#fff" d="M19.94 22.58a2.66 2.66 0 0 1 2.67-2.65 2.66 2.66 0 1 1 0 5.31 2.66 2.66 0 0 1-2.67-2.66zm-1.44.01a4.1 4.1 0 0 0 8.21 0 4.1 4.1 0 1 0-8.21 0zM27.42 18.45a.96.96 0 0 1-1.92 0 .96.96 0 0 1 1.92 0"></path>
              </svg>
            </a>
          <?php } ?>
          <?php if ( get_field('twitter-url', 'option') ) { ?>
            <a target="_blank" href="<?php the_field('twitter-url', 'option') ?>" class="twitter social-link">
              <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
                <path fill="none" stroke="#00aced" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
                <path class="color-fill" fill="#fff" d="M25.7 17.5a3.23 3.23 0 0 1 2.44.95 5.1 5.1 0 0 0 1.95-.73 3 3 0 0 1-.99 1.42c-.11.08-.21.19-.34.24v.01c.67-.01 1.22-.31 1.74-.47v.01a4.9 4.9 0 0 1-1.04 1.18l-.48.38c0 .69-.01 1.36-.15 1.94-.77 3.4-2.8 5.71-6.03 6.7-1.15.36-3.02.5-4.35.18a11.3 11.3 0 0 1-1.81-.58 7.1 7.1 0 0 1-.87-.45l-.27-.16c.3.01.65.09.98.04.31-.05.6-.04.88-.1a6.5 6.5 0 0 0 1.85-.65c.26-.15.65-.33.83-.54a3.1 3.1 0 0 1-2.87-2.1c.31.03 1.17.11 1.38-.06a2.29 2.29 0 0 1-1.01-.4 2.91 2.91 0 0 1-1.45-2.59l.32.14c.2.09.4.13.64.18.11.02.31.08.42.04h-.01c-.16-.18-.41-.3-.56-.49a3.03 3.03 0 0 1-.36-3.54l.01.01c.06.12.19.21.28.32a9.06 9.06 0 0 0 6.06 2.83 3.01 3.01 0 0 1 2.44-3.67l.37-.04"></path>
              </svg>
            </a>
          <?php } ?>
          <?php if ( get_field('youtube-url', 'option') ) { ?>
            <a target="_blank" href="<?php the_field('youtube-url', 'option') ?>" class="youtube social-link">
              <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
                <path fill="none" stroke="#b00" stroke-miterlimit="50" stroke-width="3" d="M1.5 23a21.5 21.5 0 1 1 43 0 21.5 21.5 0 0 1-43 0z"></path>
                <path class="color-fill" fill="#fff" d="M22.46 25.55l-.03-5.04 3.51 2.54zm-4.08-8.05c-1.36 0-2.88.91-2.88 2.24v6.62c0 1.33 1.52 2.14 2.88 2.14h10.65c1.36 0 2.46-.81 2.46-2.14v-6.62c0-1.33-1.1-2.24-2.46-2.24z"></path>
              </svg>
            </a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>


<?php } ?>