<?php /*
THE CONTAINER FOR SEARCH RESULTS PAGES
*/ ?>

<?php get_header(); ?>

<main class="page-cotents full-width">
  
  <!-- PAGE TITLE -->
  <?php get_template_part( 'template-parts/pages/content', 'page-title' ); ?>

  <!-- BREADCRUMBS -->
  <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

  <!-- SECONDARY NAV -->
  <?php get_template_part( 'template-parts/navigation/content', 'secondary-nav' ); ?>

  <!-- AJAX LOAD POSTS -->
  <div class="feed-container">
		<?php
			$term = (isset($_GET['s'])) ? $_GET['s'] : ''; // Get 's' querystring param
			echo do_shortcode('[ajax_load_more id="relevanssi" search="'. $term .'" post_type="post, page, adbay_movie" posts_per_page="6" scroll="false" button_label="Load More"]');
		?>
  </div>

  <!-- SPECIALS SLIDER -->
  <?php get_template_part( 'template-parts/elements/content', 'specials-slider' ); ?>

</main>

<?php get_footer(); ?>