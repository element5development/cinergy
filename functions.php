<?php

$file_includes = [
  'lib/theme_support.php',          // Enable Theme Options
  'lib/soil_setup.php',             // Enable Soil Features
  'lib/media_setup.php',            // Additional Image Sizes and Enable SVG uploads
  'lib/gf_setup.php',               // Change Submit Input to Buttons
  'lib/acf_setup.php',              // Auto Minimize ACF Sections and Create Option Pages
  'lib/roles.php',                  // Customize Roles
  'lib/whitelabel.php',             // Admnin Area Whitelabel
  'lib/menus.php',                  // Create Menu
  'lib/post_types.php',             // Create Custom Post Types
	'lib/taxonomies.php',             // Create Custom Post Taxonomies
	'lib/sidebars.php',             	// Create Custom Post Sidebars
	'lib/shortcode.php',       				// Create Shortcodes
  'lib/styleguide_functions.php',   // Styleguide Template Functions

];
foreach ($file_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'starting-point'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);

/*-----------------------------------------
  GEO LOCATION
-----------------------------------------*/
function nearest_location_number() {
	$user_location = $_SERVER['REMOTE_ADDR']; //GET USER IP
	$location_data = json_decode(file_get_contents('http://freegeoip.net/json/50.84.101.66')); //USE USER IP TO GET LOCATION INFO
	//$location_data = json_decode(file_get_contents('http://freegeoip.net/json/'.$user_location)); //USE USER IP TO GET LOCATION INFO

	if(!empty($location_data)) { //SYSTEM FOUND LOCATION
		$city = $location_data->city;
		$zip = $location_data->zip_code;
		$lat = $location_data->latitude;
		$lng = $location_data->longitude;

		//CANT FIND ZIP THEN USE LAT AND LONG TO DETERMINE ZIP
		if (is_null($zip)) {
			$location_zip_from_position = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&sensor=true')); //USE USER IP TO GET LOCATION INFO
			print_r($location_zip_from_position);
			if(!empty($location_zip_from_position)) { //SYSTEM FOUND LOCATION
				$zip = $location_zip_from_position->results[0]->address_components[6]->long_name;
			}
		}

	}

	$location_data = array( $city, $zip );
	return $location_data; //RETURN CITY AND ZIP
}

/*-----------------------------------------
  PREFILL FORM HIDDEN FILED WITH LOCATION
-----------------------------------------*/
    add_filter('gform_field_value_location', 'populate_location');
      function populate_location($value){
      return $_COOKIE['CinergyLocation'];
		}
	
	
//NEW CRON SETUP		
add_filter( 'cron_schedules', 'new_movie_showtime_cron_schedule' );
function new_movie_showtime_cron_schedule( $schedules ) { 
    $schedules['every_five_minutes'] = array(
            'interval'  => 300,
            'display'   => __( 'Every 5 Minutes', 'textdomain' )
    );
    return $schedules;
}
if ( ! wp_next_scheduled( 'new_movie_showtime_cron_hook' ) ) {
    wp_schedule_event( time(), 'every_five_minutes', 'new_movie_showtime_cron_hook' );
}

add_action('new_movie_showtime_cron_hook', 'new_movie_showtime_cron_function'); 
function new_movie_showtime_cron_function() {
    $moviePlugin = new Adbay_Movie_Plugin;
    try {
        $moviePlugin->run_nightly_showtimes_dump();  
    } catch ( Exception $e ) {
        $to = 'ahsan@element5digital.com';
	    $subject = 'Cinergy - Doesnt work';
	    $message = 'The cron function doesnt work.';
	    wp_mail( $to, $subject, $message );
    }
}
?>