<?php /*
Template Name: Sensory Movie
*/ ?>

<?php get_header(); ?>

<!-- GET DEFAULT FEATURE IMAGE -->
<?php 
	$image = get_field('default_movie_image', 'options');
	$background_url = esc_url($image['url']);
?>


<main class="page-contents full-width">

  <!-- PAGE TITLE -->
  <section class="page-title-container full-slider full-width dark-bg">
    <div class="movie" style="background-image: url('<?php echo $background_url; ?>'); background-size: cover;">
    <?php movie_db_render_backdrop( get_the_title(), 'large' ); ?>
      <div class="max-width">
        <div class="contents">
          <p class="details" style="margin: 0;">a sensory friendly screening of</p>
          <h1 style="margin: 0 0 1.5rem 0;"><?php the_title(); ?></h1>
          <p class="btn-container">
              <a href="#tickets-anchor" class="btn primary-btn smoothScroll"><span>get tickets</span></a>
            <?php if ( get_field('trailer_url') ) { ?>
              <a target="blank" href="<?php the_field('trailer_url') ?>" class="btn secondary-btn play" data-lity><span>Watch Trailer
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <defs>
                    <clipPath id="a"><path d="M189 40V16h24v24zm3-10.35l17.54 7.13L203.83 19z"/></clipPath>
                  </defs>
                  <path stroke-linecap="square" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" d="M203.83 19l5.71 17.78L192 29.65z" clip-path="url(&quot;#a&quot;)" transform="translate(-189 -16)"></path>
                </svg>
                </span>
              </a>
            <?php } ?>
          </p>
        </div>
      </div>
      <div class="overlay"></div>
    </div>
  </section>

  <!-- BREADCRUMBS -->
  <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } ?>

  <!-- MOVIE POSTER / RATING / DESCRIPTION -->
  <div class="two_col_basic">
    <div class="flex-container max-width">
      <div class="one-half">
        <?php movie_db_render_poster( get_the_title() ); ?>
      </div>
      <div class="one-half">
        <?php the_content() ?>
      </div>
    </div>
    <div class="overlay"></div>
    <div class="overlay two"></div>
  </div>


  <!-- SHOWTIMES -->
  <div class="movie_showitimes_section two_col_basic scroll">
    <a id="tickets-anchor" class="styleguide-anchor"></a>
    <div class="flex-container max-width">
  
      <?php if ( $_COOKIE["CinergyLocation"] == 'location-none' || !isset($_COOKIE["CinergyLocation"]) ) { //NO LOCATION OR BLOCKED ?>
        <div class="location-forced menu-locations">
          <h2 class="align-center">Select a location to purchase tickets</h2>
          <div class="location-option-container">
            <div id="location-cc-2" class="btn primary" onClick="window.location.reload()"><span>Copperas Cove</span></div>
            <div id="location-mid-2" class="btn primary" onClick="window.location.reload()"><span>Midland</span></div>
            <div id="location-odes-2" class="btn primary" onClick="window.location.reload()"><span>Odessa</span></div>
						<div id="location-ama-2" class="btn primary" onClick="window.location.reload()"><span>Amarillo</span></div>
						<div id="location-tulsa-2" class="btn primary" onClick="window.location.reload()"><span>Tulsa</span></div>
						<div id="location-granbury-2" class="btn primary" onClick="window.location.reload()"><span>Granbury</span></div>
						<div id="location-mfalls-2" class="btn primary" onClick="window.location.reload()"><span>Marble Falls</span></div>
          </div>
        </div>
      <?php } elseif ( get_field('copperas_cove_ticket_purchase') || get_field('midland_ticket_purchase') || get_field('odessa_ticket_purchase') || get_field('amarillo_ticket_purchase') )  { //LOCATION SET ?>
        <div class="sensory-friendly-showtimes max-width" style="text-align: center;">
          <h2>Get Your Tickets Today!</h2>
          <?php
            if ($_COOKIE['CinergyLocation'] == 'location-cc') { 
              the_field('copperas_cove_ticket_purchase');
            }
            else if ($_COOKIE['CinergyLocation'] == 'location-mid') { 
              the_field('midland_ticket_purchase');
            }
            else if ($_COOKIE['CinergyLocation'] == 'location-odes') { 
              the_field('odessa_ticket_purchase'); 
						}
						else if ($_COOKIE['CinergyLocation'] == 'location-ama') { 
              the_field('amarillo_ticket_purchase'); 
						}
						else if ($_COOKIE['CinergyLocation'] == 'location-tulsa') { 
              the_field('tulsa_ticket_purchase'); 
						}
						else if ($_COOKIE['CinergyLocation'] == 'location-granbury') { 
							the_field('granbury_ticket_purchase'); 
						}
						else if ($_COOKIE['CinergyLocation'] == 'location-mfalls') { 
							the_field('mfalls_ticket_purchase'); 
						}
            else { ?>
              <div class="max-width" style="text-align: center;">
                <h2>Intrested?</h2>
                <p><?php the_field('no_online_sales'); ?></p>
              </div>
            <?php } 
          ?>
        </div> 
      <?php } else { ?>
        <div class="max-width" style="text-align: center;">
          <p><?php the_field('no_online_sales'); ?></p>
        </div>
      <?php } ?>

    </div>
  </div>

  <!-- CAST -->
    <div class="cast two_col_basic left">
      <div class="flex-container max-width">
        <div class="one-half">
          <h2>Top Billed Cast</h2>
        </div>
        <div class="one-half">
          <?php movie_db_render_crew( get_the_title() ); ?>
        </div>
      </div>
      <div class="overlay"></div>
      <div class="overlay two"></div>
      <?php movie_db_render_backdrop( get_the_title(), 'large' ); ?>
    </div>

  <!-- PAGE GALLERY -->
  <?php movie_db_render_gallery( get_the_title(), 'large'); ?>

</main>

<!-- Wrapper-Inner End -->
<?php get_footer(); ?>